import { layout, LayoutProps, space, SpaceProps, grid, GridProps, system, color, flexbox } from 'styled-system';
import styled from '~/utils/styled';

interface IGrid extends LayoutProps, SpaceProps, GridProps {
  // This allows us to pass a different type of component than the default div
  // Solved with https://dev.to/jdcas89/start-your-app-the-right-way-featuring-react-styled-system-styled-components-and-typescript-7a4
  as?: keyof JSX.IntrinsicElements | React.ComponentType<any>;
  gridCols?: number | number[];
}
const Grid = styled.div<IGrid>`
  width: 100%;
  ${[layout, space, grid, color, flexbox]}
  ${system({
    gridCols: {
      property: 'gridTemplateColumns',
      transform: (value) => `repeat(${value}, minmax(0, 1fr))`,
    },
  })}
`;

export default Grid;
