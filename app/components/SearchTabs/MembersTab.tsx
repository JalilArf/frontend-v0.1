import React from 'react';
import { useIntl } from 'react-intl';
import Search from '~/components/Search/Search';
import { useSearchStateContext } from '~/contexts/searchStateContext';

export const MembersTab: React.FC = () => {
  const { formatMessage } = useIntl();
  const { index, searchState, setSearchState } = useSearchStateContext();
  return (
    <div className="tab-pane active" id={index}>
      <Search
        searchState={searchState}
        index="User"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'skills' },
          { attribute: 'ressources' },
          { attribute: 'interests', searchable: false, limit: 17 },
        ]}
        sortByItems={[
          // {
          //   label: formatMessage({
          //     id: 'general.filter.newly_updated',
          //     defaultMessage: '*Last active',
          //   }),
          //   index: 'User_updated_at',
          // },
          {
            label: formatMessage({
              id: 'general.filter.people.date2',
              defaultMessage: 'Signup date (newest)',
            }),
            index: 'User_id_des',
          },
          { label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }), index: 'User' },
          {
            label: formatMessage({
              id: 'general.filter.people.date1',
              defaultMessage: 'Signup date (oldest)',
            }),
            index: 'User_id_asc',
          },
          {
            label: formatMessage({ id: 'general.filter.people.alpha1', defaultMessage: 'Last name (A-Z)' }),
            index: 'User_fname_asc',
          },
          {
            label: formatMessage({ id: 'general.filter.people.alpha2', defaultMessage: 'Last name (Z-A)' }),
            index: 'User_fname_desc',
          },
        ]}
      />
    </div>
  );
};
