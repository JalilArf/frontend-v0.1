import React from 'react';
import { useIntl } from 'react-intl';
import Search from '~/components/Search/Search';
import { useSearchStateContext } from '~/contexts/searchStateContext';

export const ProjectsTab: React.FC = () => {
  const { formatMessage } = useIntl();
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <div className="tab-pane active" id={index}>
      <Search
        searchState={searchState}
        index="Project"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'programs.title', searchable: false },
          { attribute: 'challenges.title' },
          { attribute: 'skills' },
          { attribute: 'interests', searchable: false, limit: 17 },
          { attribute: 'status', searchable: false, showmore: false },
          { attribute: 'maturity', searchable: false, showmore: false },
        ]}
        sortByItems={[
          {
            label: formatMessage({
              id: 'general.filter.newly_updated',
              defaultMessage: 'Newly updated',
            }),
            index: 'Project_updated_at',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date2',
              defaultMessage: 'Date added (newest)',
            }),
            index: 'Project_id_des',
          },
          {
            label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }),
            index: 'Project',
          },
          {
            label: formatMessage({ id: 'needs.uppercase', defaultMessage: 'Needs' }),
            index: 'Project_needs',
          },
          {
            label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
            index: 'Project_members',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date1',
              defaultMessage: 'Date added (oldest)',
            }),
            index: 'Project_id_asc',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.alpha1',
              defaultMessage: 'Alphabetical (A-Z)',
            }),
            index: 'Project_title_asc',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.alpha2',
              defaultMessage: 'Alphabetical (Z-A)',
            }),
            index: 'Project_title_desc',
          },
        ]}
      />
    </div>
  );
};
