import { NextRouter } from 'next/router';
import qs from 'qs';

export const pathToSearchState = (asPath: NextRouter['asPath']) =>
  asPath.includes('?') ? qs.parse(asPath.substring(asPath.indexOf('?') + 1)) : {};
