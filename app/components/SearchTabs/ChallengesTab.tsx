import React from 'react';
import { useIntl } from 'react-intl';
import Search from '~/components/Search/Search';
import { useSearchStateContext } from '~/contexts/searchStateContext';

export const ChallengesTab: React.FC = () => {
  const { formatMessage } = useIntl();
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <div className="tab-pane active" id={index}>
      <Search
        searchState={searchState}
        index="Challenge"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'skills' },
          { attribute: 'program.title', searchable: false },
          { attribute: 'status', searchable: false, showmore: false },
          { attribute: 'interests', searchable: false, limit: 17 },
        ]}
        sortByItems={[
          {
            label: formatMessage({
              id: 'general.filter.newly_updated',
              defaultMessage: 'Newly updated',
            }),
            index: 'Challenge_updated_at',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date2',
              defaultMessage: 'Date added (newest)',
            }),
            index: 'Challenge_id_des',
          },
          {
            label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }),
            index: 'Challenge',
          },
          {
            label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
            index: 'Challenge_members',
          },
          {
            label: formatMessage({ id: 'general.projects', defaultMessage: 'Projects' }),
            index: 'Challenge_projects',
          },
          {
            label: formatMessage({ id: 'needs.uppercase', defaultMessage: 'Needs' }),
            index: 'Challenge_needs',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date1',
              defaultMessage: 'Date added (oldest)',
            }),
            index: 'Challenge_id_asc',
          },
        ]}
      />
    </div>
  );
};
