import styled from '~/utils/styled';
import Box from '../Box';

interface ChipProps {
  background?: string;
  color?: string;
}

export default styled(Box)<ChipProps>`
  border-radius: 20px;
  color: ${(p) => p.color || 'black'};
  background: ${(p) => p.background || 'lightgrey'};
  padding: 3.5px 10px;
  width: fit-content;
  margin-right: 0.5rem;
`;
