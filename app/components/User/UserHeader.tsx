import React, { useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Link from 'next/link';
// import $ from "jquery";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useUserData from '~/hooks/useUserData';
import BtnFollow from '../Tools/BtnFollow';
import ListFollowers from '../Tools/ListFollowers';
import UserShowObjects from '~/components/User/UserShowObjects';
import { useModal } from '~/contexts/modalContext';
import { ContactForm } from '../Tools/ContactForm';
import Button from '../primitives/Button';
import useGet from '~/hooks/useGet';
import { textWithPlural } from '~/utils/managePlurals';
import Box from '../Box';
import ReactGA from 'react-ga';
import Chips from '../Chip/Chips';
import { useTheme } from '@emotion/react';
import tw, { styled } from 'twin.macro';
// import "./UserHeader.scss";

const UserHeader = ({ user }) => {
  useEffect(() => {
    $('.moreSkills').click(() => {
      // when click on the "+..." skills
      $('a[href="#about"]').click(); // force click on about tab
      const element = document.querySelector('.tabContainer .infoSkills'); // get skills section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to section
    });
    $('.moreResources').click(() => {
      // when click on the "+..." resources
      $('a[href="#about"]').click(); // force click on about tab
      const element = document.querySelector('.tabContainer .infoResources'); // get resources section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to section
    });
  });

  const { formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const { userData } = useUserData();
  const theme = useTheme();
  // prettier-ignore
  let {
    id, logo_url, skills, ressources, nickname, first_name, last_name, bio, short_bio,
    stats, can_contact, has_followed
  } = user;
  if (!logo_url) {
    logo_url = '/images/default/default-user.png';
  }
  const logoStyle = {
    backgroundImage: `url(${logo_url})`,
  };

  const openFollowersModal = (e) => {
    stats.follower_count && // open modal only if user has followers
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={id} itemType="users" />,
        title: 'Followers',
        titleId: 'entity.tab.followers',
        maxWidth: '70rem',
      });
  };

  const openFollowingModal = (e) => {
    stats.following_count && // open modal only if user is following objects
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <FollowingModal userId={id} />,
        title: 'Following',
        titleId: 'user.profile.tab.following',
        maxWidth: '70rem',
      });
  };

  const showUserProfilePicture = () => {
    showModal({
      children: (
        <Box>
          <img src={logo_url} style={{ objectFit: 'contain' }} />
        </Box>
      ),
      showCloseButton: true,
      maxWidth: '30rem',
    });
  };

  const showSkillsAndResourcesChips = () => (
    <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
      {skills.length !== 0 && (
        <Box>
          <Box color={theme.colors.secondary}>
            {formatMessage({ id: 'user.profile.skills', defaultMessage: 'Skills' })}
          </Box>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              href: `/search/members/?refinementList[skills][0]=${skill}`,
            }))}
            overflowText="userSkill"
            // color={theme.colors.greys['600']}
            // color={theme.colors.greys['400']}
            color="#F2F4F8"
            // color="#fdf1fd"
            showCount={4}
          />
        </Box>
      )}
      {ressources.length !== 0 && (
        <Box>
          <Box color={theme.colors.secondary}>
            {formatMessage({ id: 'user.profile.resources', defaultMessage: 'Resources' })}
          </Box>
          <Chips
            data={ressources.map((resource) => ({
              title: resource,
              href: `/search/members/?refinementList[ressources][0]=${resource}`,
            }))}
            overflowText="resourceSkill"
            // color={theme.colors.pink}
            // color={theme.colors.secondary}
            // color={theme.colors.greys['800']}
            // color={theme.colors.greys['300']}
            color="#eff7ff"
            // color="#c6c6c6"
            showCount={4}
          />
        </Box>
      )}
    </Box>
  );

  return (
    <div className="userHeader--top row">
      <div className="col-lg-2 col-12 d-none d-lg-block">
        <div className="userImg" style={logoStyle} onClick={showUserProfilePicture} />
      </div>

      <div className="col-lg-10 col-12 userInfos">
        <div className="infoContainer">
          <div className="userSmallImg">
            <div style={logoStyle} onClick={showUserProfilePicture} />
          </div>
          <div className="firstRow">
            <div className="nameInfos">
              <h1 className="title">{`${first_name} ${last_name}`}</h1>
              <p className="nickname">{`@${nickname}`}</p>
            </div>
            <Box display={['none', 'flex']}>{showSkillsAndResourcesChips()}</Box>
          </div>
        </div>
        <div className="userStats">
          {/* show follow button if user is not connected, or if he is connected but not the user viewed */}
          {(!userData || (userData && userData.id !== id)) && (
            <BtnFollow followState={has_followed} itemType="users" itemId={id} displayButton />
          )}
          {/* show contact button if user is connected, that he's not the viewed user, and that viewed user wants to be contacted */}
          {userData && userData.id !== id && can_contact !== false && (
            <Button
              btnType="secondary"
              onClick={() => {
                ReactGA.modalview('/send-message');
                showModal({
                  children: <ContactForm itemId={id} closeModal={() => setIsOpen(false)} />,
                  title: 'Send message to {userFullName}',
                  titleId: 'user.contactModal.title',
                  values: { userFullName: first_name + ' ' + last_name },
                });
              }}
            >
              {formatMessage({ id: 'user.btn.contact', defaultMessage: 'Contact' })}
            </Button>
          )}
          <div>
            <span className="text" tabIndex={0} onClick={openFollowersModal} onKeyUp={openFollowersModal}>
              <strong>{stats.follower_count}</strong>&nbsp;{textWithPlural('follower', stats.follower_count)}
            </span>
            <span className="text" tabIndex={0} onClick={openFollowingModal} onKeyUp={openFollowingModal}>
              <strong>{stats.following_count || 0}</strong>&nbsp;{textWithPlural('following', stats.following_count)}
            </span>
          </div>
        </div>
        <p className="about">{short_bio || bio}</p>
        <Box display={['flex', 'none']}>{showSkillsAndResourcesChips()}</Box>
      </div>

      {userData && userData.id === id && (
        <Box className="col-12 userActions" pt={[4, undefined, undefined, 0]}>
          <Link href={`/user/${id}/edit`}>
            <a>
              <FontAwesomeIcon icon="edit" />
              <FormattedMessage id="user.profile.edit.btnEdit" defaultMessage="Edit Profile" />
            </a>
          </Link>
        </Box>
      )}
    </div>
  );
};

const FollowingModal = ({ userId }) => {
  const { data: followings } = useGet(`/api/users/${userId}/following`);
  return <UserShowObjects list={followings} />;
};

export default UserHeader;
