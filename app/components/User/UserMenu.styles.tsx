import styled from '~/utils/styled';
import { MenuButton } from '@reach/menu-button';
import { DropDownMenu } from '../Header/Header.styles';

export const BtnUserMenu = styled(MenuButton)`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  box-shadow: 0 0 8px 3px rgba(0, 0, 0, 0.2);
  transition-duration: 200ms;
  background-size: cover;
  background-position: center;
  background-color: white;
  border: 3px solid white;
`;
export const Container = styled.div`
  min-width: 0 !important;

  .dropdown-toggle::after {
    display: none !important;
  }

  .btnUserMenu:hover {
    box-shadow: 0 0 8px 3px rgba(0, 0, 0, 0.4);
    transition-duration: 200ms;
  }

  .profileImg {
    width: 100%;
    padding: 3px;
    border-radius: 50%;
  }

  .signup {
    margin-right: 15px;
    font-weight: 600 !important;
    font-size: 17px;

    a {
      margin-right: 8px;
    }

    @media (min-width: 992px) {
      display: none;
    }

    @media (max-width: 400px) {
      display: none;
    }
  }

  .nav-link {
    color: white !important;
  }

  .dropdown-item {
    svg {
      color: grey;
    }
  }
`;

export const UserDropDownMenu = styled(DropDownMenu)`
  a,
  div {
    padding: 6px 18px;
    @media (max-width: ${(p) => p.theme.breakpoints.lg}) {
      padding: 10px 18px;
    }
  }
  div[role='none'] {
    padding: 0 !important;
  }
`;

export const Nav = styled.nav`
  margin-top: 0px;
  justify-content: flex-start;
  border-bottom: 1px solid #dee2e6;
  position: sticky;
  top: -1rem;
  background: white;
  margin-bottom: 1rem;
  z-index: 1;
  .nav-item {
    color: #5c5d5d;
    border: none !important;
    border-bottom: 2px solid transparent !important;
    font-size: 18px;
    font-weight: 500;
    width: fit-content;
    margin-bottom: 0;
    &.active,
    &:hover {
      border-bottom-color: #2987cd !important;
    }
  }
`;
