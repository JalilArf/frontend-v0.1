import { useRouter } from 'next/router';
import React, { useContext, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import Box from '~/components/Box';
import Button from '~/components/primitives/Button';
import P from '~/components/primitives/P';
import Alert from '~/components/Tools/Alert';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import { UserContext } from '~/contexts/UserProvider';
import Translate from '~/utils/Translate';
import ReactGA from 'react-ga';

const UserDelete = ({ userId }) => {
  const { showModal, setIsOpen } = useModal();
  const UserDeleteModal = () => {
    const api = useApi();
    const [reason, setReason] = useState('');
    const [errors, setErrors] = useState();
    const router = useRouter();
    const userContext = useContext(UserContext);
    const handleChange = (event) => {
      setReason(event.target.value);
    };

    const errorMessage = errors?.includes('err-') ? (
      <FormattedMessage id={errors} defaultMessage="An error has occurred" />
    ) : (
      errors
    );

    const deleteAccount = (event) => {
      event.preventDefault();
      const headers = {
        'access-token': userContext.credentials.accessToken,
        client: userContext.credentials.client,
        uid: userContext.credentials.uid,
      };
      if (reason) {
        // if user gave reason to delete account, send it to a JOGL admin via mail
        // @TODO find better way to manage this
        const param = {
          object: 'Account deletion explanation', // mail subject
          content: `The user ${userId} deleted their account with the following reason: ${reason}`, // mail content, with reason to leave message
        };
        // send it to user 2, which is JOGL admin user
        api
          .post('/api/users/2/send_email', param)
          .then(() => setReason('')) // reset reason
          .catch(() => setReason('')); // reset reason
      }
      // delete account
      api
        .delete(`/api/users`, { headers })
        .then(() => {
          ReactGA.event({ category: 'User', action: 'delete', label: `user ${userId}` }); // record event to Google Analytics
          userContext.logout(); // logout user
          setIsOpen(false); // close modal
          router.push('/'); // redirect to homepage
        })
        .catch((error) => {
          setErrors(error.toString()); // if errors, show them
        });
    };
    return (
      <>
        {errors && <Alert type="danger" message={errorMessage} />}
        <P fontWeight="600" fonSize="1.1rem" mb={0}>
          <FormattedMessage
            id="settings.account.delete.modal.message"
            defaultMessage="Are you sure to leave the JOGL community?"
          />
        </P>
        <P fontSize=".9rem" fontStyle="italic">
          <FormattedMessage
            id="settings.account.delete.modal.info"
            defaultMessage="The account will be archived for 30 days before a permanent deletion"
          />
        </P>
        <P fontSize=".95rem" mb={0}>
          <FormattedMessage
            id="settings.account.delete.modal.reason"
            defaultMessage="If you do want to go, could you tell us why? We want to learn from your own experience. (Optional)"
          />
        </P>
        <input type="text" className="form-control" id="reason" name="reason" onChange={handleChange} />
        <Box row spaceX={3} pt={3}>
          <Button btnType="danger" onClick={deleteAccount}>
            <FormattedMessage id="general.yes" defaultMessage="Yes" />
          </Button>
          <Button onClick={() => setIsOpen(false)}>
            <FormattedMessage id="general.no" defaultMessage="No" />
          </Button>
        </Box>
      </>
    );
  };
  return (
    <Button
      onClick={() => {
        showModal({
          children: <UserDeleteModal />,
          title: 'Delete my JOGL account',
          titleId: 'settings.account.delete.btn',
          maxWidth: '30rem',
        });
      }}
      btnType="danger"
    >
      <Translate id="settings.account.delete.btn" defaultMessage="Delete my JOGL account" />
    </Button>
  );
};
export default UserDelete;
