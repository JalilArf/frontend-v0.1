import Axios from 'axios';
import React, { Fragment, useContext, useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import Box from '~/components/Box';
import Grid from '~/components/Grid';
import FormToggleComponent from '~/components/Tools/Forms/FormToggleComponent';
import P from '~/components/primitives/P';
import { useApi } from '~/contexts/apiContext';
import { UserContext } from '~/contexts/UserProvider';
import { useTheme } from '@emotion/react';

interface Props {
  refresh?: () => {};
}

const UserNotificationsSettings = ({ refresh = undefined }: Props) => {
  const [notificationsSettings, setNotificationsSettings] = useState();
  const api = useApi();
  const user = useContext(UserContext);
  const intl = useIntl();
  const theme = useTheme();

  useEffect(() => {
    const axiosSource = Axios.CancelToken.source();
    if (user) {
      const fetchSettings = async () => {
        const res = await api.get(`/api/notifications/settings`, { cancelToken: axiosSource.token }).catch((err) => {
          if (!Axios.isCancel(err)) {
            console.error("Couldn't GET settings", err);
          }
        });
        if (res?.data?.settings) {
          setNotificationsSettings(res.data);
        }
      };
      fetchSettings();
    }
    return () => {
      axiosSource.cancel();
    };
  }, [api, user]);

  const saveSettings = () => {
    if (notificationsSettings) {
      // setIsSending(true);
      api
        .post(`/api/notifications/settings`, { settings: notificationsSettings.settings })
        .then(() => {
          // setIsSending(false);
        })
        .catch(() => {
          // setIsSending(false);
        });
      if (refresh !== undefined) {
        refresh();
      }
    }
  };

  const isDisabled = (category, delivery_method) => {
    if (notificationsSettings) {
      if (!notificationsSettings.settings.enabled) {
        return true;
      } else {
        if (delivery_method) {
          if (category) {
            if (!notificationsSettings.settings.categories[category].enabled) {
              return true;
            } else {
              if (!notificationsSettings.settings.delivery_methods[delivery_method].enabled) {
                return true;
              }
            }
          }
        }
      }
      return false;
    }
  };

  const handleChange = (category, delivery_method, isDisabled) => {
    var tempSettings = { ...notificationsSettings };
    if (!isDisabled) {
      if (category) {
        if (delivery_method) {
          const isCategoryMailEnabled =
            tempSettings.settings.categories[category].delivery_methods[delivery_method].enabled;
          tempSettings.settings.categories[category].delivery_methods[delivery_method].enabled = !isCategoryMailEnabled;
        } else {
          const isCategoryNotifEnabled = tempSettings.settings.categories[category].enabled;
          tempSettings.settings.categories[category].enabled = !isCategoryNotifEnabled;
        }
      } else {
        if (delivery_method) {
          const isGlobalMailEnabled = tempSettings.settings.delivery_methods[delivery_method].enabled;
          tempSettings.settings.delivery_methods[delivery_method].enabled = !isGlobalMailEnabled;
        } else {
          const isGlobalNotifEnabled = tempSettings.settings.enabled;
          tempSettings.settings.enabled = !isGlobalNotifEnabled;
        }
      }
      setNotificationsSettings(tempSettings);
      saveSettings();
    }
  };

  const notifCategories = notificationsSettings?.settings.categories;
  const isGeneralSiteNotifEnabled = notificationsSettings?.settings.enabled;
  const isGeneralEmailNotifEnabled = notificationsSettings?.settings.delivery_methods.email.enabled;

  return (
    <Box pt={4} pb={7}>
      <h3>
        {intl.formatMessage({
          id: 'settings.notifications',
          defaultMessage: 'Notifications settings',
        })}
      </h3>
      <P color={theme.colors.greys['700']}>
        {intl.formatMessage({
          id: 'settings.notifications.details',
          defaultMessage:
            'Manage notifications and news you receive for any object (projects, programs, challenges, groups & needs) you follow or are part of.',
        })}
      </P>

      {/* <Grid display="grid" gridTemplateColumns="minmax(130px, 1fr) 130px 130px" alignItems="center" pt={3}> */}
      <Grid
        display="grid"
        gridTemplateColumns={['minmax(130px, 1fr) 85px 85px', 'minmax(130px, 1fr) 105px 105px', '385px 105px 105px']}
        alignItems="center"
        pt={5}
      >
        {/* table header */}
        <Box fontWeight="bold" fontSize={3}>
          {intl.formatMessage({
            id: 'settings.notifications.type',
            defaultMessage: 'Type',
          })}
        </Box>
        <Box textAlign="right" fontWeight="bold" fontSize={3}>
          {intl.formatMessage({
            id: 'settings.notifications.jogl',
            defaultMessage: 'On JOGL',
          })}
        </Box>
        <Box textAlign="right" fontWeight="bold" fontSize={3}>
          {intl.formatMessage({
            id: 'settings.notifications.email',
            defaultMessage: 'Email',
          })}
        </Box>

        {/* table first row (global settings) */}
        <Box pt={4} pb={5}>
          <Box fontWeight="600">
            {intl.formatMessage({
              id: 'settings.notifications.all',
              defaultMessage: 'All',
            })}
          </Box>
          <Box color={theme.colors.greys['700']}>
            {intl.formatMessage({
              id: 'settings.notifications.all.details',
              defaultMessage: 'Enable or disable all notification at once',
            })}
          </Box>
        </Box>
        <Box pt={4} pb={5}>
          <FormToggleComponent
            toggleType="notif"
            id="notifG"
            isChecked={isGeneralSiteNotifEnabled}
            onChange={() => handleChange(undefined, undefined, undefined)}
          />
        </Box>
        <Box pt={4} pb={5}>
          <FormToggleComponent
            toggleType="notif"
            id="mailG"
            isChecked={isGeneralEmailNotifEnabled}
            isDisabled={isDisabled(undefined, 'email')}
            onChange={() => handleChange(undefined, 'email', isDisabled(undefined, 'email'))}
          />
        </Box>

        {/* other table rows (one for each detailed notification setting) */}
        {notificationsSettings &&
          Object.keys(notifCategories)
            // temporary remove "notification" category which are notif that don't fit in other categories (as we don't have for now)
            // also remove "space" category as it's not yet ready/functional
            .filter((category) => category !== 'notification' && category !== 'space')
            .map((category, i) => {
              const isNotifChecked = notifCategories[category].enabled;
              const isMailChecked = notifCategories[category].delivery_methods.email.enabled;
              return (
                <Fragment key={i}>
                  <Box py={2} pr={[0, undefined, 4]}>
                    <Box fontWeight="600">
                      {intl.formatMessage({
                        id: `settings.notifications.${category}`,
                        defaultMessage: category,
                      })}
                    </Box>
                    <Box color={theme.colors.greys['700']}>
                      {intl.formatMessage({
                        id: `settings.notifications.${category}.details`,
                        defaultMessage: `${category} explanation`,
                      })}
                    </Box>
                  </Box>
                  <FormToggleComponent
                    toggleType="notif"
                    id="notifC"
                    isChecked={isNotifChecked}
                    isDisabled={isDisabled(category, undefined)}
                    onChange={() => handleChange(category, undefined, isDisabled(category, undefined))}
                  />
                  <FormToggleComponent
                    toggleType="notif"
                    id="mailC"
                    isChecked={isMailChecked}
                    isDisabled={isDisabled(category, 'email')}
                    onChange={() => handleChange(category, 'email', isDisabled(category, 'email'))}
                  />
                </Fragment>
              );
            })}
      </Grid>
    </Box>
  );
};

export default UserNotificationsSettings;
