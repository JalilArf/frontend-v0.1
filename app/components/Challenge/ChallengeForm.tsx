import { FC, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Link from 'next/link';
/** * Form objects ** */
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormTextAreaComponent from '~/components/Tools/Forms/FormTextAreaComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from '~/components/Tools/Forms/FormValidator';
import challengeFormRules from './challengeFormRules.json';
import Box from '../Box';
import Alert from '../Tools/Alert';
import { Challenge } from '~/types';
/** * Images/Style ** */

interface Props {
  mode: 'edit' | 'create';
  challenge: Challenge;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const ChallengeForm: FC<Props> = ({
  mode,
  challenge,
  sending = false,
  hasUpdated = false,
  handleChange,
  handleSubmit,
}) => {
  const { formatMessage } = useIntl();
  const validator = new FormValidator(challengeFormRules);
  const [stateValidation, setStateValidation] = useState({});
  const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } =
    stateValidation || '';

  const handleChangeChallenge = (key, content) => {
    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = validator.validate(challenge);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
    handleChange(key, content);
  };

  const handleSubmitChallenge = () => {
    /* Validators control before submit */
    const validation = validator.validate(challenge);
    if (validation.isValid) {
      handleSubmit();
    } else {
      const newStateValidation = {};
      let firstError = true;
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 130; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };

  const renderBtnsForm = () => {
    const textAction = mode === 'edit' ? 'Update' : 'Create';
    const urlBack = mode === 'edit' ? `/challenge/${challenge.short_title}` : '/search/challenges';

    return (
      <>
        <Box row justifyContent="center" mt={5} mb={3}>
          <Link href={urlBack}>
            <a>
              <button type="button" className="btn btn-outline-primary">
                <FormattedMessage id="entity.form.back" defaultMessage="Back" />
              </button>
            </a>
          </Link>
          <button
            type="button"
            onClick={handleSubmitChallenge}
            className="btn btn-primary"
            disabled={sending}
            style={{ marginLeft: '10px' }}
          >
            {sending && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id={`entity.form.btn${textAction}`} defaultMessage={textAction} />
          </button>
        </Box>
        {hasUpdated && (
          <Alert
            type="success"
            message={
              <FormattedMessage
                id="general.editSuccessMsg"
                defaultMessage="The changes have been saved successfully."
              />
            }
          />
        )}
      </>
    );
  };

  return (
    <form className="challengeForm">
      <FormDefaultComponent
        content={challenge.title}
        errorCodeMessage={valid_title ? valid_title.message : ''}
        id="title"
        isValid={valid_title ? !valid_title.isInvalid : undefined}
        mandatory
        onChange={handleChangeChallenge}
        title={formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
        placeholder={formatMessage({
          id: 'challenge.form.title.placeholder',
          defaultMessage: 'A great challenge',
        })}
      />
      <FormDefaultComponent
        content={challenge.title_fr}
        id="title_fr"
        onChange={handleChangeChallenge}
        title={formatMessage({ id: 'entity.info.title_fr', defaultMessage: 'Title fr' })}
        placeholder={formatMessage({
          id: 'challenge.form.title.placeholder',
          defaultMessage: 'A great challenge',
        })}
      />
      <FormDefaultComponent
        content={challenge.short_title}
        errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
        id="short_title"
        isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
        onChange={handleChangeChallenge}
        mandatory
        pattern={/[A-Za-z0-9]/g}
        title={formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
        placeholder={formatMessage({
          id: 'challenge.form.short_title.placeholder',
          defaultMessage: 'agreatchallenge',
        })}
        prepend="#"
      />
      <FormTextAreaComponent
        content={challenge.short_description}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        id="short_description"
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory
        maxChar={340}
        onChange={handleChangeChallenge}
        rows={5}
        title={formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
        placeholder={formatMessage({
          id: 'challenge.form.short_description.placeholder',
          defaultMessage: 'The challenge briefly explained',
        })}
      />
      <FormTextAreaComponent
        content={challenge.short_description_fr}
        id="short_description_fr"
        maxChar={340}
        onChange={handleChangeChallenge}
        rows={3}
        title={formatMessage({ id: 'entity.info.short_description_fr', defaultMessage: 'Short description fr' })}
        placeholder={formatMessage({
          id: 'challenge.form.short_description.placeholder',
          defaultMessage: 'The challenge briefly explained',
        })}
      />
      {mode === 'edit' && (
        <>
          <FormWysiwygComponent
            id="description"
            title={formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
            placeholder={formatMessage({
              id: 'challenge.form.description.placeholder',
              defaultMessage: 'Describe the challenge in detail, with formatted text, images...',
            })}
            content={challenge.description}
            onChange={handleChangeChallenge}
            show
          />
          <FormWysiwygComponent
            id="description_fr"
            title={formatMessage({ id: 'entity.info.description_fr', defaultMessage: 'Description fr' })}
            placeholder={formatMessage({
              id: 'challenge.form.description.placeholder',
              defaultMessage: 'Describe the challenge in detail, with formatted text, images...',
            })}
            content={challenge.description_fr}
            onChange={handleChangeChallenge}
          />
        </>
      )}

      <FormInterestsComponent
        content={challenge.interests}
        errorCodeMessage={valid_interests ? valid_interests.message : ''}
        mandatory
        onChange={handleChangeChallenge}
        title={formatMessage({ id: 'entity.info.interests', defaultMessage: 'Interests' })}
      />
      <FormSkillsComponent
        content={challenge.skills}
        errorCodeMessage={valid_skills ? valid_skills.message : ''}
        id="skills"
        type="challenge"
        isValid={valid_skills ? !valid_skills.isInvalid : undefined}
        mandatory
        onChange={handleChangeChallenge}
        placeholder={formatMessage({
          id: 'general.skills.placeholder',
          defaultMessage: 'Big data, Web Development, Open Science...',
        })}
        title={formatMessage({ id: 'entity.info.skills', defaultMessage: 'Expected skills' })}
      />
      {mode === 'edit' && (
        <>
          {/* <FormWysiwygComponent
              id="rules"
              title={formatMessage({ id: 'entity.info.rules', defaultMessage: 'Rules' })}
              placeholder={formatMessage({
                id: 'challenge.form.rules.placeholder',
                defaultMessage: 'Describe the rules to be able to participate to the challenge, to submit projects...',
              })}
              content={challenge.rules}
              onChange={handleChangeChallenge}
            />
            <FormWysiwygComponent
              id="rules_fr"
              title={formatMessage({ id: 'entity.info.rules_fr', defaultMessage: 'Rules fr' })}
              placeholder={formatMessage({
                id: 'challenge.form.rules.placeholder',
                defaultMessage: 'Describe the rules to be able to participate to the challenge, to submit projects...',
              })}
              content={challenge.rules_fr}
              onChange={handleChangeChallenge}
            /> */}
          {/* <FormWysiwygComponent
              id="faq"
              title={formatMessage({ id: 'entity.info.faq', defaultMessage: 'Faq' })}
              placeholder={formatMessage({
                id: 'challenge.form.faq.placeholder',
                defaultMessage: 'Write the FAQ...',
              })}
              content={challenge.faq}
              onChange={handleChangeChallenge}
            />
            <FormWysiwygComponent
              id="faq_fr"
              title={formatMessage({ id: 'entity.info.faq_fr', defaultMessage: 'Faq fr' })}
              placeholder={formatMessage({
                id: 'challenge.form.faq.placeholder',
                defaultMessage: 'Write the FAQ...',
              })}
              content={challenge.faq_fr}
              onChange={handleChangeChallenge}
            /> */}
          <FormDropdownComponent
            id="status"
            type="challenge"
            warningMsgIntl={{
              id: 'challenge.info.dropDownMsg',
              defaultMessage:
                "As long as the status of this challenge is on 'Draft', it won't be visible on the platform.",
            }}
            title={formatMessage({ id: 'entity.info.status', defaultMessage: 'Status' })}
            content={challenge.status}
            options={['draft', 'soon', 'accepting', 'evaluating', 'active', 'completed']}
            onChange={handleChangeChallenge}
          />
          <FormImgComponent
            type="banner"
            id="banner_url"
            imageUrl={challenge.banner_url}
            itemId={challenge.id}
            itemType="challenges"
            title={formatMessage({ id: 'challenge.info.banner_url', defaultMessage: 'Challenge banner' })}
            content={challenge.banner_url}
            defaultImg="/images/default/default-challenge.jpg"
            onChange={handleChangeChallenge}
            tooltipMessage={formatMessage({
              id: 'challenge.info.banner_url.tooltip',
              defaultMessage:
                'For an optimal display, choose a visual in the format 1240 x 400 pixels (accepted formats: .png, .jpeg, .jpg; maximum weight: 2Mo).',
            })}
          />
          <FormImgComponent
            id="logo_url"
            content={challenge.logo_url}
            title={formatMessage({ id: 'challenge.info.logo_url', defaultMessage: 'Challenge logo' })}
            imageUrl={challenge.logo_url}
            itemId={challenge.id}
            type="avatar"
            itemType="challenges"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChangeChallenge}
          />
          <FormDefaultComponent
            id="launch_date"
            content={challenge.launch_date && challenge.launch_date.split('T')[0]}
            onChange={handleChangeChallenge}
            type="date"
            title={formatMessage({ id: 'entity.info.launch_date', defaultMessage: 'Launch date' })}
          />
          <FormDefaultComponent
            id="final_date"
            content={challenge.final_date && challenge.final_date.split('T')[0]}
            onChange={handleChangeChallenge}
            type="date"
            title={formatMessage({ id: 'entity.info.final_date', defaultMessage: 'Final date' })}
          />
          <FormDefaultComponent
            id="end_date"
            content={challenge.end_date && challenge.end_date.split('T')[0]}
            onChange={handleChangeChallenge}
            type="date"
            title={formatMessage({ id: 'entity.info.end_date', defaultMessage: 'End date' })}
          />
          {/* <FormDefaultComponent
              id="address"
              title={formatMessage({ id: 'general.address', defaultMessage: 'Address' })}
              placeholder={formatMessage({
                id: 'general.address.placeholder',
                defaultMessage: '5620 Impact Street',
              })}
              content={challenge.address}
              onChange={handleChangeChallenge}
            /> */}
          {/* <FormDefaultComponent
              id="city"
              title={formatMessage({ id: 'general.city', defaultMessage: 'City' })}
              placeholder={formatMessage({
                id: 'general.city.placeholder',
                defaultMessage: 'Bangkok, Paris, Medellin...',
              })}
              content={challenge.city}
              onChange={handleChangeChallenge}
            />
            <FormDefaultComponent
              id="country"
              title={formatMessage({ id: 'general.country', defaultMessage: 'Country' })}
              placeholder={formatMessage({
                id: 'general.country.placeholder',
                defaultMessage: 'India, Ouganda, Brazil, France..',
              })}
              content={challenge.country}
              onChange={handleChangeChallenge}
            /> */}
        </>
      )}
      {renderBtnsForm()}
    </form>
  );
};
export default ChallengeForm;
