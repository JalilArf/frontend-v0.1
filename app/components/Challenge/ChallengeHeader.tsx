import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC, useContext } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import H1 from '~/components/primitives/H1';
import { useModal } from '~/contexts/modalContext';
import { UserContext } from '~/contexts/UserProvider';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Challenge } from '~/types/challenge';
import { textWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';
import Translate from '~/utils/Translate';
import Box from '../Box';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import { ProjectLinkModal } from '../Project/ProjectLinkModal';
import BtnFollow from '../Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import BtnSave from '../Tools/BtnSave';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import { useTheme } from '~/utils/theme';
import { StatusCircle, statusToStep } from './ChallengeStatus';
import P from '../primitives/P';
// import "./ChallengeHeader.scss";

const StatusStep = styled(Box)`
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    transform: rotate(135deg) translateY(-5px) translateX(6px) !important;
  }
`;
interface Props {
  challenge: Challenge;
  lang: string;
}
const ChallengeHeader: FC<Props> = ({ challenge, lang = 'en' }) => {
  const { formatMessage } = useIntl();
  const user = useContext(UserContext);
  const router = useRouter();
  const { userData } = useUserData();
  const { showModal, setIsOpen } = useModal();
  const theme = useTheme();
  const { data: projectsData, mutate: mutateProjects } = useGet(`/api/challenges/${challenge.id}/projects`);

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const Stats = ({ value, title }) => (
    <Box justifyContent="center" alignItems="center">
      <div>{value}</div>
      <div>{title}</div>
    </Box>
  );

  const {
    has_followed,
    has_saved,
    id,
    is_member,
    is_owner,
    members_count,
    projects_count,
    needs_count,
    status,
    title,
    title_fr,
    program,
    short_description,
    short_description_fr,
  } = challenge;

  // dynamically set challengeStatus to completed if date is over and admin forgot to set it as complete, else show normal status
  // const challengeStatus = final_date && getDaysLeft(final_date) === 0 ? 'completed' : status;
  const statusStep = statusToStep(status);
  return (
    <div className="challengeHeader">
      <Box px={[3, undefined, undefined, undefined, 0]}>
        <Box alignItems="center" width={'full'} mb={[6, undefined, undefined, 8]} pt={5}>
          <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
            <H1 fontSize={['2.55rem', undefined, '3rem']}>
              <FormattedMessage id="challenge" defaultMessage="Challenge" />
              &nbsp;
              <span>{`{${lang === 'fr' && title_fr ? title_fr : title}}`}</span>
            </H1>
            <Box row pl={5} alignSelf="center">
              {userData && (
                <>
                  <Box pr={5}>
                    <BtnSave itemType="challenges" itemId={id} saveState={has_saved} />
                  </Box>
                  <BtnFollow followState={has_followed} itemType="challenges" itemId={id} />
                </>
              )}
              <ShareBtns type="challenge" />
            </Box>
          </Box>

          {/* edit button */}
          {challenge.is_admin && (
            <Box mt={4}>
              <Link href={`/challenge/${challenge.short_title}/edit`}>
                <a style={{ display: 'flex', justifyContent: 'center' }}>
                  <FontAwesomeIcon icon="edit" />
                  <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
                </a>
              </Link>
            </Box>
          )}
        </Box>
        <Grid display="grid" gridTemplateColumns={['100%', undefined, undefined, '22% 50% 28%']}>
          <Box
            row
            alignSelf="flex-start"
            alignItems="center"
            justifyContent="center"
            justifySelf="center"
            spaceX={4}
            flexWrap="wrap"
            order={[2, undefined, undefined, 1]}
          >
            <Box row pb={3}>
              {program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
                <>
                  <strong>
                    <FormattedMessage id="entity.info.program.title" defaultMessage="Program: " />
                  </strong>
                  &nbsp;
                  <span>
                    <Link href={`/program/${program.short_title}`}>
                      <a>{program.title}</a>
                    </Link>
                  </span>
                </>
              )}
            </Box>
            {/* where statusStep[0] = step number, and statusStep[1] = step color */}
            <Box textAlign={['center', undefined, 'inherit']}>
              <Grid
                display="grid"
                gridTemplateColumns={['27% 53%', undefined, undefined, '33% 53%']}
                justifyContent={['center', undefined, undefined, 'flex-start']}
                alignItems="center"
                width="12rem"
                margin={['auto', undefined, undefined, 'inherit']}
                style={{ paddingBottom: '15px' }}
              >
                {status !== 'draft' && (
                  <>
                    <StatusCircle mr={2} step={statusStep[0]}>
                      <StatusStep>{statusStep[0]}/4</StatusStep>
                    </StatusCircle>
                    <Box color={statusStep[1]}>
                      {formatMessage({
                        id: `challenge.info.status.${status}`,
                        defaultMessage: status,
                      })}
                    </Box>
                  </>
                )}
              </Grid>
            </Box>
          </Box>
          <Box textAlign="center" px={4} order={[1, undefined, undefined, 2]}>
            <P fontSize="17.5px">{lang === 'fr' && short_description_fr ? short_description_fr : short_description}</P>
          </Box>
          <Box order={3} pt={[4, undefined, undefined, 0]}>
            <Box row spaceX={2} justifyContent={['center', undefined, undefined, 'flex-end']} alignItems="center">
              {!is_owner && ( // show join button only if we are not owner of challenge
                <BtnJoin
                  joinState={is_member}
                  itemType="challenges"
                  itemId={id}
                  textJoin={<FormattedMessage id="challenge.info.btnJoin" defaultMessage="Join challenge" />}
                  textUnjoin={<FormattedMessage id="challenge.info.btnUnjoin" defaultMessage="Leave challenge" />}
                  // onJoin={() => router.push(`/challenges/${id}`)}
                />
              )}
              {/* show submit button only for connected users, and only if status of challenge is accepting projects + challenge is attached to a program */}
              {user.isConnected && projectsData && status === 'accepting' && program.id !== -1 && (
                <Button
                  // eslint-disable-next-line react/jsx-no-bind
                  onClick={() => {
                    showModal({
                      children: (
                        <ProjectLinkModal
                          alreadyPresentProjects={projectsData?.projects}
                          challengeId={id}
                          programId={program.id}
                          mutateProjects={mutateProjects}
                          // eslint-disable-next-line react/jsx-no-bind
                          closeModal={() => setIsOpen(false)}
                        />
                      ),
                      title: 'Submit a project',
                      titleId: 'attach.project.title',
                      maxWidth: '50rem',
                    });
                  }}
                  btnType="secondary"
                >
                  <Translate id="attach.project.title" defaultMessage="Submit a project" />
                </Button>
              )}
            </Box>

            <Box
              textAlign="center"
              alignItems="center"
              justifyContent="space-around"
              spaceX={[4, 6]}
              row
              pt={[4, undefined, 6]}
              pb={2}
              px={[0, undefined, 2]}
              width={['100%', '30rem', undefined, '100%']}
              alignSelf="center"
            >
              <A href={`/challenge/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
                <Stats value={projects_count} title={textWithPlural('project', projects_count)} />
              </A>
              <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8} />
              <A href={`/challenge/${router.query.short_title}?tab=needs`} shallow noStyle scroll={false}>
                <Stats value={needs_count} title={textWithPlural('need', needs_count)} />
              </A>
              <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8} />
              <A href={`/challenge/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
                <Stats value={members_count} title={textWithPlural('participant', members_count)} />
              </A>
            </Box>
          </Box>
        </Grid>
      </Box>
    </div>
  );
};
export default ChallengeHeader;
