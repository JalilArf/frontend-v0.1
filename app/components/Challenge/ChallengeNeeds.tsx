import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Need } from '~/types';
import Box from '../Box';
import Grid from '../Grid';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import Button from '../primitives/Button';
import P from '../primitives/P';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

const ChallengeNeeds = ({ challengeId }) => {
  const needsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  const { data: dataNeeds, response } = useGet<{ needs: Need[] }>(
    `/api/challenges/${challengeId}/needs?items=${needsPerQuery}`
  );
  const [needs, setNeeds] = useState([]);
  const { userData } = useUserData();
  const [currentPage, setCurrentPage] = useState(2);
  const [hasLoadOnce, setHasLoadOnce] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const api = useApi();
  useEffect(() => {
    // set dataNeeds from first api call, only once, or when filtering needs by challenge
    if (!hasLoadOnce && dataNeeds?.needs) setNeeds(dataNeeds.needs);
  }, [dataNeeds]);

  const loadMore = () => {
    setIsLoading(true);
    api.get(`/api/challenges/${challengeId}/needs?items=${needsPerQuery}&page=${currentPage}`).then((res) => {
      setIsLoading(false);
      const nextNeeds = res.data.needs;
      if (needs) setNeeds([...needs, ...nextNeeds]);
      setHasLoadOnce(true); // set to true so it doesn't setNeeds from first call
      setCurrentPage(currentPage + 1); // increment current page count
    });
  };
  return (
    <div>
      <Box>
        <P>
          <FormattedMessage
            id="program.needs.text"
            defaultMessage="To participate to this challenge, participate to as many of its needs. Check out the needs already submitted, contribute to them or create your own!"
          />
        </P>
        {!userData && ( // if user is not connected
          <A href="/signIn">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toFillNeed}'}
              values={{
                toFillNeed: <FormattedMessage id="program.signinCta.need" defaultMessage="to fill a need" />,
              }}
            />
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        {!dataNeeds ? (
          <Loading />
        ) : needs?.length === 0 ? (
          <NoResults type="need" />
        ) : (
          <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} pb={4}>
            {needs?.map((need, i) => (
              <NeedCard
                key={i}
                title={need.title}
                project={need.project}
                skills={need.skills}
                resources={need.ressources}
                hasSaved={need.has_saved}
                id={need.id}
                postsCount={need.posts_count}
                membersCount={need.members_count}
                publishedDate={need.created_at}
                dueDate={need.end_date}
                status={need.status}
              />
            ))}
          </Grid>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          response?.headers['total-count'] > needsPerQuery && currentPage <= response?.headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={loadMore} disabled={isLoading}>
                {isLoading && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id="general.load" defaultMessage="Load more" />
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default ChallengeNeeds;
