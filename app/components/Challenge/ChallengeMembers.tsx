import React, { useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import Box from '../Box';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import A from '../primitives/A';
import { useApi } from '~/contexts/apiContext';
import QuickSearchBar from '../Tools/QuickSearchBar';
import Loading from '../Tools/Loading';
import Button from '../primitives/Button';

const ChallengeMembers = ({ challengeId }) => {
  const membersPerQuery = 24; // number of members we get per query calls (make it 3 to test locally)
  const { data: dataMembers, response } = useGet(`/api/challenges/${challengeId}/members?items=${membersPerQuery}`);
  const [members, setMembers] = useState([]);
  const { userData } = useUserData();
  const [currentPage, setCurrentPage] = useState(2);
  const [hasLoadOnce, setHasLoadOnce] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const api = useApi();
  useEffect(() => {
    // set dataMembers from first api call, only once, or when filtering members by challenge
    if (!hasLoadOnce && dataMembers?.members) setMembers(dataMembers.members);
  }, [dataMembers]);

  const loadMore = () => {
    setIsLoading(true);
    api.get(`/api/challenges/${challengeId}/members?items=${membersPerQuery}&page=${currentPage}`).then((res) => {
      setIsLoading(false);
      const nextMembers = res.data.members;
      if (members) setMembers([...members, ...nextMembers]);
      setHasLoadOnce(true); // set to true so it doesn't setMembers from first call
      setCurrentPage(currentPage + 1); // increment current page count
    });
  };

  return (
    <>
      {!userData && ( // if user is not connected
        <Box spaceX={2} pb={4}>
          <A href="/signin">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toContactMembers}'}
              values={{
                toContactMembers: (
                  <FormattedMessage id="challenge.signinCta.members" defaultMessage="to contact members" />
                ),
              }}
            />
          </A>
        </Box>
      )}
      <Box position="relative">
        {/* Members grid/list */}
        {members && (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid gridGap={[4, undefined, 5]} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} pb={4}>
              {members.length === 0 ? (
                <Loading />
              ) : (
                members?.map((member, i) => (
                  <UserCard
                    key={i}
                    id={member.id}
                    firstName={member.first_name}
                    lastName={member.last_name}
                    nickName={member.nickname}
                    shortBio={member.short_bio}
                    skills={member.skills}
                    resources={member.ressources}
                    status={member.status}
                    lastActive={member.current_sign_in_at}
                    logoUrl={member.logo_url}
                    canContact={member.can_contact}
                    mutualCount={member.stats.mutual_count}
                    projectsCount={member.stats.projects_count}
                    noMobileBorder={false}
                  />
                ))
              )}
            </Grid>
            {
              // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
              response?.headers['total-count'] > membersPerQuery && currentPage <= response?.headers['total-pages'] && (
                <Box alignSelf="center" pt={4}>
                  <Button onClick={loadMore} disabled={isLoading}>
                    {isLoading && (
                      <>
                        <span
                          className="spinner-border spinner-border-sm text-center"
                          role="status"
                          aria-hidden="true"
                        />
                        &nbsp;
                      </>
                    )}
                    <FormattedMessage id="general.load" defaultMessage="Load more" />
                  </Button>
                </Box>
              )
            }
          </>
        )}
      </Box>
    </>
  );
};

export default ChallengeMembers;
