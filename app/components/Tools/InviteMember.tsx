// eslint-disable-next-line no-unused-vars
import { useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import Button from '../primitives/Button';
import Box from '../Box';
import ReactGA from 'react-ga';

export default function InviteMember({ itemType = '', itemId }) {
  const [emailInput, setEmailInput] = useState('');
  const [disabledBtn, setDisabledBtn] = useState(false);
  const [inviteSend, setInviteSend] = useState(false);
  const [missInput, setMissInput] = useState(false);
  const [error, setError] = useState(false);
  const api = useApi();
  const intl = useIntl();
  const handleChangeEmail = (event) => {
    setEmailInput(event.target.value);
    setMissInput(false);
  };
  const resetState = () => {
    setEmailInput('');
    setDisabledBtn(false);
    setInviteSend(false);
    setMissInput(false);
    setError(false);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    if (
      itemType === 'projects' ||
      itemType === 'communities' ||
      itemType === 'challenges' ||
      itemType === 'programs' ||
      itemType === 'spaces'
    ) {
      if (itemId) {
        if (emailInput !== '') {
          setDisabledBtn(true);
          let params = {};
          params = { stranger_email: emailInput };
          api
            .post(`/api/${itemType}/${itemId}/invite`, params)
            .then((res) => {
              const userId = res.config.headers.userId;
              // send event to google analytics
              ReactGA.event({ category: 'Invite', action: 'invite user', label: `[${userId},${itemId},${itemType}]` });
              setInviteSend(true);
              setTimeout(() => {
                resetState();
              }, 1500);
            })
            .catch(() => {
              setError(true);
              setTimeout(() => {
                setDisabledBtn(false);
                setError(false);
              }, 8000);
            });
        } else {
          setMissInput(true);
          setTimeout(() => {
            setMissInput(false);
          }, 4000);
        }
      } else {
        console.warn('itemId is missing');
      }
    } else {
      console.warn('itemType not compatible');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <Box pb={2} pt={2}>
        <input
          type="email"
          className={`form-control ${missInput && 'is-invalid'}`}
          id="byEmail"
          placeholder={intl.formatMessage({
            id: 'member.invite.mail.placeholder',
            defaultMessage: 'Enter email',
          })}
          value={emailInput}
          style={{ maxWidth: '300px' }}
          onChange={handleChangeEmail}
        />
        {missInput && (
          <div className="invalid-feedback">
            <FormattedMessage id="member.invite.mail.empty" defaultMessage="Type an email here" />
          </div>
        )}
      </Box>
      <Button type="submit" disabled={disabledBtn}>
        {inviteSend ? (
          <FormattedMessage id="member.invite.btnSendEnded" defaultMessage="Invitation sent" />
        ) : (
          <FormattedMessage id="member.invite.btnSend" defaultMessage="Send invitation" />
        )}
      </Button>
      {error && (
        <div className="alert alert-danger" role="alert">
          <FormattedMessage id="err-" defaultMessage="An error has occurred" />
        </div>
      )}
    </form>
  );
}
