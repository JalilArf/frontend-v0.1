import React, { FC, FormEvent, useState } from 'react';
import { useIntl } from 'react-intl';
import Box from '~/components/Box';
import Button from '~/components/primitives/Button';
import H2 from '~/components/primitives/H2';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import { Resource } from '~/types';
import Card from '../Card';
import P from '../primitives/P';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormWysiwygComponent from './Forms/FormWysiwygComponent';
import Loading from './Loading';

const ManageResources = ({ itemType, itemId }) => {
  const { formatMessage } = useIntl();
  const { data: resourceList, mutate: resourceListMutate } = useGet<Resource[]>(`/api/${itemType}/${itemId}/resources`);
  const [showResourceCreate, setShowResourceCreate] = useState(false);
  const addResource = () => {
    setShowResourceCreate(!showResourceCreate);
  };
  return (
    <Box spaceY={3}>
      <H2>{formatMessage({ id: 'program.resourcesTitle', defaultMessage: 'Resources' })}</H2>
      <Button onClick={addResource}>{formatMessage({ id: 'general.add', defaultMessage: 'Add' })}</Button>
      {showResourceCreate && (
        <ResourceFormCard
          mode="create"
          itemType={itemType}
          itemId={itemId}
          onCreate={(newResource: Resource) => resourceListMutate({ data: [...resourceList, newResource] }, false)}
        />
      )}
      <Box spaceY={3} pt={8}>
        {!resourceList ? (
          <Loading />
        ) : (
          resourceList
            ?.sort(function (a, b) {
              return a.id - b.id; // sort by id (asc)
            })
            .map((value, i) => (
              <Box key={i} pt={6}>
                <P pl={2} style={{ fontSize: '2rem' }}>
                  {i + 1}
                </P>
                <ResourceFormCard
                  value={value}
                  itemType={itemType}
                  itemId={itemId}
                  mode="edit"
                  onUpdate={(updatedResource: Resource) => {
                    resourceListMutate(
                      {
                        data: resourceList.map((resource) => {
                          if (resource.id === updatedResource.id) return updatedResource;
                          return resource;
                        }),
                      },
                      false
                    );
                  }}
                  onDelete={(resourceId: number) => {
                    resourceListMutate(
                      {
                        // removes the resource which has the same id as resourceId
                        data: resourceList.filter((resource) => resource.id !== resourceId),
                      },
                      false
                    );
                  }}
                />
              </Box>
            ))
        )}
      </Box>
    </Box>
  );
};
interface IResourceFormCard {
  value?: Resource;
  mode: 'create' | 'edit';
  itemType: string;
  itemId: number;
  onCreate?: (newResource: Resource) => void;
  onUpdate?: (updatedResource: Resource) => void;
  onDelete?: (resourceId: number) => void;
}
const ResourceFormCard: FC<IResourceFormCard> = ({
  value = { title: '', content: '', id: undefined },
  mode,
  itemType,
  itemId,
  onCreate,
  onUpdate,
  onDelete,
}) => {
  const api = useApi();
  const { formatMessage } = useIntl();
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  const [resource, setResource] = useState<{ resource: Resource }>({
    resource: {
      id: value.id,
      title: value.title,
      content: value.content,
    },
  });
  // handle every changes to the resource
  const handleChange: (key: number, content: string) => void = (key, content) => {
    setResource((prevResource) => ({ resource: { ...prevResource.resource, [key]: content } }));
    setShowSubmitBtn(content !== resource.resource.title || content !== resource.resource.content);
  };
  // handle when submitting (update, or create), an resource
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (mode === 'edit') {
      api.patch(`/api/${itemType}/${itemId}/resources/${resource.resource.id}`, resource).then((res) => {
        onUpdate(resource.resource); // call onUpdate function
        setShowSubmitBtn(false); // hide update button
      });
    }
    if (mode === 'create') {
      api.post(`/api/${itemType}/${itemId}/resources`, resource).then((res) => {
        onCreate(resource.resource); // call onCreate function
        setResource({ resource: { title: '', content: '', id: undefined } }); // reset resource fields
      });
    }
  };
  // handle when deleting on resource
  const handleDelete = () => {
    api.delete(`/api/${itemType}/${itemId}/resources/${resource.resource.id}`).then((res) => {
      onDelete(resource.resource.id); // call onDelete function
    });
  };
  const intlTranslation =
    mode === 'create'
      ? { id: 'entity.form.btnCreate', defaultMessage: 'Create' }
      : { id: 'entity.form.btnUpdate', defaultMessage: 'Update' };
  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <Box flexDirection={['column', undefined, 'row']} justifyContent="space-between">
          <Box spaceY={4} width={'100%'} pr={[0, undefined, 5]}>
            <Box>
              <FormDefaultComponent
                id="title"
                content={resource.resource.title}
                title={formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
                placeholder={formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
                onChange={handleChange}
              />
            </Box>
            <Box>
              <FormWysiwygComponent
                id="content"
                content={resource.resource.content}
                title={formatMessage({ id: 'program.resources.content', defaultMessage: 'Content' })}
                placeholder={formatMessage({
                  id: 'program.resource.content.placeholder',
                  defaultMessage: 'Describe your resource',
                })}
                onChange={handleChange}
                show
              />
            </Box>
          </Box>
          <Box spaceY={2} justifyContent="center" alignItems="flex-end">
            {(showSubmitBtn || mode === 'create') && <Button type="submit">{formatMessage(intlTranslation)}</Button>}
            {mode !== 'create' && (
              <Button type="button" btnType="danger" onClick={handleDelete}>
                {formatMessage({ id: 'feed.object.delete', defaultMessage: 'Delete' })}
              </Button>
            )}
          </Box>
        </Box>
      </form>
    </Card>
  );
};

export default ManageResources;
