import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, ReactNode, useCallback, useEffect, useState, Fragment } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import { DataSource, ItemType } from '~/types';
import styled from '~/utils/styled';
import Button from '../primitives/Button';
import ReactTooltip from 'react-tooltip';
import Box from '../Box';
import ReactGA from 'react-ga';
import useUser from '~/hooks/useUser';
import Axios from 'axios';

const Bell = styled(FontAwesomeIcon)`
  cursor: pointer;
  font-size: 1.5rem !important;
  color: #da9c00;
  opacity: 0.8;
`;

interface Props {
  btnType?: string;
  displayButton?: boolean;
  followState: boolean;
  itemType: ItemType;
  itemId: number;
  textFollow?: string | ReactNode;
  textUnfollow?: string | ReactNode;
  source?: DataSource;
  width?: string;
}
const BtnFollow: FC<Props> = ({
  btnType = 'primary',
  displayButton = false,
  followState: followStateProp = false,
  itemType = undefined,
  itemId = undefined,
  textFollow = <FormattedMessage id="general.follow" defaultMessage="Follow" />,
  textUnfollow = <FormattedMessage id="general.unfollow" defaultMessage="Unfollow" />,
  source = 'api',
  width,
}) => {
  const [followState, setFollowState] = useState(followStateProp);
  const [sending, setSending] = useState(false);
  const [icon, setIcon] = useState(['far', 'bell-slash']);
  const api = useApi();
  const { formatMessage } = useIntl();
  const { user } = useUser();
  useEffect(() => {
    // if data also comes from algolia (and not only api), get save state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (source === 'algolia') {
      if (user) {
        const fetchSaves = async () => {
          const res = await api
            .get(`/api/${itemType}/${itemId}/follow`, { cancelToken: axiosSource.token })
            .catch((err) => {
              if (!Axios.isCancel(err)) {
                console.error("Couldn't GET saves", err);
              }
            });
          if (res?.data?.has_followed) {
            setFollowState(res.data.has_followed);
          }
        };
        fetchSaves();
      }
      setFollowState(false);
    }
    return () => {
      // This will prevent to setFollowState on unmounted BtnSave
      axiosSource.cancel();
    };
  }, [api, itemId, itemType, source, user]);

  useEffect(() => {
    // change icon when followState changes
    setIcon(followState ? ['fas', 'bell'] : ['far', 'bell-slash']);
  }, [followState]);

  const changeStateFollow = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      const action = followState ? 'unfollow' : 'follow';
      // send event to google analytics
      ReactGA.event({ category: 'Button', action: action, label: `[${user?.id},${itemId},${itemType}]` });
      if (action === 'follow') {
        // if user is following
        api
          .put(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't PUT /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      } else {
        // else unfollow
        api
          .delete(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't DELETE /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      }
    }
  };
  const text = followState ? textUnfollow : textFollow;
  const action = followState ? 'unfollow' : 'follow';
  // if displayButton if not set to true, display icon by default
  if (!displayButton)
    return (
      <>
        {sending ? (
          <span>
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
            &nbsp;
          </span>
        ) : (
          <>
            <Box data-tip={formatMessage({ id: `general.${action}`, defaultMessage: action })} data-for="follow">
              <Bell
                icon={icon}
                onClick={changeStateFollow}
                onKeyUp={changeStateFollow}
                onMouseEnter={() => {
                  // change icon depending on follow state
                  setIcon(followState ? ['far', 'bell-slash'] : ['fas', 'bell']);
                }}
                onMouseLeave={() => {
                  // change icon depending on follow state
                  setIcon(followState ? ['fas', 'bell'] : ['far', 'bell-slash']);
                }}
                tabIndex={0}
              />
              <ReactTooltip id="follow" delayHide={300} effect="solid" />
            </Box>
          </>
        )}
      </>
    );
  // else display full button
  return (
    <Button onClick={changeStateFollow} onKeyUp={changeStateFollow} btnType={btnType} disabled={sending} width={width}>
      {sending ? (
        <span>
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
          &nbsp; {text}
        </span>
      ) : (
        text
      )}
    </Button>
  );
};
export default BtnFollow;
