import TitleInfo from '~/components/Tools/TitleInfo';
import { useIntl } from 'react-intl';
import Select from 'react-select';
import { FC } from 'react';

interface Props {
  id: string;
  title: any;
  content: string;
  options: any[];
  onChange?: (id: number, value: string | number) => void;
  mandatory?: boolean;
  type?: string;
  warningMsgIntl?: string;
  errorCodeMessage?: string;
  placeholder?: string;
  isSearchable?: boolean;
  tooltipMessage?: string;
}

const FormDropdownComponent: FC<Props> = ({
  id,
  title,
  content = '',
  options = [],
  onChange = (id, value) => console.warn(`onChange doesn't exist to update ${value} of ${id}`),
  mandatory = false,
  type = '',
  warningMsgIntl,
  errorCodeMessage,
  placeholder,
  isSearchable = false,
  tooltipMessage,
}) => {
  const { formatMessage } = useIntl();
  const handleChange = (key, content) => {
    onChange(content.name, key.value);
  };

  const optionTranslation = (option) =>
    id === 'name' || id === 'birth_date' // if id of form is name or birth_date, just display option
      ? option
      : id === 'gender' // if dropdown is a user category, show them
      ? formatMessage({ id: `user.profile.edit.gender.${option}`, defaultMessage: option })
      : id === 'category' // if dropdown is a user category, show them
      ? formatMessage({ id: `user.profile.edit.select.${option}`, defaultMessage: option })
      : type === 'challenge' // if dropdown comes from challenge, display special challenge status translation
      ? formatMessage({ id: `challenge.info.status.${option}`, defaultMessage: option })
      : id === 'maturity' // if dropdown comes from project maturity field
      ? formatMessage({ id: `project.maturity.${option}`, defaultMessage: option })
      : // else display default status translation
        formatMessage({ id: `entity.info.status.${option}`, defaultMessage: option });

  const optionsList = options.map((option) => {
    return {
      value: option,
      label: optionTranslation(option),
    };
  });
  const customStyles = {
    option: (provided) => ({
      ...provided,
      padding: '6px 10px',
    }),
  };

  return (
    <div className="formDropdown">
      <TitleInfo title={title} mandatory={mandatory} tooltipMessage={tooltipMessage} />
      <div className="content">
        {warningMsgIntl &&
          content === 'draft' && ( // display warning message if applicable
            <p className="dropdownWarningMsg">
              {formatMessage({ id: warningMsgIntl.id, defaultMessage: warningMsgIntl.defaultMessage })}
            </p>
          )}
        <Select
          name={id}
          id={id}
          options={optionsList}
          styles={customStyles}
          isSearchable={isSearchable}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={
            placeholder ||
            formatMessage({
              id: 'user.profile.edit.select.default',
              defaultMessage: 'Select an option',
            })
          }
          defaultValue={content && { value: content, label: optionTranslation(content) }}
          noOptionsMessage={() => null}
          onChange={handleChange}
        />
      </div>
      {errorCodeMessage && (
        <div className="invalid-feedback" style={{ display: 'block' }}>
          {formatMessage({ id: errorCodeMessage, defaultMessage: 'Value is not valid' })}
        </div>
      )}
    </div>
  );
};
export default FormDropdownComponent;
