import { FormattedMessage, useIntl } from 'react-intl';
import AsyncCreatableSelect from 'react-select/async-creatable';
import algoliasearch from 'algoliasearch/lite';
import TitleInfo from '~/components/Tools/TitleInfo';
// import "./FormResourcesComponent.scss";

const FormResourcesComponent = ({
  content = [],
  errorCodeMessage = '',
  id = 'resources',
  mandatory = false,
  type = 'default',
  onChange = (value, resources) => console.warn(`onChange doesn't exist to update ${value} of ${resources}`),
  placeholder = '',
  title = 'Title',
}) => {
  const intl = useIntl();

  const handleChange = (resourcesList) => {
    // convert react-select array (resourcesList) into an arry with just the resources labels
    const resources = resourcesList?.map(({ label }) => label);
    onChange(id, resources);
  };

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('Ressource'); // Ressource
  let algoliaResources = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['ressource_name'], // ressource_name
        hitsPerPage: 5,
      })
      .then((content) => {
        if (content) {
          algoliaResources = content.hits;
        } else {
          algoliaResources = [''];
        }
        algoliaResources = algoliaResources.map(({ ressource_name }) => {
          return { value: ressource_name, label: ressource_name };
        }); // ressource_name
        resolve(algoliaResources);
      });
  };
  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const currentResources = content.map((resource) => {
    if (resource.resource_name) {
      return { label: resource.resource_name, value: resource.resource_name };
    }
    return { label: resource, value: resource };
  });

  const customStyles = {
    loadingIndicator: (provided) => ({
      ...provided,
      display: 'none',
    }),
  };

  return (
    <div className="formResources">
      <TitleInfo title={title} mandatory={mandatory} />

      <div className="content ressource-container" id="ressources">
        <label className="form-check-label" htmlFor={`show${id}`}>
          <FormattedMessage
            id={`help.resources.${type}`}
            defaultMessage="Type your resources in the box bellow. To validate a resource, click on it or press Enter."
          />
        </label>
        <AsyncCreatableSelect
          name={id}
          cacheOptions
          isMulti
          defaultValue={currentResources && currentResources}
          defaultOptions={false}
          components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
          formatCreateLabel={(inputValue) =>
            `${intl.formatMessage({ id: 'general.add', defaultMessage: `Add` })} "${inputValue}"`
          }
          blurInputOnSelect={false} // force keeping focus when selecting option (for mobile)
          noOptionsMessage={() => null}
          loadOptions={loadOptions}
          onChange={handleChange}
          placeholder={placeholder}
          // allowCreateWhileLoading={true}
          styles={customStyles}
        />
        {errorCodeMessage && (
          <div className="invalid-feedback">
            <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
          </div>
        )}
      </div>
    </div>
  );
};

export default FormResourcesComponent;
