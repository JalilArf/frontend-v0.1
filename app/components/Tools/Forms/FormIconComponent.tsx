import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import Box from '~/components/Box';
import Alert from '~/components/Tools/Alert';
import BtnUploadIcon from '~/components/Tools/BtnUploadIcon';
import { ItemType } from '~/types';
// import "./FormIconComponent.scss";

interface Props {
  id: string;
  imageUrl: string;
  itemId: number;
  itemType: ItemType;
  maxSizeFile: number;
  onChange: (value: any) => void;
  isCustomLink: boolean;
}
export default class FormIconComponent extends Component<Props> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      error: '',
      imageUrl: this.props.imageUrl,
      uploading: false,
      previewImageUrl: undefined,
    };
  }

  static get defaultProps() {
    return {
      id: '',
      imageUrl: '',
      itemId: '',
      itemType: '',
      maxSizeFile: 3145728,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      isCustomLink: false,
    };
  }
  previewFile(file) {
    var preview = document.querySelector('.iconPreview img');
    var reader = new FileReader();
    reader.onloadend = function () {
      preview.src = reader.result;
    };
    if (file) {
      reader.readAsDataURL(file);
      this.setState({ previewImageUrl: preview.src });
    } else {
      preview.src = '';
    }
  }

  handleChange(file) {
    this.previewFile(file);
    this.props.onIconUpload(file);
  }

  render() {
    const { itemId, itemType, isCustomLink } = this.props;
    const { error, imageUrl, uploading, previewImageUrl } = this.state;
    // if previewImageUrl is undefined, display imageUrl passed by parent (when changing url name), else display the previewImageUrl (after uploading custom icon)
    const imgTodisplay = !previewImageUrl ? this.props.imageUrl : previewImageUrl;

    return (
      <Box row alignItems="center" width="100%" pt={[1, 6]} pl={[0, 4]} pb={[3, undefined]}>
        <div className="preview iconPreview">
          {imgTodisplay && <img src={imgTodisplay} width="40px" />}
          {/* <img src={imageUrl} width="40px" /> */}
        </div>
        <Box className="btnUploadZone" alignItems="center">
          {error !== '' && <Alert type="danger" message={error} />}
          {uploading && (
            <div className="spinner-border text-secondary" role="status">
              <span className="sr-only">
                <FormattedMessage id="general.loading" defaultMessage="Loading..." />
              </span>
            </div>
          )}
          {!uploading && isCustomLink && (
            <BtnUploadIcon
              fileTypes={['image/jpeg', 'image/png', 'image/jpg', 'image/svg+xml']}
              itemId={itemId}
              itemType={itemType}
              maxSizeFile={4194304}
              imageUrl={imageUrl}
              text={<FormattedMessage id="info-1002" defaultMessage="Choose a file" />}
              onIconUpload={this.handleChange}
            />
          )}
        </Box>
      </Box>
    );
  }
}
