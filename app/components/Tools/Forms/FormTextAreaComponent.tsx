import { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import TitleInfo from '~/components/Tools/TitleInfo';
import InfoMaxCharComponent from '~/components/Tools/Info/InfoMaxCharComponent';
// import "./FormTextAreaComponent.scss";

export default class FormTextAreaComponent extends Component {
  static get defaultProps() {
    return {
      errorCodeMessage: '',
      id: 'default',
      isValid: undefined,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      placeholder: '',
      maxChar: undefined,
      mandatory: false,
      title: 'Title',
    };
  }

  handleChange(event) {
    this.props.onChange(event.target.id, event.target.value);
  }

  render() {
    const { content, errorCodeMessage, id, isValid, maxChar, mandatory, placeholder, title, rows = 5 } = this.props;

    return (
      <div className="formTextArea">
        <TitleInfo title={title} mandatory={mandatory} />
        <div className="content">
          <div className="input-group">
            <textarea
              className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''}`}
              id={id}
              placeholder={placeholder}
              value={content === null ? '' : content}
              rows={rows}
              onChange={this.handleChange.bind(this)}
            />
            {errorCodeMessage && (
              <div className="invalid-feedback">
                <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
              </div>
            )}
          </div>
          <InfoMaxCharComponent content={content} maxChar={maxChar} />
        </div>
      </div>
    );
  }
}
