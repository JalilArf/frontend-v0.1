import { FormattedMessage, useIntl } from 'react-intl';
// import "./InfoInterestsComponent.scss";
import $ from 'jquery';
import TitleInfo from '~/components/Tools/TitleInfo';
import Box from '~/components/Box';
import ReactTooltip from 'react-tooltip';
import { defaultSdgsInterests } from '~/utils/utils';

export default function InfoInterestsComponent({ title = 'Title', content = [] }) {
  const intl = useIntl();
  const showMore = (items) => {
    $('.less').show();
    $(`.interest:lt(${items})`).show(300);
    $('.more').hide();
  };

  const showLess = () => {
    $('.interest').not(':lt(4)').hide(300);
    $('.more').show();
    $('.less').hide();
  };
  // get sdgs infos array from external function
  const defaultInterests = defaultSdgsInterests(intl.formatMessage);

  let interestClass = '';
  if (content.length > 0) {
    return (
      <div className="infoInterests">
        {title && <TitleInfo title={title} />}
        <div className="interests">
          {content
            .sort((a, b) => a - b) // sort sdgs by asc order
            .map((interestId, index) => {
              interestClass = index < 4 ? 'interest' : 'interest toggle';
              const interest = defaultInterests.find((el) => el.value === interestId);
              const largeLabel = interest.label.length > 29;

              return (
                <div className={interestClass} key={index}>
                  <Box
                    bg={interest.color}
                    alignItems="center"
                    position="absolute"
                    height="full"
                    lineHeight="14px"
                    pb={2}
                    data-tip={intl.formatMessage({
                      id: `sdg-description-${interestId}`,
                      defaultMessage: `SDG ${interestId} text explanation`,
                    })}
                    data-for={`sdg-${interestId}`}
                  >
                    <Box
                      row
                      color="white"
                      pt={'.65rem'}
                      px={2}
                      pb={0}
                      height="50px"
                      overflow="hide"
                      width="100%"
                      fontFamily="Bebas Neue"
                    >
                      <span
                        style={{
                          fontSize: interest.value < 10 ? '1.7rem' : '1.6rem',
                          paddingRight: largeLabel ? '5px' : '10px',
                          paddingTop: '1px',
                          fontWeight: 'bold',
                        }}
                      >
                        {interestId}
                      </span>
                      <span
                        style={{
                          fontSize: interest.label.length > 18 ? 'calc(100% - 3px)' : 'calc(100% - 1px)',
                          lineHeight: largeLabel ? '12px' : '13px',
                        }}
                      >
                        {interest.label}
                      </span>
                    </Box>
                    <img
                      style={{ width: '100%', height: largeLabel ? '50px' : '52px', objectFit: 'contain' }}
                      src={`/images/interests/Interest-${interestId}-icon.png`}
                    />
                  </Box>
                  <ReactTooltip id={`sdg-${interestId}`} effect="solid" type="info" className="solid-tooltip" />
                </div>
              );
            })}
          {content.length > 4 && (
            <>
              <Box as="button" className="more" onClick={() => showMore(content.length)}>
                <FormattedMessage id="general.showmore" defaultMessage="Show More" />
              </Box>
              <Box as="button" className="less" onClick={() => showLess()}>
                <FormattedMessage id="general.showless" defaultMessage="Show Less" />
              </Box>
            </>
          )}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
