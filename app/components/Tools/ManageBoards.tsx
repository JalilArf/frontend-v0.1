import Link from 'next/link';
import React, { FC, FormEvent, useState } from 'react';
import { useIntl } from 'react-intl';
import Box from '~/components/Box';
import Button from '~/components/primitives/Button';
import H2 from '~/components/primitives/H2';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import { Board } from '~/types';
import Card from '../Card';
import P from '../primitives/P';
import Alert from './Alert';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormWysiwygComponent from './Forms/FormWysiwygComponent';
import InviteBoardMember from '../Program/InviteBoardMember';
import 'twin.macro';
import Loading from './Loading';

const ManageBoards = ({ itemType, itemId }) => {
  const { formatMessage } = useIntl();
  const { data: boardList, mutate: boardListMutate, revalidate: boardListRevalidate } = useGet<Board[]>(
    `/api/${itemType}/${itemId}/boards`
  );
  const [showBoardCreate, setShowBoardCreate] = useState(false);
  const addBoard = () => {
    setShowBoardCreate(!showBoardCreate);
  };
  return (
    <Box spaceY={3}>
      <H2>{formatMessage({ id: 'program.boardTitle', defaultMessage: 'Boards' })}</H2>
      <Button onClick={addBoard}>{formatMessage({ id: 'general.add', defaultMessage: 'Add' })}</Button>
      {showBoardCreate && (
        <BoardFormCard
          mode="create"
          itemType={itemType}
          itemId={itemId}
          onMembersChange={() => {
            boardListRevalidate();
          }}
          onCreate={(newBoard: Board) => {
            boardListMutate(
              {
                data: [...boardList, newBoard],
              },
              false
            );
            // hide add button after having created the board
            setShowBoardCreate(false);
          }}
        />
      )}
      <Box spaceY={3} pt={8}>
        {!boardList ? (
          <Loading />
        ) : (
          boardList
            ?.sort(function (a, b) {
              return a.id - b.id; // sort by id (asc)
            })
            .map((value, i) => (
              <Box key={i} pt={6}>
                <P pl={2} style={{ fontSize: '2rem' }}>
                  {i + 1}
                </P>
                <BoardFormCard
                  value={value}
                  itemType={itemType}
                  itemId={itemId}
                  mode="edit"
                  onMembersChange={() => boardListRevalidate()}
                  onUpdate={(updatedBoard: Board) => {
                    boardListMutate(
                      {
                        data: boardList.map((board) => {
                          if (board.id === updatedBoard.id) return updatedBoard;
                          return board;
                        }),
                      },
                      false
                    );
                  }}
                  onDelete={(boardId: number) => {
                    boardListMutate(
                      {
                        // removes the board which has the same id as boardId
                        data: boardList.filter((board) => board.id !== boardId),
                      },
                      false
                    );
                  }}
                />
              </Box>
            ))
        )}
      </Box>
    </Box>
  );
};
interface IBoardFormCard {
  value?: Board;
  mode: 'create' | 'edit';
  itemType: string;
  itemId: number;
  onCreate?: (newBoard: Board) => void;
  onUpdate?: (updatedBoard: Board) => void;
  onDelete?: (boardId: number) => void;
  onMembersChange?: () => void;
}
const BoardFormCard: FC<IBoardFormCard> = ({
  value = { title: '', description: '', id: undefined, users: [] },
  mode,
  itemType,
  itemId,
  onCreate,
  onUpdate,
  onDelete,
  onMembersChange,
}) => {
  const api = useApi();
  const { formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [board, setBoard] = useState<{ board: Board }>({
    board: {
      id: value.id,
      title: value.title,
      description: value.description,
      users: value.users,
    },
  });

  // handle every changes to the board
  const handleChange: (key: number, content: string) => void = (key, content) => {
    setBoard((prevBoard) => ({ board: { ...prevBoard.board, [key]: content } }));
    setShowSubmitBtn(content !== board.board.title || content !== board.board.description);
  };
  // handle when submitting (update, or create), an board
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (mode === 'edit') {
      api.patch(`/api/${itemType}/${itemId}/boards/${board?.board.id}`, board).then(() => {
        onUpdate(board.board); // call onUpdate function
        showBoardUpdateConfirmation();
        setShowSubmitBtn(false); // hide update button
      });
    }
    if (mode === 'create') {
      api.post(`/api/${itemType}/${itemId}/boards`, board).then(() => {
        onCreate(board.board); // call onCreate function
        setBoard({ board: { title: '', description: '', id: undefined, users: [] } }); // reset board fields
      });
    }
  };
  // handle when deleting on board
  const handleDelete = () => {
    api.delete(`/api/${itemType}/${itemId}/boards/${board.board.id}`).then(() => {
      onDelete(board.board.id); // call onDelete function
    });
  };

  const showBoardUpdateConfirmation = () => {
    setHasUpdated(true);
    setTimeout(() => {
      // hide message after 3.5se
      setHasUpdated(false);
    }, 3500);
  };

  const handleMembersChange = () => {
    showBoardUpdateConfirmation();
    onMembersChange();
    // force change board to see new users (@TOFIX)
    // when new value come from props update board state (used to display updated members after adding or deleting one)
    // TODO fix this not use revalidate to re-get all boards on members change, and use mutate instead (but that means we need to pass member id, and logo_url to this file when adding a member)
    setBoard({
      board: {
        id: value.id,
        title: value.title,
        description: value.description,
        users: value.users,
      },
    });
  };

  const deleteMember = (memberId) => {
    api
      .delete(`/api/${itemType}/${itemId}/boards/${board.board.id}/users/${memberId}`)
      .then(() => {
        handleMembersChange();
        showBoardUpdateConfirmation();
      })
      .catch((err) => {
        console.error(`Couldn't delete member of board with memberId=${memberId}`, err);
      });
  };

  const intlTranslation =
    mode === 'create'
      ? { id: 'entity.form.btnCreate', defaultMessage: 'Create' }
      : { id: 'entity.form.btnUpdate', defaultMessage: 'Update' };
  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <Box flexDirection={['column', undefined, 'row']} justifyContent="space-between">
          <Box spaceY={4} width={'100%'} pr={[0, undefined, 5]}>
            <Box>
              <FormDefaultComponent
                id="title"
                content={board.board.title}
                title={formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
                placeholder={formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
                onChange={handleChange}
              />
            </Box>
            <Box>
              <FormWysiwygComponent
                id="description"
                content={board.board.description}
                title={formatMessage({ id: 'program.board.content', defaultMessage: 'Content' })}
                placeholder={formatMessage({
                  id: 'program.board.content.placeholder',
                  defaultMessage: 'Describe your board.',
                })}
                onChange={handleChange}
                show
              />
            </Box>
            {mode === 'edit' ? (
              // only show 'add members button if we are in edit mode, else we can't add members to an empty board
              <Button
                tw="mt-0"
                type="button"
                onClick={() => {
                  showModal({
                    children: (
                      <InviteBoardMember
                        programId={itemId}
                        boardId={board.board.id}
                        onMembersAdded={handleMembersChange}
                      />
                    ),
                    title: 'Add new members',
                    titleId: 'member.btnNewMembers',
                    allowOverflow: true,
                  });
                }}
              >
                {formatMessage({ id: 'member.btnNewMembers', defaultMessage: 'Add new members' })}
              </Button>
            ) : (
              // don't show button when creating board, cause youo can't add members to yet a non existant board
              formatMessage({
                id: 'member.btnNewMembers.noBtn',
                defaultMessage: 'To add members to your board, you first need to create it',
              })
            )}
            {board.board.users.length !== 0 && (
              <Box>
                <div className="needWorkers">
                  {formatMessage({ id: 'attach.members', defaultMessage: 'Members: ' })}
                  <span className="workers imgList">
                    {board.board.users.map(
                      (boardMember: { logo_url: string; id: number; first_name: string; last_name: string }, index) => (
                        <>
                          <Link href={`/user/${boardMember.id}`} key={index}>
                            <a>
                              <img
                                className="userImg"
                                src={boardMember.logo_url}
                                alt={`${boardMember.first_name} ${boardMember.last_name}`}
                              />
                            </a>
                          </Link>
                          <Button
                            className="close delete"
                            aria-label="Close"
                            onClick={() => deleteMember(boardMember.id)}
                          >
                            <span aria-hidden="true">&times;</span>
                          </Button>
                        </>
                      )
                    )}
                  </span>
                </div>
              </Box>
            )}
            {hasUpdated && (
              <Alert
                type="success"
                message={formatMessage({ id: 'program.board.updated', defaultMessage: 'The board has been updated.' })}
              />
            )}
          </Box>
          <Box spaceY={2} justifyContent="center" alignItems="flex-end" pt={[4, undefined, 0]}>
            {(showSubmitBtn || mode === 'create') && (
              <Button type="submit" width="100%">
                {formatMessage(intlTranslation)}
              </Button>
            )}
            {mode !== 'create' && (
              <Button type="button" btnType="danger" onClick={handleDelete} width="100%">
                {formatMessage({ id: 'feed.object.delete', defaultMessage: 'Delete' })}
              </Button>
            )}
          </Box>
        </Box>
      </form>
    </Card>
  );
};

export default ManageBoards;
