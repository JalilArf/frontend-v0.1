import { FC } from 'react';
import { ItemType } from '~/types';
import DocumentCard from './DocumentCard';
import DocumentCardFeed from './DocumentCardFeed';

interface Props {
  documents: any[];
  cardType: 'feed' | 'cards';
  postId?: number;
  itemType: ItemType;
  itemId: number;
  isAdmin: boolean;
  isEditing?: boolean;
  showDeleteIcon?: boolean;
  refresh?: () => void;
}
const DocumentsList: FC<Props> = ({
  documents = [],
  isAdmin = false,
  cardType,
  itemId,
  isEditing = false,
  itemType,
  postId,
  showDeleteIcon = true,
  refresh,
}) => {
  const makeCards = () => {
    const cards = documents.map((document, index) => {
      if (document) {
        if (cardType === 'feed') {
          return (
            <DocumentCardFeed document={document} key={index} isEditing={isEditing} postId={postId} refresh={refresh} />
          );
        }
        return (
          <DocumentCard
            document={document}
            key={index}
            itemId={itemId}
            isAdmin={isAdmin}
            itemType={itemType}
            refresh={refresh}
            showDeleteIcon={showDeleteIcon}
          />
        );
      }
      return '';
    });
    return cards;
  };

  return (
    <div className="feedDocumentList container-fluid">
      <div className="row">{makeCards()}</div>
    </div>
  );
};

export default DocumentsList;
