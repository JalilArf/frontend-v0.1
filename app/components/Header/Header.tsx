import Box from '../Box';
import cookie from 'js-cookie';
import Link from 'next/link';
import Loading from '../Tools/Loading';
import React, { useContext, useState } from 'react';
import { faBars, faCheck, faGlobe, faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormattedMessage, useIntl } from 'react-intl';
import { Menu, MenuLink } from '@reach/menu-button';
import { Router, useRouter } from 'next/router';
import { Transition } from 'react-transition-group';
import Button from '~/components/primitives/Button';
import UserMenu from '~/components/User/UserMenu';
import { UserContext } from '~/contexts/UserProvider';
import useGet from '~/hooks/useGet';
import { Program } from '~/types';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import {
  AMenu,
  Container,
  DesktopNav,
  DropDownMenu,
  LangItem,
  Logo,
  MobileNav,
  MobileSideNav,
  StyledMenuButton,
} from './Header.styles';
import HeaderNotifications from './HeaderNotifications';

const languages = [
  { id: 'en', title: 'English' },
  { id: 'fr', title: 'Français' },
  { id: 'de', title: 'Deutsch' },
  { id: 'es', title: 'Español' },
];

const Header = () => {
  const router = useRouter();
  const theme = useTheme();
  const { user, isConnected } = useContext(UserContext);
  const [isOpen, setOpen] = useState(false);
  const { data: programs } = useGet<Program[]>('/api/programs');

  // force close mobile menu when changing page, as the href of some pages are the same(all search page)
  Router.events.on('routeChangeStart', () => setOpen(false));

  const gotoSignInPage = () => {
    router.push({ pathname: '/signin', query: { redirectUrl: router.asPath } });
  };

  const ProgramsDropDown = () => (
    <Menu>
      <StyledMenuButton>
        <FormattedMessage id="general.programs" defaultMessage="Programs" /> <span aria-hidden>▾</span>
      </StyledMenuButton>

      <DropDownMenu>
        {programs ? (
          programs?.reverse().map((program, i) => (
            // browse through programs (from newest to oldest)
            <Link key={i} href={`/program/${program.short_title}`} passHref>
              <MenuLink to={`/program/${program.short_title}`}>{program.title}</MenuLink>
            </Link>
          ))
        ) : (
          <Loading />
        )}
      </DropDownMenu>
    </Menu>
  );

  return (
    <Container height={10} px={[3]}>
      {/* for users navigating with keyboard, show this so they can skip content*/}
      <div className="skip-links">
        Skip to <a href="#main">content</a> or <a href="#footer">footer</a>
      </div>
      {/* Mobile Nav -- Will only display on mobile breakpoints */}
      <MobileNav display={['flex', undefined, undefined, 'none']}>
        <div>
          <Link href="/" passHref>
            <Box width={theme.sizes[11]} height="auto">
              {/* <Logo src="/images/logo_img.png" alt="JOGL icon" width={320} height={158} quality={50} /> */}
              <Logo src="/images/logo_img.png" alt="JOGL icon" unsized quality={50} />
            </Box>
          </Link>
        </div>
        <Box row>
          {isConnected && user && (
            <Box row spaceX={4} pr={2}>
              <HeaderNotifications userId={user.id} />
              <UserMenu />
            </Box>
          )}
          <Menu>
            <BurgerButton onClick={() => setOpen(!isOpen)}>
              <FontAwesomeIcon icon={faBars} />
            </BurgerButton>
            <Transition in={isOpen} timeout={300}>
              {(state) => (
                // state change: exited -> entering -> entered -> exiting -> exited
                <MobileSideNav open={state === 'entering' || state === 'entered'} p={4}>
                  <MobileLinksBox spaceY={3}>
                    {/* {navLinks.map((link, i) => (
                      <Link href={link.url} key={i} passHref>
                        <AMenu>
                          <FormattedMessage id={link.intlId} defaultMessage={link.title} />
                        </AMenu>
                      </Link>
                    ))} */}
                    <Link href="/search/members" passHref>
                      <AMenu>
                        <FontAwesomeIcon icon={faSearch} />
                        {/* <FormattedMessage id="header.search" defaultMessage="Search" /> */}
                        <FormattedMessage id="general.explore" defaultMessage="Explore" />
                      </AMenu>
                    </Link>
                    <Box pt={3}>
                      <ProgramsDropDown />
                    </Box>
                    {isConnected && (
                      <Box pt={3}>
                        <CreateObjectsDropdown />
                      </Box>
                    )}
                    <Box pt={3}>
                      <LangDropdown />
                    </Box>
                    {!isConnected && (
                      <Box>
                        <Button onClick={gotoSignInPage}>
                          <FormattedMessage id="header.signInUp" defaultMessage="Sign in / Register" />
                        </Button>
                      </Box>
                    )}
                  </MobileLinksBox>
                </MobileSideNav>
              )}
            </Transition>
          </Menu>
        </Box>
      </MobileNav>
      {/* Desktop Nav */}
      <DesktopNav display={['none', undefined, undefined, 'flex']}>
        <Box row spaceX={[undefined, undefined, 4, 5]} display="inline-flex" alignItems="center">
          <Link href="/" passHref>
            <AMenu>
              <Box width={theme.sizes[12]} height="auto">
                <Logo src="/images/logo_img.png" alt="JOGL icon" unsized quality={50} />
                {/* <Logo src="/images/logo_img.png" alt="JoGL icon" wwidth="320px" hheight="158px" quality={50} unsized /> */}
                {/* <Logo src="/images/logo_img.png" alt="JoGL icon" width={320} height={158} quality={50} /> */}
                {/* <img src="/images/logo_img.png" alt="JoGL icon" width="100%" /> */}
              </Box>
            </AMenu>
          </Link>
          <Link href="/search/members" passHref>
            <AMenu>
              <FontAwesomeIcon icon={faSearch} />
              <FormattedMessage id="general.explore" defaultMessage="Explore" />
            </AMenu>
          </Link>
          <ProgramsDropDown />
        </Box>
        <Box row spaceX={2}>
          {user && isConnected && <HeaderNotifications userId={user.id} />}
          {user && <CreateObjectsDropdown />}
          <LangDropdown />
          {isConnected ? (
            <UserMenu />
          ) : (
            <Button onClick={gotoSignInPage}>
              {/* <FormattedMessage id="header.signIn" defaultMessage="Sign in" /> */}
              <FormattedMessage id="header.signInUp" defaultMessage="Sign in / Register" />
            </Button>
          )}
        </Box>
      </DesktopNav>
    </Container>
  );
};

const CreateObjectsDropdown = () => (
  <Menu>
    <StyledMenuButton textAlign={['left', undefined, undefined, 'center']}>
      <FormattedMessage id="entity.form.btnCreate" defaultMessage="Create" /> <span aria-hidden>▾</span>
    </StyledMenuButton>
    <DropDownMenu>
      <Link href="/project/create" passHref>
        <MenuLink to="/project/create">
          <FormattedMessage id="header.createProject" defaultMessage="a project" />
        </MenuLink>
      </Link>
      <Link href="/community/create" passHref>
        <MenuLink to="/community/create">
          <FormattedMessage id="header.createGroup" defaultMessage="a group" />
        </MenuLink>
      </Link>
    </DropDownMenu>
  </Menu>
);

const LangDropdown = () => {
  const { locale } = useIntl();
  const changeLanguage = (newLanguage) => {
    // This is allowed because it is only called on client side.
    // This code has a lot of implications, because normally there's no locale set in the
    // cookies so the prioritized language is the one from your browser if it exists.
    // If your browser language is not supported it will fallback to english.
    // But if you set a locale in the cookies this lang will prioritize over all langs.
    // This is all defined in the custom server in server.js
    cookie.set('locale', newLanguage, { expires: 365 });
    window.location.reload();
  };
  return (
    <Menu>
      <StyledMenuButton textAlign={['left', undefined, undefined, 'center']} uppercase>
        <FontAwesomeIcon icon={faGlobe} /> {locale} <span aria-hidden>▾</span>
      </StyledMenuButton>
      <DropDownMenu>
        {languages.map((lang, i) => (
          <LangItem
            onClick={() => changeLanguage(lang.id)}
            onSelect={() => changeLanguage(lang.id)}
            selected={locale === lang.id}
            key={i}
          >
            {lang.title} {locale === lang.id && <FontAwesomeIcon icon={faCheck} />}
          </LangItem>
        ))}
      </DropDownMenu>
    </Menu>
  );
};

const BurgerButton = styled(StyledMenuButton)`
  z-index: 1;
  position: relative;
  margin-left: 0.75rem;
  font-size: 1.5rem;
`;

const MobileLinksBox = styled(Box)`
  > * + * {
    ${(p) => `border-top: 1px solid ${p.theme.colors.greys['400']}!important`};
    padding-top: 0.75rem;
  }
`;

export default React.memo(Header);
