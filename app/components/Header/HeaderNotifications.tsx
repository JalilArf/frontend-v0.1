import Box from '../Box';
import Link from 'next/link';
import Loading from '../Tools/Loading';
import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormattedMessage } from 'react-intl';
import { Menu, MenuLink } from '@reach/menu-button';
import { useApi } from '~/contexts/apiContext';
import styled from '~/utils/styled';
import { DropDownMenu, StyledMenuButton } from './Header.styles';
import { displayObjectRelativeDate } from '~/utils/utils';
import P from '../primitives/P';
import { NotifIconWrapper } from '~/utils/notificationsIcons';
import { notifIconTypes } from '~/utils/notificationsIcons';
import 'twin.macro';

const HeaderNotifications = ({ userId }) => {
  const api = useApi();
  const [loading, setLoading] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(0);

  useEffect(() => {
    // to make sure API calls do not set state when unmounted
    let isFetching = true;
    api
      .post('/api/graphql/', {
        query: `
            query {
              user (id: ${userId} ) {
                notifications(first: 1) {
                  unreadCount
                  totalCount
                }
              }
            }
          `,
      })
      .then((response) => {
        var notifications = response.data.data.user.notifications;
        isFetching && setUnreadNotificationsCount(notifications.unreadCount);
      });

    return () => (isFetching = false);
  }, []);

  const visitNotificationLinkAndMarkAsRead = (notification) => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markNotificationAsRead(input: {id: ${notification.id}}) {
              notification {
                id
                read
              }
            }
          }`,
      })
      .then((_response) => {
        // On success, update unread count
        notification.read = true;
        setUnreadNotificationsCount(unreadNotificationsCount - 1);
      });
  };

  const markAllNotificationsAsRead = () => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markAllNotificationsAsRead(input: {}) {
              unreadCount
            }
          }`,
      })
      .then((response) => {
        // On success, update unread count
        setUnreadNotificationsCount(response.data.unreadCount);
        let notifs = notifications.map((notif) => {
          notif.read = true;
          return notif;
        });
        setNotifications(notifs);
      });
  };

  const handleClick = () => {
    setLoading(true);
    api
      .post('/api/graphql/', {
        query: `
              query {
                user (id: ${userId} ) {
                  notifications(first: 20) {
                    edges {
                      node {
                        id
                        link
                        read
                        subjectLine
                        createdAt
                        category
                      }
                    }
                  }
                }
              }
            `,
      })
      .then((response) => {
        var notifications = response.data.data.user.notifications;
        setNotifications(notifications.edges.map((edge) => edge.node));
        setLoading(false);
      })
      .catch(() => setLoading(false));
  };

  return (
    <Menu>
      {({ isExpanded }) => (
        <React.Fragment>
          <Box alignSelf="flex-end" mb="4px">
            <StyledMenuButton
              style={{ position: 'relative' }}
              onClick={() => isExpanded && handleClick()}
              onTouchStart={handleClick} // isExpanded seems to be not working on mobile, so always trigger handleClick on mobile
            >
              <FontAwesomeIcon icon="bell" style={{ fontSize: '1.2rem' }} />
              <Box
                position="absolute"
                right="2px"
                top="-9px"
                bg="#D11024"
                color="white"
                fontSize="0.72rem"
                fontWeight="600"
                borderRadius="4px"
                padding="0px 2px"
              >
                {unreadNotificationsCount > 0 && unreadNotificationsCount <= 99
                  ? // if notif number is between 1 and 99, display count
                    unreadNotificationsCount
                  : // if more than 99, display 99+
                    unreadNotificationsCount > 99 && '99+'}
              </Box>
            </StyledMenuButton>
          </Box>
          <NotifDropDown>
            <Box
              row
              justifyContent="space-between"
              alignItems="center"
              py={2}
              px={3}
              borderBottom="1px solid grey"
              flexWrap="wrap"
            >
              <Box fontWeight="bold" pr={2}>
                <FormattedMessage id="settings.notifications.title" defaultMessage="Notifications" />
              </Box>
              <Box row borderTop="none" alignItems="flex-end">
                <FakeLink borderTop="none" onClick={() => markAllNotificationsAsRead()}>
                  <FormattedMessage id="settings.notifications.markAsRead" defaultMessage="Mark All as read" />
                </FakeLink>
                <Box px={1}>.</Box>
                <Box>
                  <Link href={`/user/${userId}/settings`}>
                    <a>
                      <FormattedMessage id="menu.profile.settings" defaultMessage="Settings" />
                    </a>
                  </Link>
                </Box>
              </Box>
            </Box>
            <NotificationsList>
              {loading ? (
                <Loading />
              ) : (
                notifications.map((notification) => {
                  return (
                    <Notification
                      as="a"
                      href={notification.link}
                      key={notification.id}
                      hasRead={notification.read}
                      onClick={() => visitNotificationLinkAndMarkAsRead(notification)}
                    >
                      <div tw="flex flex-row">
                        <div tw="flex pr-2">
                          <NotifIconWrapper>{notifIconTypes[notification.category]}</NotifIconWrapper>
                        </div>
                        <div tw="flex flex-wrap flex-row">
                          <P mr={2} mb={0}>
                            {notification.subjectLine}
                          </P>
                          <P mb={0} color="#797979">
                            {displayObjectRelativeDate(notification.createdAt)}
                          </P>
                        </div>
                      </div>
                    </Notification>
                  );
                })
              )}
            </NotificationsList>

            <Box alignItems="center" py={2} borderTop="1px solid grey">
              <Link href="/notifications" passHref>
                <a>
                  <FormattedMessage id="program.seeAll" defaultMessage="See all" />
                </a>
              </Link>
            </Box>
          </NotifDropDown>
        </React.Fragment>
      )}
    </Menu>
  );
};

const NotifDropDown = styled(DropDownMenu)`
  border: ${(p) => `1px solid ${p.theme.colors.greys['700']}`};
  > div * {
    border-top: none;
    font-size: 0.9rem;
  }
  a {
    white-space: normal;
  }
  width: 400px;

  @media (max-width: 500px) {
    width: 330px;
  }
  @media (max-width: 450px) {
    width: 300px;
  }
  @media (max-width: 425px) {
    width: 280px;
  }
  @media (max-width: 400px) {
    width: 235px;
  }
  @media (max-width: 360px) {
    width: 215px;
  }
  @media (max-width: 330px) {
    width: 205px;
  }
`;

const NotificationsList = styled(Box)`
  overflow-y: scroll;
  height: 470px;
  max-height: 70vh;
  a {
    white-space: normal;
    font-size: 15px !important;
  }
`;

const Notification = styled(MenuLink)`
  background: ${(p) => (p.hasRead ? 'white' : '#EDF2FA')};
  border-bottom: ${(p) => `1px solid ${p.theme.colors.greys['300']}!important`};
  padding: 10px 12px;
  &:hover {
    background: ${(p) => (p.hasRead ? '#f6f6f6' : '#e2e7ee')};
    color: inherit;
  }
`;

const FakeLink = styled(Box)`
  color: ${(p) => p.theme.colors.primary};
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }
`;

export default HeaderNotifications;
