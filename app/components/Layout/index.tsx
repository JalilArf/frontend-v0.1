import { useIntl } from 'react-intl';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { ReactNode } from 'react';
import Header from '~/components/Header/Header';
import Footer from '~/components/Footer/Footer';
// import tw from 'twin.macro';
interface Props {
  title?: string;
  desc?: string;
  img?: string;
  className?: string;
  noHeaderFooter?: boolean;
  children?: ReactNode;
}
export default function Layout({ title, desc, img, className, noHeaderFooter = false, children }: Props) {
  const { formatMessage } = useIntl();
  const router = useRouter();
  return (
    <>
      <Head>
        <title>{title || formatMessage({ id: 'general.title', defaultMessage: 'JOGL - Just One Giant Lab' })}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="theme-color" content="#ffffff" />
        <meta
          name="description"
          content={
            desc ||
            formatMessage({
              id: 'general.joglDesc',
              defaultMessage:
                'JOGL helps sync humanity onto solving our most important social & environmental problems using open science, responsible innovation & continuous learning.',
            })
          }
        />
        {/* Social media */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content={process.env.ADDRESS_FRONT + router.asPath} />
        <meta
          property="og:title"
          content={title || formatMessage({ id: 'general.title', defaultMessage: 'JOGL - Just One Giant Lab' })}
        />
        <meta property="og:image" content={img || `${process.env.ADDRESS_FRONT}/images/planet.jpg`} />
        <meta
          property="og:description"
          content={
            desc ||
            formatMessage({
              id: 'general.joglDesc',
              defaultMessage:
                'JOGL helps sync humanity onto solving our most important social & environmental problems using open science, responsible innovation & continuous learning.',
            })
          }
        />
        <meta property="og:site_name" content="JOGL - Just One Giant Lab" />
        <meta property="og:locale" content="en_US" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@justonegiantlab" />
      </Head>
      {/* TODO implement user connection */}
      {!noHeaderFooter && <Header userConnected={false} />}
      <main className={className || 'main'} id="main">
        {children}
      </main>
      {!noHeaderFooter && <Footer />}
    </>
  );
}
