import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React, { FC, useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Box from '~/components/Box';
import BtnClap from '~/components/Tools/BtnClap';
import Loading from '~/components/Tools/Loading';
import ShareBtns from '~/components/Tools/ShareBtns/ShareBtns';
import useGet from '~/hooks/useGet';
import { Post, User } from '~/types';
import { displayObjectDate, linkify } from '~/utils/utils';

interface Props {
  post: Post;
  user: User;
  refresh?: () => void;
}
const PostDisplayMinimal: FC<Props> = ({ post: postProp }) => {
  const { data: post, revalidate: revalidatePost } = useGet<Post>(`/api/posts/${postProp.id}`, {
    initialData: postProp,
  });
  const [loading, setLoading] = useState(true);
  const intl = useIntl();
  const openSinglePostPage = (e) => {
    // open single post page in a new page
    if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
      // if function is launched via keypress, execute only if it's the 'enter' key)
      const win = window.open(`/post/${post.id}`, '_blank');
      win.focus();
    }
  };

  useEffect(() => {
    setLoading(false);
  }, []);

  if (loading) {
    return <Loading active={loading} />;
  }
  if (post !== undefined) {
    const creatorId = post.creator.id;
    const objImg = post.creator.logo_url ? post.creator.logo_url : '/images/default/default-user.png';
    const postFromName =
      post.from.object_type === 'need' // if post is a need
        ? intl.formatMessage({ id: 'post.need', defaultMessage: '[Need]: ' }) + post.from.object_name
        : post.from.object_name;

    const userImgStyle = { backgroundImage: `url(${objImg})` };
    const maxChar = 200; // maximum character in a post to be displayed by default;
    const isLongText = post.content.length > maxChar;
    const contentWithLinks = linkify(post.content);
    return (
      <Box borderBottom="1px solid #d3d3d3" py={2}>
        <Box px={3} className="post postCard">
          <div className="topContent">
            {/* <div className="topBar"> */}
            <Box justifyContent="space-between" className="topBar" style={{ marginBottom: '.7rem' }}>
              <div className="left">
                <div className="userImgContainer">
                  <Link href={`/user/${creatorId}`}>
                    <a>
                      <div className="userImg" style={userImgStyle} />
                    </a>
                  </Link>
                </div>
                <div className="topInfo">
                  <Link href={`/user/${creatorId}`}>
                    <a>{`${post.creator.first_name} ${post.creator.last_name}`}</a>
                  </Link>

                  <div className="date">{displayObjectDate(post.created_at)}</div>
                  <div className="from">
                    <FormattedMessage id="post.from" defaultMessage="From: " />
                    <Link href={`/${post.from.object_type}/${post.from.object_id}`}>
                      <a>{postFromName}</a>
                    </Link>
                  </div>
                </div>
              </div>
            </Box>

            <div className={`postTextContainer ${isLongText && 'hideText'}`}>
              {/* add hideText class to hide text if it's too long */}
              <div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
              {isLongText && ( // show "view more" link if text is too long
                <button
                  type="button"
                  className="viewMore"
                  onClick={openSinglePostPage}
                  onKeyUp={openSinglePostPage}
                  tabIndex={0}
                >
                  ...
                  <FormattedMessage id="general.showmore" defaultMessage="Show More" />
                </button>
              )}
            </div>
          </div>
          <div className="actionBar">
            <Box row>
              <BtnClap
                itemType="posts"
                itemId={post.id}
                type="text"
                clapState={post.has_clapped}
                refresh={revalidatePost}
              />
              <button className="btn-postcard btn" onClick={openSinglePostPage} type="button">
                <FontAwesomeIcon icon="comments" size="lg" />
                <FormattedMessage id="post.commentAction" defaultMessage="Comment" />
              </button>
              <ShareBtns type="post" specialObjId={post.id} />
            </Box>
          </div>
        </Box>
      </Box>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default PostDisplayMinimal;
