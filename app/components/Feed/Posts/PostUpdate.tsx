import { useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import FormPost from './FormPost';
import { findMentions, noHTMLMentions, transformMentions } from '~/components/Feed/Mentions';
import { useApi } from '~/contexts/apiContext';
import ReactGA from 'react-ga';

export default function PostUpdate({ content: contentProp = '', userImg, userId, postId, closeOrCancelEdit, refresh }) {
  const [content, setContent] = useState(noHTMLMentions(contentProp));
  const [documents, setDocuments] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [error, setError] = useState();
  const api = useApi();

  const handleChange = (newContent) => {
    setContent(newContent);
  };

  const handleChangeDoc = (newDocuments) => {
    setDocuments(newDocuments);
  };
  useEffect(() => {
    console.warn(error);
  }, [error]);

  const handleSubmit = () => {
    const foundMentions = findMentions(content);
    const contentNoMentions = transformMentions(content);
    const updateJson = {
      post: {
        content: contentNoMentions,
      },
    };
    if (foundMentions) {
      updateJson.post.mentions = foundMentions;
    }
    if (documents) {
      updateJson.post.documents = documents;
    }
    setUploading(true);
    api
      .patch(`/api/posts/${postId}`, updateJson)
      .then(() => {
        // record event to Google Analytics
        ReactGA.event({ category: 'Post', action: 'post update', label: `[${userId},${postId}]` });
        window.gtag('event', 'post update', { category: 'Post', userId, postId });
        if (documents.length > 0) {
          const itemId = postId;
          const itemType = 'posts';
          const type = 'documents';
          if (itemId) {
            const bodyFormData = new FormData();
            Array.from(documents).forEach((file) => {
              bodyFormData.append(`${type}[]`, file);
            });

            const config = {
              headers: { 'Content-Type': 'multipart/form-data' },
            };

            api
              .post(`/api/${itemType}/${itemId}/${type}`, bodyFormData, config)
              .then((res) => {
                if (res.status === 200) {
                  setUploading(false);
                  refresh();
                } else {
                  setUploading(false);
                  setError(<FormattedMessage id="err-" defaultMessage="An error has occured" />);
                }
              })
              .catch((err) => {
                setUploading(false);
                setError(`${err.response.data.status} : ${err.response.data.error}`);
              });
          }
        }
        closeOrCancelEdit(); // close update/edit box after update
        refresh();
      })
      .catch((err) => {
        console.error(`Couldn't PATCH post with id=${postId}`, err);
      });
  };
  return (
    <FormPost
      action="update"
      content={content}
      documents={documents}
      handleChange={handleChange}
      handleChangeDoc={handleChangeDoc}
      handleSubmit={handleSubmit}
      uploading={uploading}
      cancelEdit={closeOrCancelEdit}
      userImg={userImg}
    />
  );
}
