/* eslint-disable camelcase */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React, { FC } from 'react';
import { FormattedDate, FormattedMessage, useIntl } from 'react-intl';
import H1 from '~/components/primitives/H1';
import BtnClap from '~/components/Tools/BtnClap';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';
import { useModal } from '~/contexts/modalContext';
import useMembers from '~/hooks/useMembers';
import useUserData from '~/hooks/useUserData';
import { Community } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import { renderOwnerNames } from '~/utils/utils';
import Box from '../Box';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import BtnFollow from '../Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import BtnSave from '../Tools/BtnSave';
import ListFollowers from '../Tools/ListFollowers';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import QuickSearchBar from '../Tools/QuickSearchBar';
import Chips from '../Chip/Chips';
import { useTheme } from '@emotion/react';
// import Image from 'next/image';
import Image2 from '../Image2';

interface Props {
  community: Community;
}
const CommunityHeader: FC<Props> = ({ community }) => {
  const { members, membersError } = useMembers('communities', community.id);
  const { showModal, setIsOpen } = useModal();
  const { userData } = useUserData();
  const { formatMessage } = useIntl();
  const theme = useTheme();

  const showMembersModal = (e) => {
    members &&
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid gridGap={[2, 4]} gridCols={[1, null, null, 2]} display={['grid', 'inline-grid']} py={[0, 2, 4]}>
              {members.map((member, i) => (
                <UserCard
                  key={i}
                  id={member.id}
                  firstName={member.first_name}
                  lastName={member.last_name}
                  nickName={member.nickname}
                  shortBio={member.short_bio}
                  logoUrl={member.logo_url}
                  canContact={member.can_contact}
                  hasFollowed={member.has_followed}
                  projectsCount={member.stats.projects_count}
                  mutualCount={member.stats.mutual_count}
                  skills={member.skills}
                  role={member.owner ? 'leader' : !member.owner && member.admin && 'admin'}
                />
              ))}
            </Grid>
          </>
        ),
        title: 'Members',
        titleId: 'entity.tab.members',
        maxWidth: '70rem',
      });
  };

  const showFollowersModal = (e) => {
    follower_count && // open modal only if group has followers
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={id} itemType="communities" />,
        title: 'Followers',
        titleId: 'entity.tab.followers',
        maxWidth: '70rem',
      });
  };

  const {
    banner_url,
    follower_count = 0,
    has_followed,
    id,
    is_member,
    is_owner,
    members_count = 0,
    short_description,
    short_title,
    title,
    skills,
    ressources,
    interests,
    has_clapped,
    has_saved,
    claps_count,
    users,
    creator,
    created_at,
    updated_at,
    is_pending,
    is_private,
  } = community;
  let bannerUrl = banner_url;
  if (!banner_url) {
    bannerUrl = '/images/default/default-group.jpg';
  }
  return (
    <div className="row communityHeader">
      <Box alignItems="center" width={'full'} mb={5} px={4}>
        <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
          <H1 fontSize={['2.55rem', undefined, '3rem']}>{title}</H1>
          {userData && (
            <Box row>
              <Box pl={4} pr={2}>
                <BtnSave itemType="communities" itemId={id} saveState={has_saved} />
              </Box>
              <BtnFollow followState={has_followed} itemType="communities" itemId={id} />
            </Box>
          )}
        </Box>
        {/* edit button */}
        {community.is_admin && (
          <Box mt={5}>
            <Link href={`/community/${community.id}/edit`}>
              <a style={{ display: 'flex', justifyContent: 'center' }}>
                <FontAwesomeIcon icon="edit" />
                <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
              </a>
            </Link>
          </Box>
        )}
      </Box>
      <div className="col-lg-7 col-md-12 communityHeader--banner">
        <Image2 src={banner_url} alt={`${title} banner`} unsized />
      </div>
      <div className="col-lg-5 col-md-12 communityHeader--info">
        <p className="infos">#{short_title}</p>
        <p className="info">{short_description}</p>
        {created_at && (
          <Box color={theme.colors.secondary}>
            <FormattedMessage id="general.created_at" defaultMessage="Created on: " />
            <FormattedDate value={created_at} year="numeric" month="long" day="2-digit" />
          </Box>
        )}
        {/* show updated date only if it exists and if it's not the same day as created date (OR if only updated_at exists and not created_at) */}
        {((updated_at && created_at && created_at.substr(0, 10) !== updated_at.substr(0, 10)) ||
          (updated_at && !created_at)) && (
          <Box color={theme.colors.secondary}>
            <FormattedMessage id="general.updated_at" defaultMessage="Last update: " />
            <FormattedDate value={updated_at} year="numeric" month="long" day="2-digit" />
          </Box>
        )}
        <Box pt={4} /> {/* empty div with paddingTop */}
        {users !== undefined && users.length > 0 && renderOwnerNames(users, creator)}
        <InfoInterestsComponent
          place="entity_header"
          title={formatMessage({ id: 'general.sdgs', defaultMessage: "SDG's" })}
          content={interests}
        />
        {skills.length !== 0 && (
          <Box mb={4}>
            <Box color={theme.colors.secondary}>
              {formatMessage({ id: 'entity.info.skills', defaultMessage: 'Skills' })}
            </Box>
            <Chips
              data={skills.map((skill) => ({
                title: skill,
                href: `/search/groups/?refinementList[skills][0]=${skill}`,
              }))}
              overflowText="seeMore"
              // color={theme.colors.primary}
              color="#F2F4F8"
              showCount={4}
            />
          </Box>
        )}
        {ressources.length !== 0 && (
          <Box mb={4}>
            <Box color={theme.colors.secondary}>
              {formatMessage({ id: 'entity.info.resources', defaultMessage: 'Resources' })}
            </Box>
            <Chips
              data={ressources.map((resource) => ({
                title: resource,
                href: `/search/groups/?refinementList[ressources][0]=${resource}`,
              }))}
              overflowText="seeMore"
              // color={theme.colors.pink}
              // color={theme.colors.greys['300']}
              color="#eff7ff"
              showCount={4}
            />
          </Box>
        )}
        <div className="communityStats">
          <span className="text" onClick={showFollowersModal} onKeyUp={showFollowersModal} tabIndex={0}>
            <strong>{follower_count}</strong>&nbsp;{textWithPlural('follower', follower_count)}
          </span>
          <span className="text" onClick={showMembersModal} onKeyUp={showMembersModal} tabIndex={0}>
            <strong>{members_count || 0}</strong>&nbsp;{textWithPlural('member', members_count)}
          </span>
        </div>
        <Box row spaceX={4} py={4} alignItems="center">
          {!is_owner && (
            <BtnJoin
              joinState={is_pending ? 'pending' : is_member}
              isPrivate={is_private}
              itemType="communities"
              itemId={id}
            />
          )}
          <BtnClap itemType="communities" itemId={id} clapState={has_clapped} clapCount={claps_count} />
        </Box>
        <ShareBtns type="community" />
      </div>
    </div>
  );
};
export default CommunityHeader;
