import { useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import DropdownRole from '../Tools/DropdownRole';
import { useApi } from '~/contexts/apiContext';
import Box from '../Box';
import Button from '../primitives/Button';
import A from '../primitives/A';
// import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';
import { useModal } from '~/contexts/modalContext';
import styled from '~/utils/styled';
import ReactTooltip from 'react-tooltip';

const MoreActions = styled(Box)`
  :hover {
    background: lightgrey;
  }
`;

export default function MemberCard({
  member,
  itemId,
  itemType,
  isOwner,
  callBack = () => {
    console.warn('Missing callback');
  },
}) {
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const [showChangedMessage, setShowChangedMessage] = useState(false);

  const onRoleChanged = () => {
    setShowChangedMessage(true);
    setTimeout(() => {
      setShowChangedMessage(false);
    }, 1500);
  };
  const getRole = () => {
    let actualRole = 'member';
    if (!member.owner && !member.admin && !member.member) {
      actualRole = 'pending';
    }
    if (member.member) {
      actualRole = 'member';
    }
    if (member.owner) {
      actualRole = 'owner';
    } else if (member.admin) {
      actualRole = 'admin';
    }
    return actualRole;
  };
  const acceptMember = (newRole) => {
    const role = getRole();
    const jsonToSend = {
      user_id: member.id,
      previous_role: role,
      new_role: newRole,
    };
    setSending(true);
    api
      .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
      .then(() => {
        setSending(false);
        callBack();
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        setSending(false);
      });
  };
  const removeOrRejectMember = () => {
    setSending('remove');
    api
      .delete(`/api/${itemType}/${itemId}/members/${member.id}`)
      .then(() => {
        setSending('');
        setIsOpen(false);
        callBack();
      })
      .catch(() => {
        setSending('');
        setIsOpen(false);
        callBack();
      });
  };

  if (member) {
    const role = getRole();

    let imgTodisplay = '/images/default/default-user.png';
    if (member.logo_url_sm) {
      imgTodisplay = member.logo_url_sm;
    }

    const bgLogo = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };
    const MoreActionsModal = ({ setSending }) => {
      // when opening more actions modal (from the "...")
      const [showSubmitBtn, setShowSubmitBtn] = useState(false);
      const [customRole, setCustomRole] = useState('');
      const [isButtonDisabled, setIsButtonDisabled] = useState('');
      const handleChange: (key: number, content: string) => void = (key, content) => {
        setCustomRole(content);
        setShowSubmitBtn(content && content !== customRole); // show save button only if there is content and it's different from current customRole
      };
      const handleSubmit = () => {
        // api.patch(`/api/${itemType}/${itemId}/members`, {"custom_role": customRole}).then((res) => {
        //   setShowSubmitBtn(false); // hide update button
        // });
      };
      return (
        <>
          {/* <FormDefaultComponent
            content={customRole}
            id="custom_role"
            title={formatMessage({ id: 'member.custom_role.add', defaultMessage: 'Add custom role' })}
            onChange={handleChange}
            placeholder={formatMessage({
              id: 'member.custom_role.placeholder',
              defaultMessage: 'Designer, Lead developer, CEO..',
            })}
          />
          {showSubmitBtn && (
            <Button type="button" onClick={handleSubmit} width="100%">
              {formatMessage({ id: 'entity.form.btnSave', defaultMessage: 'Save' })}
            </Button>
          )} */}
          {(role !== 'owner' || isOwner) && (
            // restriction that prevents admins from removing owners, but allow owners to remove other owners
            <Box>
              {/* <Box style={{ borderTop: '1px solid lightgrey' }} pt={5} mt={5}> */}
              <Button
                btnType="danger"
                disabled={isButtonDisabled}
                onClick={() => {
                  setIsButtonDisabled(true);
                  removeOrRejectMember();
                }}
              >
                {isButtonDisabled && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id="member.remove" defaultMessage="Remove member" />
              </Button>
            </Box>
          )}
        </>
      );
    };

    return (
      <div>
        <Box flexDirection={['column', 'row']} justifyContent={['flex-start', 'space-between']} key={member.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <A href={`/user/${member.id}`}>
              {member.first_name} {member.last_name}
            </A>
          </Box>
          <Box row>
            {role === 'pending' ? (
              // if member role is pending, show buttons to accept or reject the request
              <>
                <button
                  type="button"
                  className="btn btn-outline-success"
                  disabled={sending}
                  style={{ marginBottom: 0, marginLeft: '5px' }}
                  onClick={() => acceptMember('member')}
                >
                  {sending && (
                    <>
                      <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="general.accept" defaultMessage="Accept" />
                </button>
                <button
                  type="button"
                  className="btn btn-outline-danger"
                  disabled={sending === 'remove'}
                  style={{ marginBottom: 0, marginLeft: '5px' }}
                  onClick={removeOrRejectMember}
                >
                  {sending === 'remove' && (
                    <>
                      <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="general.reject" defaultMessage="Reject" />
                </button>
              </>
            ) : (
              // else show member role dropdown, and more action button
              <Box row width={['100%', '20rem']} alignItems="center">
                <Box width="100%">
                  <DropdownRole
                    onRoleChanged={onRoleChanged}
                    actualRole={role}
                    callBack={callBack}
                    itemId={itemId}
                    itemType={itemType}
                    // show different list roles depending if user is owner (to prevent admins changing their roles to "owner")
                    listRole={isOwner ? ['owner', 'admin', 'member'] : ['admin', 'member']}
                    member={member}
                    // don't show dropdown of roles if member you want to change is owner (except if you are the owner), to prevent admins changing owner's role.
                    isDisabled={role !== 'owner' || isOwner}
                  />
                </Box>
                <MoreActions
                  role="button"
                  onClick={() =>
                    showModal({
                      children: <MoreActionsModal setSending={setSending} />,
                      title: 'More actions',
                      titleId: 'general.more_actions',
                    })
                  }
                  onKeyUp={(e) =>
                    e.keyCode === 13 &&
                    showModal({
                      children: <MoreActionsModal setSending={setSending} />,
                      title: 'More actions',
                      titleId: 'general.more_actions',
                    })
                  }
                  // show/hide tooltip on element focus/blur
                  onFocus={(e) => ReactTooltip.show(e.target)}
                  onBlur={(e) => ReactTooltip.hide(e.target)}
                  tabIndex="0"
                  ml={2}
                  px={2}
                  borderRadius="5px"
                  fontSize="120%"
                  data-tip={formatMessage({ id: `general.more_actions`, defaultMessage: 'More actions' })}
                  data-for="more_actions"
                >
                  •••
                </MoreActions>
                <ReactTooltip id="more_actions" delayHide={300} effect="solid" role="tooltip" />
              </Box>
            )}
          </Box>
        </Box>
        {showChangedMessage && (
          <div
            className={`roleChangedMessage alert member${member.id} alert-success`}
            role="alert"
            style={{ marginTop: '7px' }}
          >
            <FormattedMessage id="member.role.changed" defaultMessage="role was updated" />
          </div>
        )}
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
