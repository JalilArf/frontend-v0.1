import { FormattedMessage } from 'react-intl';
import Link from 'next/link';
import Loading from '~/components/Tools/Loading';
import NeedWorkersDelete from './NeedWorkersDelete';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import ReactTooltip from 'react-tooltip';
const NeedWorkers = ({ need = undefined, mode }) => {
  const modal = useModal();
  const api = useApi();
  const needMembers = need.users_sm;
  const showAllWorkers = () => {
    api.get(`/api/needs/${need.id}/members`).then((res) => {
      // get all need members, then show modal showing them
      modal.showModal({
        children: (
          <Grid gridGap={[2, 4]} gridCols={[1, 2]} display={['grid', 'inline-grid']} py={[0, 2, 4]}>
            {res.data.members.map((member, i) => (
              <UserCard
                key={i}
                id={member.id}
                firstName={member.first_name}
                lastName={member.last_name}
                nickName={member.nickname}
                shortBio={member.short_bio}
                logoUrl={member.logo_url}
                canContact={member.can_contact}
                hasFollowed={member.has_followed}
                mutualCount={member.stats.mutual_count}
              />
            ))}
          </Grid>
        ),
        maxWidth: '60rem',
      });
    });
  };

  return (
    <div className="needWorkers">
      <FormattedMessage id="need.card.working" defaultMessage="Working on it: " />
      <span className="workers">
        <span className="imgList">
          {needMembers ? (
            needMembers.map((user, index) => {
              const logoUrl = !user.logo_url ? '/images/default/default-user.png' : user.logo_url;
              if (index < 6) {
                return (
                  <>
                    <Link href={`/user/${user.id}`} key={index}>
                      <a>
                        <img
                          className="userImg"
                          src={logoUrl}
                          alt={`${user.first_name} ${user.last_name}`}
                          data-tip={`${user.first_name} ${user.last_name}`}
                          data-for="need_worker"
                        />
                        <ReactTooltip id="need_worker" effect="solid" className="forceTooltipBg" />
                      </a>
                    </Link>
                    {mode === 'update' && need.is_owner && (
                      <NeedWorkersDelete worker={user} itemId={need.id} itemType="needs" />
                    )}
                  </>
                );
              }
              return '';
            })
          ) : (
            <Loading />
          )}
          {need.members_count > 6 && (
            <div
              className="moreMembers"
              onClick={showAllWorkers}
              onKeyUp={(e) =>
                // execute only if it's the 'enter' key
                (e.which === 13 || e.keyCode === 13) && showAllWorkers()
              }
              tabIndex={0}
            >
              +{need.members_count - 6}
            </div>
          )}
        </span>
      </span>
    </div>
  );
};
export default NeedWorkers;
