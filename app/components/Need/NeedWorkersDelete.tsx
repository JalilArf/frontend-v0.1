import { Component } from 'react';
import { injectIntl, IntlShape } from 'react-intl';
import { ApiContext } from '~/contexts/apiContext';
import { ItemType } from '~/types';
import ReactTooltip from 'react-tooltip';

interface Props {
  itemId: number;
  itemType: ItemType;
  worker: { id: number };
  intl: IntlShape;
}
interface State {
  sending: boolean;
}
class NeedWorkersDelete extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      sending: false,
    };
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: undefined,
      worker: undefined,
    };
  }

  deleteWorker(worker) {
    const api = this.context;
    const { itemId, itemType, intl } = this.props;
    this.setState({ sending: true });
    api
      .delete(`/api/${itemType}/${itemId}/members/${worker.id}`)
      .then(() => {
        this.setState({ sending: false });
        alert(
          intl.formatMessage({
            id: 'need.user.deleted',
            defaultMessage: 'Member was removed, please refresh the page to see result',
          })
        );
      })
      .catch((err) => {
        console.error(`Couldn't delete ${itemType} itemId=${itemId}`, err);
        alert(
          intl.formatMessage({
            id: 'need.user.deleted',
            defaultMessage: 'Member was removed, please refresh the page to see result',
          })
        );

        this.setState({ sending: false });
      });
  }

  render() {
    const { worker, intl } = this.props;
    const { sending } = this.state;
    if (worker === undefined) {
      // eslint-disable-next-line @rushstack/no-null
      return null;
    }
    return (
      <>
        <button
          type="button"
          className="close delete"
          aria-label="Close"
          disabled={sending}
          onClick={() => this.deleteWorker(worker)}
          data-tip={intl.formatMessage({ id: 'general.remove', defaultMessage: 'delete' })}
          data-for="need_worker_delete"
          // show/hide tooltip on element focus/blur
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
        >
          {sending ? (
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
          ) : (
            <span aria-hidden="true">&times;</span>
          )}
        </button>
        <ReactTooltip id="need_worker_delete" effect="solid" />
      </>
    );
  }
}
NeedWorkersDelete.contextType = ApiContext;
export default injectIntl<undefined, Props>(NeedWorkersDelete);
