import { useRouter } from 'next/router';
import React, { FC } from 'react';
import { useIntl } from 'react-intl';
// import ChallengeMiniCard from '~/components/Challenge/ChallengeMiniCard';
import PostDisplay from '~/components/Feed/Posts/PostDisplay';
import H2 from '~/components/primitives/H2';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Challenge, Need, Post, Project } from '~/types';
import Box from '../Box';
import Carousel from '../Carousel';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import ProjectCard from '../Project/ProjectCard';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import ChallengeCard from '../Challenge/ChallengeCard';

interface Props {
  programId: number;
  programFeedId: number;
  meetingInfo: string;
  isAdmin?: boolean;
  posts?: Post[];
  challenges?: Challenge[];
  refreshPost?: () => void;
}
const ProgramHome: FC<Props> = ({
  programId,
  programFeedId,
  meetingInfo,
  isAdmin = false,
  posts,
  challenges,
  refreshPost,
}) => {
  const router = useRouter();
  const { userData } = useUserData();
  const { data: dataProjects } = useGet<{ projects: Project[] }>(`/api/programs/${programId}/projects?items=7`);
  const { data: dataNeeds } = useGet<{ needs: Need[] }>(`/api/programs/${programId}/needs?items=5`);
  const { formatMessage } = useIntl();

  return (
    <Box spaceY={3} position="relative">
      {/* <Box pb={5}>
        <P display={['none', undefined, 'block']}>{shortDescription}</P>
        <A href={`/program/${router.query.short_title}?tab=about`} shallow>
          {formatMessage({
            id: 'program.home.learnMore',
            defaultMessage: 'Learn more about the Program and the community!',
          })}
        </A>
      </Box> */}
      {/* Challenges section */}
      <Box pb={8}>
        <Box row justifyContent="space-between" alignItems="center">
          <H2>{formatMessage({ id: 'program.home.challengeActive', defaultMessage: 'Active challenges' })}</H2>
          <A href={`/program/${router.query.short_title}?tab=challenges`} shallow scroll={false}>
            {formatMessage({ id: 'program.seeAll', defaultMessage: 'See All' })}
          </A>
        </Box>
        {/* <P>
          {formatMessage({
            id: 'program.home.challengeCta',
            defaultMessage: 'Get involved in Challenges by submitting a project, evaluating submissions, & voting',
          })}
        </P> */}
        {!challenges ? (
          <Loading />
        ) : challenges?.length === 0 ? (
          <NoResults type="challenge" />
        ) : (
          <Carousel spaceX={12}>
            {challenges
              ?.filter(({ status }) => status !== 'draft')
              // display only 3 last challenges
              .map((challenge, index) => (
                <ChallengeCard
                  key={index}
                  id={challenge.id}
                  banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                  short_title={challenge.short_title}
                  title={challenge.title}
                  title_fr={challenge.title_fr}
                  short_description={challenge.short_description}
                  short_description_fr={challenge.short_description_fr}
                  membersCount={challenge.members_count}
                  needsCount={challenge.needs_count}
                  clapsCount={challenge.claps_count}
                  projectsCount={challenge.projects_count}
                  status={challenge.status}
                  has_saved={challenge.has_saved}
                  width="18.4rem"
                />
                // <ChallengeMiniCard key={index} icon={logo_url} title={title} shortTitle={short_title} status={status} />
              ))}
          </Carousel>
        )}
        {/* <Box pt={4}>
            <A href="/project/create">Have a project you'd like to submit?</A>
          </Box> */}
      </Box>
      {/* Projects section */}
      <Box pb={8}>
        <Box row justifyContent="space-between" alignItems="center">
          <H2>{formatMessage({ id: 'program.featuredProjects', defaultMessage: 'Latest Projects' })}</H2>
          <A href={`/program/${router.query.short_title}?tab=projects`} shallow scroll={false}>
            {formatMessage({ id: 'program.seeAll', defaultMessage: 'See All' })}
          </A>
        </Box>
        {!dataProjects ? (
          <Loading />
        ) : dataProjects?.projects.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Carousel spaceX={12}>
            {[...dataProjects.projects]
              .filter(({ challenges }) =>
                // display only projects that have been accepted to the challenge (go through all project's challenges and return true if one of them has project_status === "accepted" )
                Object.values(challenges).some((challenge) => challenge.project_status === 'accepted')
              )
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  clapsCount={project.claps_count}
                  postsCount={project.posts_count}
                  has_saved={project.has_saved}
                  skills={project.skills}
                  banner_url={project.banner_url}
                  width="18.4rem"
                />
              ))}
          </Carousel>
        )}
        <Box pt={4}>
          <A href="/project/create">
            {formatMessage({ id: 'program.ideaProject', defaultMessage: 'Have an idea for a project?' })}
          </A>
        </Box>
      </Box>
      {/* Needs section */}
      <Box pb={8}>
        <Box row justifyContent="space-between" alignItems="center" alignItems="center">
          <H2>{formatMessage({ id: 'program.latestNeeds', defaultMessage: 'Latest needs' })}</H2>
          <A href={`/program/${router.query.short_title}?tab=needs`} shallow scroll={false}>
            {formatMessage({ id: 'program.seeAll', defaultMessage: 'See All' })}
          </A>
        </Box>
        {!dataNeeds?.needs ? (
          <Loading />
        ) : dataNeeds?.needs?.length === 0 ? (
          <NoResults type="need" />
        ) : (
          <Carousel spaceX={12}>
            {[...dataNeeds.needs].map((need, i) => (
              <NeedCard
                key={i}
                title={need.title}
                project={need.project}
                skills={need.skills}
                resources={need.ressources}
                hasSaved={need.has_saved}
                id={need.id}
                postsCount={need.posts_count}
                membersCount={need.members_count}
                publishedDate={need.created_at}
                dueDate={need.end_date}
                status={need.status}
                width="18.4rem"
              />
            ))}
          </Carousel>
        )}
        <Box pt={4}>
          <A href="/search/needs">
            {formatMessage({ id: 'program.needsMatch', defaultMessage: 'Want to see needs that match you skills?' })}
          </A>
        </Box>
      </Box>
      {/* Posts bloc */}
      <Box pb={8}>
        <Box row justifyContent="space-between" alignItems="center">
          <H2>{formatMessage({ id: 'program.announcements', defaultMessage: 'Announcements' })}</H2>
          <A href={`/program/${router.query.short_title}?tab=news`} shallow scroll={false}>
            {formatMessage({ id: 'program.seeAll', defaultMessage: 'See All' })}
          </A>
        </Box>
        {!posts ? (
          <Loading />
        ) : posts?.length === 0 ? (
          <NoResults type="post" />
        ) : (
          <Carousel spaceX={12}>
            {[...posts].map((post, i) => (
              <PostDisplay
                post={post}
                key={i}
                feedId={programFeedId}
                user={userData}
                refresh={refreshPost}
                isAdmin={isAdmin}
                cardNoComments
                width="18.4rem"
              />
            ))}
          </Carousel>
        )}
      </Box>
      {/* Meeting info bloc */}
      {meetingInfo && (
        <Box
          display={['flex', undefined, undefined, undefined, 'none']}
          bg="white"
          shadow
          border="2px solid lightgrey"
          borderRadius="1rem"
          p={3}
        >
          <H2>{formatMessage({ id: 'program.meeting_information', defaultMessage: "Let's meet!" })}</H2>
          <InfoHtmlComponent title="" content={meetingInfo} />
        </Box>
      )}
    </Box>
  );
};
export default ProgramHome;
