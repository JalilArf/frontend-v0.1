import { FC } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Link from 'next/link';
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import Box from '../Box';
import Alert from '../Tools/Alert';
import { Program } from '~/types';
// import "./ProgramForm.scss";

interface Props {
  mode: 'edit' | 'create';
  program: Program;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const ProgramForm: FC<Props> = ({ mode, program, sending = false, hasUpdated = false, handleChange, handleSubmit }) => {
  const { formatMessage } = useIntl();

  const renderBtnsForm = () => {
    const textAction = mode === 'edit' ? 'Update' : 'Create';
    const urlBack = mode === 'edit' ? `/program/${program.short_title}` : '/';

    return (
      <>
        <Box row justifyContent="center" mt={5} mb={3}>
          <Link href={urlBack}>
            <a>
              <button type="button" className="btn btn-outline-primary">
                <FormattedMessage id="entity.form.back" defaultMessage="Back" />
              </button>
            </a>
          </Link>
          <button
            type="button"
            onClick={handleSubmit}
            className="btn btn-primary"
            disabled={sending}
            style={{ marginLeft: '10px' }}
          >
            {sending && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id={`entity.form.btn${textAction}`} defaultMessage={textAction} />
          </button>
        </Box>
        {hasUpdated && (
          <Alert
            type="success"
            message={
              <FormattedMessage
                id="general.editSuccessMsg"
                defaultMessage="The changes have been saved successfully."
              />
            }
          />
        )}
      </>
    );
  };

  return (
    <form className="programForm">
      <FormDefaultComponent
        id="title"
        content={program.title}
        title={formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
        placeholder={formatMessage({
          id: 'program.form.title.placeholder',
          defaultMessage: 'A Great Program',
        })}
        onChange={handleChange}
        mandatory
      />
      <FormDefaultComponent
        id="title_fr"
        content={program.title_fr}
        title={formatMessage({ id: 'entity.info.title_fr', defaultMessage: 'Title fr' })}
        placeholder={formatMessage({ id: 'program.form.title.placeholder', defaultMessage: 'A Great Program' })}
        onChange={handleChange}
      />
      <FormDefaultComponent
        id="short_title"
        content={program.short_title}
        title={formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
        placeholder={formatMessage({
          id: 'program.form.short_title.placeholder',
          defaultMessage: 'agreatprogram',
        })}
        onChange={handleChange}
        prepend="#"
        mandatory
        pattern={/[A-Za-z0-9]/g}
      />

      <FormTextAreaComponent
        content={program.short_description}
        id="short_description"
        maxChar={500}
        onChange={handleChange}
        rows={3}
        title={formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
        placeholder={formatMessage({
          id: 'program.form.short_description.placeholder',
          defaultMessage: 'The program briefly explained',
        })}
        mandatory
      />
      <FormTextAreaComponent
        content={program.short_description_fr}
        id="short_description_fr"
        maxChar={500}
        onChange={handleChange}
        rows={3}
        title={formatMessage({ id: 'entity.info.short_description_fr', defaultMessage: 'Short description FR' })}
        placeholder={formatMessage({
          id: 'program.form.short_description.placeholder',
          defaultMessage: 'The program briefly explained',
        })}
      />
      <FormWysiwygComponent
        id="description"
        content={program.description}
        title={formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
        placeholder={formatMessage({
          id: 'program.form.description.placeholder',
          defaultMessage: 'Describe the program in detail, with formatted text, images...',
        })}
        onChange={handleChange}
        show
      />
      <FormWysiwygComponent
        id="description_fr"
        content={program.description_fr}
        title={formatMessage({ id: 'entity.info.description_fr', defaultMessage: 'Description fr' })}
        placeholder={formatMessage({
          id: 'program.form.description.placeholder',
          defaultMessage: 'Describe the program in detail, with formatted text, images...',
        })}
        onChange={handleChange}
      />
      <FormWysiwygComponent
        id="enablers"
        content={program.enablers}
        title={formatMessage({ id: 'entity.info.enablers', defaultMessage: 'Enablers' })}
        placeholder={formatMessage({
          id: 'program.form.meeting_information.placeholder',
          defaultMessage: 'Add the program sponsors and supporters (you can add link the link to the image)',
        })}
        onChange={handleChange}
      />
      <FormWysiwygComponent
        id="meeting_information"
        content={program.meeting_information}
        title={formatMessage({ id: 'program.form.meeting_information', defaultMessage: 'Meeting information' })}
        placeholder={formatMessage({
          id: 'program.form.meeting_information',
          defaultMessage: 'Meeting information',
        })}
        onChange={handleChange}
      />
      <FormImgComponent
        id="banner_url"
        content={program.banner_url}
        title={formatMessage({ id: 'program.info.banner_url', defaultMessage: 'Program banner' })}
        imageUrl={program.banner_url}
        itemId={program.id}
        type="banner"
        itemType="programs"
        defaultImg="/images/default/default-program.jpg"
        onChange={handleChange}
        tooltipMessage={formatMessage({
          id: 'challenge.info.banner_url.tooltip',
          defaultMessage:
            'For an optimal display, choose a visual in the format 1240 x 400 pixels (accepted formats: .png, .jpeg, .jpg; maximum weight: 2Mo).',
        })}
      />
      <FormImgComponent
        id="logo_url"
        content={program.logo_url}
        title={formatMessage({ id: 'program.info.logo_url', defaultMessage: 'Program logo' })}
        imageUrl={program.logo_url}
        itemId={program.id}
        type="avatar"
        itemType="programs"
        defaultImg="/images/default/default-program.jpg"
        onChange={handleChange}
      />
      <FormDropdownComponent
        id="status"
        content={program.status}
        title={formatMessage({ id: 'entity.info.status', defaultMessage: 'Status' })}
        options={['draft', 'soon', 'active', 'completed']}
        onChange={handleChange}
      />
      <FormDefaultComponent
        id="launch_date"
        content={program.launch_date ? program.launch_date.substr(0, 10) : undefined}
        title={formatMessage({ id: 'entity.info.launch_date', defaultMessage: 'Launch date' })}
        onChange={handleChange}
        type="date"
      />
      <FormDefaultComponent
        id="end_date"
        content={program.end_date ? program.end_date.substr(0, 10) : undefined}
        title={formatMessage({ id: 'entity.info.end_date', defaultMessage: 'End date' })}
        onChange={handleChange}
        type="date"
      />
      <FormDefaultComponent
        id="contact_email"
        content={program.contact_email}
        title={formatMessage({ id: 'program.form.contact_email', defaultMessage: 'Contact email' })}
        placeholder={formatMessage({
          id: 'program.form.contact_email.placeholder',
          defaultMessage: 'The program contact email',
        })}
        onChange={handleChange}
        mandatory
      />
      {renderBtnsForm()}
    </form>
  );
};
export default ProgramForm;
