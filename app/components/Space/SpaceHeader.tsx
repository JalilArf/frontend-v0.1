/* eslint-disable camelcase */
import React, { useState, FC, useEffect, useRef } from 'react';
import { FormattedMessage } from 'react-intl';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { flexbox, space, layout, display } from 'styled-system';
import styled from '~/utils/styled';
import BtnFollow from '../Tools/BtnFollow';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import Box from '~/components/Box';
import { useRouter } from 'next/router';
import A from '../primitives/A';
import BtnSave from '../Tools/BtnSave';
import H1 from '~/components/primitives/H1';
import useUserData from '~/hooks/useUserData';
import { renderTeam, useScrollHandler } from '~/utils/utils';
import { textWithPlural } from '~/utils/managePlurals';
import { Members, Space } from '~/types';
import BtnJoin from '../Tools/BtnJoin';

// import "./SpaceHeader.scss";

const Img = styled.img`
  ${[flexbox, space, display, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
`;

const LogoImg = styled.img`
  ${[flexbox, space, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
  width: 50px;
  height: 50px;
`;

const StickyHeading = styled(Box)`
  position: fixed;
  height: 70px;
  top: ${(p) => (p.isSticky ? '64px' : '-1000px')};
  width: 100%;
  z-index: 9;
  border-top: 1px solid grey;
  left: 0;
  transition: top 333ms;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    height: 60px;
    .actions {
      display: none;
    }
    img {
      width: 40px;
      height: 40px;
    }
  }
  > div {
    max-width: 1280px;
    margin: 0 auto;
    width: 100%;
  }
`;

interface Props {
  space: Space;
  lang: string;
  members: Members;
}
const SpaceHeader: FC<Props> = ({ space, members, lang = 'en' }) => {
  const router = useRouter();
  const { userData } = useUserData();
  const [offSetTop, setOffSetTop] = useState();
  const headerRef = useRef();
  const isSticky = useScrollHandler(offSetTop);
  useEffect(() => {
    setOffSetTop(headerRef?.current?.offsetTop);
  }, [headerRef]);

  const {
    id,
    logo_url,
    members_count,
    title,
    title_fr,
    short_title,
    is_admin,
    is_member,
    is_owner,
    // has_followed,
    has_saved,
    status,
  } = space;

  return (
    <Box width="100%" py={4}>
      <Box row ref={headerRef}>
        <Img
          src={logo_url || 'images/jogl-logo.png'}
          style={{ objectFit: 'contain', backgroundColor: 'white' }}
          mr={4}
          ml={[undefined, undefined, 4]}
          mb={[undefined, undefined, 2]}
          width={['5rem', '7rem', '9rem', '10rem']}
          height={['5rem', '7rem', '9rem', '10rem']}
          alignSelf="center"
          alt="space logo"
          mt={[0, undefined, '-90px', '-100px']}
        />
        <Box width="100%" justifyContent="space-between" flexDirection={['column', 'row', 'column', 'row']}>
          <Box flexDirection={['column', undefined, 'row']}>
            <Box pr={[0, undefined, 5]} pb={[5, undefined, 0]}>
              <Box flexDirection={['column', undefined, 'row']} alignItems={['start', undefined, 'center']}>
                <H1 fontSize={['2.18rem', '2.3rem', '2.5rem', '2.78rem']} pr={5}>
                  {lang === 'fr' && title_fr ? title_fr : title}
                </H1>
                {/* display team members logos */}
                <Box row py={[5, undefined, 0]}>
                  {userData && (
                    <>
                      {/* <Box pr={5}> */}
                      <Box>
                        <BtnSave itemType="spaces" itemId={id} saveState={has_saved} />
                      </Box>
                      {/* <BtnFollow followState={has_followed} itemType="spaces" itemId={id} /> */}
                    </>
                  )}
                  <ShareBtns type="space" />
                </Box>
              </Box>
              {is_admin && (
                <Box row>
                  <A href={`/space/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
                    <Box
                      justifyContent="center"
                      alignItems="center"
                      row
                      color="violet"
                      style={{ textDecoration: 'underline' }}
                    >
                      {members_count + ' ' + textWithPlural('member', members_count)}
                    </Box>
                  </A>
                  <Link href={`/space/${short_title}/edit`}>
                    <a style={{ display: 'flex', justifyContent: 'center', marginLeft: '10px' }}>
                      <FontAwesomeIcon icon="edit" />
                      <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
                    </a>
                  </Link>
                </Box>
              )}
            </Box>
            {renderTeam(members, 'space', space?.short_title, members_count, 'internal')}
          </Box>
          <Box spaceY={2}>
            {!is_owner && status !== 'completed' && (
              // show join button only if we are not owner, or if status is different from "completed"
              <BtnJoin
                joinState={is_member}
                itemType="spaces"
                itemId={id}
                textJoin={<FormattedMessage id="challenge.info.btnJoin" defaultMessage="*Become a member" />}
                textUnjoin={<FormattedMessage id="challenge.info.btnUnjoin" defaultMessage="*Leave space" />}
              />
            )}
            {/* <BtnFollow followState={has_followed} itemType="spaces" itemId={id} displayButton /> */}
          </Box>
        </Box>
      </Box>
      {/* heading with minimal infos, that sticks on top of screen, when you reached space logo */}
      <StickyHeading isSticky={isSticky} className="stickyHeading" bg="white" justifyContent="center">
        <Box row alignItems="center" px={[4, undefined, undefined, undefined, 0]}>
          <LogoImg src={space?.logo_url || 'images/jogl-logo.png'} mr={4} alt={`${space?.title} logo`} />
          <H1 fontSize={['1.8rem', '2.18rem']} lineHeight="26px">
            {lang === 'fr' && space?.title_fr ? space?.title_fr : space?.title}
          </H1>
          <Box row pl={5} className="actions">
            {userData && (
              <>
                <Box pr={5}>
                  <BtnSave itemType="spaces" itemId={id} saveState={has_saved} />
                </Box>
                {/* <BtnFollow followState={has_followed} itemType="spaces" itemId={id} /> */}
              </>
            )}
            <ShareBtns type="space" />
          </Box>
        </Box>
      </StickyHeading>
    </Box>
  );
};

export default SpaceHeader;
