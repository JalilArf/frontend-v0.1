import { FC } from 'react';
import { useIntl } from 'react-intl';
import Box from '~/components/Box';
import H2 from '~/components/primitives/H2';
import useGet from '~/hooks/useGet';
import { Document } from '~/types/common';
import DocumentsList from '../Tools/Documents/DocumentsList';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';

interface Props {
  documents: Document[];
  isAdmin: boolean;
  spaceId: number;
}
const SpaceResources: FC<Props> = ({ spaceId, isAdmin, documents }) => {
  const { data: resourcesList } = useGet(`/api/spaces/${spaceId}/resources`);
  const { formatMessage } = useIntl();
  return (
    <Box>
      <H2 px={[3, 4]}>{formatMessage({ id: 'space.resourcesTitle', defaultMessage: 'Resources' })}</H2>
      {!resourcesList ? (
        <Loading />
      ) : resourcesList?.length === 0 ? (
        ''
      ) : (
        <Box spaceY={7} pt={9}>
          {[...resourcesList].map((resource, i) => (
            <Box>
              <H2 px={[3, 4]} pb={2}>
                {resource.title}
              </H2>
              <Box bg="white" px={[3, 4]}>
                <InfoHtmlComponent title="" content={resource.content} />
              </Box>
            </Box>
          ))}
        </Box>
      )}
      <H2 pt={6}>{formatMessage({ id: 'entity.tab.documents', defaultMessage: 'Documents' })}</H2>
      <DocumentsList
        documents={documents}
        itemId={spaceId}
        isAdmin={isAdmin}
        itemType="spaces"
        cardType="cards"
        showDeleteIcon={false}
      />
    </Box>
  );
};

export default SpaceResources;
