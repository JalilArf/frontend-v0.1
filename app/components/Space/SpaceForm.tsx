import { FC } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Link from 'next/link';
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import Box from '../Box';
import FormToggleComponent from '../Tools/Forms/FormToggleComponent';
import Alert from '../Tools/Alert';
import H2 from '../primitives/H2';
import Grid from '../Grid';
import { Space } from '~/types';
// import "./SpaceForm.scss";

interface Props {
  mode: string;
  space: Space;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (value: any) => void;
  handleChangeFeatured: (value: any) => void;
  handleChangeShowTabs: (value: any) => void;
  handleSubmit: () => void;
}

const SpaceForm: FC<Props> = ({
  mode,
  space,
  sending = false,
  hasUpdated = false,
  handleChange,
  handleChangeFeatured,
  handleChangeShowTabs,
  handleSubmit,
}) => {
  const { formatMessage } = useIntl();

  const renderBtnsForm = () => {
    const textAction = mode === 'create' ? 'Create' : 'Update';
    const urlBack = mode === 'create' ? '/' : `/space/${space.short_title}`;
    return (
      <>
        <Box row justifyContent="center" mt={5} mb={3}>
          <Link href={urlBack}>
            <a>
              <button type="button" className="btn btn-outline-primary">
                <FormattedMessage id="entity.form.back" defaultMessage="Back" />
              </button>
            </a>
          </Link>
          <button
            type="button"
            onClick={handleSubmit}
            className="btn btn-primary"
            disabled={sending}
            style={{ marginLeft: '10px' }}
          >
            {sending && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id={`entity.form.btn${textAction}`} defaultMessage={textAction} />
          </button>
        </Box>
        {hasUpdated && (
          <Alert
            type="success"
            message={
              <FormattedMessage
                id="general.editSuccessMsg"
                defaultMessage="The changes have been saved successfully."
              />
            }
          />
        )}
      </>
    );
  };
  return (
    <form className="spaceForm">
      {/* Basic fields */}
      {(mode === 'edit' || mode === 'create') && (
        <>
          <FormDefaultComponent
            id="title"
            content={space.title}
            title={formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
            placeholder={formatMessage({
              id: 'space.form.title.placeholder',
              defaultMessage: 'A Great Space',
            })}
            onChange={handleChange}
            mandatory
          />
          <FormDefaultComponent
            id="short_title"
            content={space.short_title}
            title={formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
            placeholder={formatMessage({
              id: 'space.form.short_title.placeholder',
              defaultMessage: 'agreatspace',
            })}
            onChange={handleChange}
            prepend="#"
            mandatory
            pattern={/[A-Za-z0-9]/g}
          />
          <FormTextAreaComponent
            content={space.short_description}
            id="short_description"
            maxChar={500}
            onChange={handleChange}
            rows={3}
            title={formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
            placeholder={formatMessage({
              id: 'space.form.short_description.placeholder',
              defaultMessage: 'The space briefly explained',
            })}
            mandatory
          />
        </>
      )}
      {(mode === 'edit_about' || mode === 'create') && (
        <FormWysiwygComponent
          id="description"
          content={space.description}
          title={formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
          placeholder={formatMessage({
            id: 'space.form.description.placeholder',
            defaultMessage: 'Describe the space in detail, with formatted text, images...',
          })}
          onChange={handleChange}
          show
        />
      )}
      {(mode === 'edit_partners' || mode === 'create') && (
        <FormWysiwygComponent
          id="enablers"
          content={space.enablers}
          title={formatMessage({ id: 'entity.info.enablers', defaultMessage: 'Partners' })}
          placeholder={formatMessage({
            id: 'space.form.meeting_information.placeholder',
            defaultMessage: 'Add the space sponsors and supporters (you can add link the link to the image)',
          })}
          onChange={handleChange}
          show
        />
      )}
      {(mode === 'edit' || mode === 'create') && (
        <>
          <FormImgComponent
            id="banner_url"
            content={space.banner_url}
            title={formatMessage({ id: 'space.info.banner_url', defaultMessage: 'Space banner' })}
            imageUrl={space.banner_url}
            itemId={space.id}
            type="banner"
            itemType="spaces"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChange}
            tooltipMessage={formatMessage({
              id: 'challenge.info.banner_url.tooltip',
              defaultMessage:
                'For an optimal display, choose a visual in the format 1240 x 400 pixels (accepted formats: .png, .jpeg, .jpg; maximum weight: 2Mo).',
            })}
          />
          <FormImgComponent
            id="logo_url"
            content={space.logo_url}
            title={formatMessage({ id: 'space.info.logo_url', defaultMessage: 'Space logo' })}
            imageUrl={space.logo_url}
            itemId={space.id}
            type="avatar"
            itemType="spaces"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChange}
          />
          <FormDropdownComponent
            id="status"
            content={space.status}
            title={formatMessage({ id: 'entity.info.status', defaultMessage: 'Status' })}
            options={['draft', 'active', 'completed']}
            onChange={handleChange}
          />
          <FormDefaultComponent
            id="contact_email"
            content={space.contact_email}
            title={formatMessage({ id: 'space.form.contact_email', defaultMessage: 'Contact email' })}
            placeholder={formatMessage({
              id: 'space.form.contact_email.placeholder',
              defaultMessage: 'The space contact email',
            })}
            onChange={handleChange}
            mandatory
          />
        </>
      )}
      {/* Optional spaces tabs */}
      {mode === 'edit' && (
        <>
          <H2 pt={6}>*Optional spaces tabs</H2>
          <Grid display="grid" gridTemplateColumns={['1fr 1fr', '1fr 1fr 1fr 1fr 1fr']} pb={5}>
            <FormToggleComponent
              id="selected_tabs.programs"
              title={formatMessage({
                id: 'general.programs',
                defaultMessage: '*Programs',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.selected_tabs.programs}
              onChange={handleChangeShowTabs}
            />
            <FormToggleComponent
              id="selected_tabs.challenges"
              title={formatMessage({
                id: 'general.challenges',
                defaultMessage: 'Challenges',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.selected_tabs.challenges}
              onChange={handleChangeShowTabs}
            />
            <FormToggleComponent
              id="selected_tabs.resources"
              title={formatMessage({
                id: 'entity.info.resources',
                defaultMessage: 'Resources',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.selected_tabs.resources}
              onChange={handleChangeShowTabs}
            />
            <FormToggleComponent
              id="selected_tabs.enablers"
              title={formatMessage({
                id: 'entity.info.enablers',
                defaultMessage: 'Partners',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.selected_tabs.enablers}
              onChange={handleChangeShowTabs}
            />
            <FormToggleComponent
              id="selected_tabs.faqs"
              title={formatMessage({
                id: 'entity.info.faq',
                defaultMessage: 'FAQs',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.selected_tabs.faqs}
              onChange={handleChangeShowTabs}
            />
          </Grid>
        </>
      )}
      {/* Home tab fields */}
      {mode === 'edit_home' && (
        <>
          <H2>*Show featured objects</H2>
          <Grid display="grid" gridTemplateColumns={['1fr 1fr', '1fr 1fr 1fr 1fr']} pb={5}>
            <FormToggleComponent
              id="show_featured.programs"
              title={formatMessage({
                id: 'general.programs',
                defaultMessage: 'Programs',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.show_featured.programs}
              onChange={handleChangeFeatured}
            />
            <FormToggleComponent
              id="show_featured.challenges"
              title={formatMessage({
                id: 'general.challeges',
                defaultMessage: 'Challenges',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.show_featured.challenges}
              onChange={handleChangeFeatured}
            />
            <FormToggleComponent
              id="show_featured.projects"
              title={formatMessage({
                id: 'general.projects',
                defaultMessage: 'Projects',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.show_featured.projects}
              onChange={handleChangeFeatured}
            />
            <FormToggleComponent
              id="show_featured.needs"
              title={formatMessage({
                id: 'needs.uppercase',
                defaultMessage: 'Needs',
              })}
              choice1={formatMessage({ id: 'general.no', defaultMessage: 'No' })}
              choice2={formatMessage({ id: 'general.yes', defaultMessage: 'Yes' })}
              isChecked={space.show_featured.needs}
              onChange={handleChangeFeatured}
            />
          </Grid>
          <FormDefaultComponent
            id="home_header"
            content={space.home_header}
            title={formatMessage({ id: 'space.form.home_header', defaultMessage: '*Call to action header' })}
            placeholder={formatMessage({
              id: 'space.form.home_header.placeholder',
              defaultMessage: '*Help our organization',
            })}
            onChange={handleChange}
          />
          <FormWysiwygComponent
            id="home_info"
            content={space.home_info}
            title={formatMessage({ id: 'entity.info.home_info', defaultMessage: '*Additional information' })}
            placeholder={formatMessage({
              id: 'space.form.home_info.placeholder',
              defaultMessage: '*home_info placeholder',
            })}
            onChange={handleChange}
            show
          />
        </>
      )}
      {renderBtnsForm()}
    </form>
  );
};
export default SpaceForm;
