const path = require('path');

module.exports = {
  rootDir: path.join(__dirname, '../../'),
  // To resolve some modules
  moduleDirectories: ['node_modules'],
  moduleNameMapper: {
    // Basically ignores static assets like images because if they change we don't car in our tests
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    // For css modules, it will output the name of the object key rather than the generated className.
    // If we change a css property it won't be seen in the tests which is great.
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
    '\\.css$': require.resolve('./style-mock.js'),
    '^~(.*)$': '<rootDir>$1',
  },
  watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname', 'jest-watch-select-projects'],
  coveragePathIgnorePatterns: ['/node_modules/', '/__tests__/', '__server_tests__/'],
  // /////////////////////////////
  testPathIgnorePatterns: ['/node_modules/', '/coverage/', '/dist/', '/other/', '/.next/'],
};
