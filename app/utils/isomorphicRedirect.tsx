import Router from 'next/router';

export default function isomorphicRedirect(ctx, asPath) {
  if (typeof window !== 'undefined') {
    Router.push(asPath);
  } else {
    ctx.res.writeHead(302, { Location: asPath });
    ctx.res.end();
  }
  // Return empty object because getInitialProps doesn't allow null returns
  return {};
}
