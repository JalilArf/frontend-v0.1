import React, { Fragment, useRef, useEffect, useState } from 'react';
import { FormattedMessage, FormattedDate, FormattedRelativeTime, FormattedPlural } from 'react-intl';
import Link from 'next/link';
import { selectUnit } from '@formatjs/intl-utils';
import styled from './styled';
import A from '~/components/primitives/A';
// import ReactGA from 'react-ga';
// import $ from "jquery";

export function linkify(content) {
  // detect links in a text and englobe them with a <a> tag
  const urlRegex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm;
  return content.replace(urlRegex, (url) => {
    const addHttp = url.substr(0, 4) !== 'http' ? 'http://' : '';
    return `<a href="${addHttp}${url}" target="_blank" rel="noopener">${url}</a>`;
  });
}

export function linkifyQuill(text) {
  // detect links outside of <a> tag and linkify them (only for quill content)
  const exp = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
  return text.replace(exp, function () {
    return arguments[1] ? arguments[0] : `<a href="${arguments[3]}" target='_blank' rel="noopener">${arguments[3]}</a>`;
  });
}

export function stickyTabNav(isEdit) {
  // Add the sticky class to the tab navigation when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function stickyScrollTop(sticky) {
    if (window.pageYOffset >= sticky) {
      // when we arrive at the level of the tab navigation div
      navtabs.classList.add('sticky'); // add class sticky
    } else {
      navtabs.classList.remove('sticky');
    }
  }
  // Get the tab navigation
  const navtabs = document.querySelector('.nav-tabs');
  // Get the offset position of the tab navigation
  if (navtabs) {
    // check if nav
    const sticky = navtabs.offsetTop - 80; // equals to the offset from top of navtab - 80px (header height)
    // When the user scrolls the page, execute stickyScrollTop
    stickyScrollTop(sticky); // launch function once
    window.onscroll = function () {
      stickyScrollTop(sticky);
    }; // redo function on scroll

    $('.nav-tabs .nav-item.nav-link').on('click', () => {
      window.scrollTo({ top: sticky, behavior: 'smooth' }); // stickyValue is dynamically 0 or sticky depending if the tab navigation is sticky
      // TODO fix this
      // if (!isEdit) hash = $(this)[0].hash;
    });
  }
}

export const useScrollHandler = (elOffSetTop: number) => {
  // setting initial value to true
  const [scroll, setScroll] = useState(false);
  // running on mount
  useEffect(() => {
    const onScroll = () => {
      const scrollCheck = window.scrollY > elOffSetTop;
      if (scrollCheck !== scroll) {
        setScroll(scrollCheck);
      }
    };
    // setting the event handler from web API
    document.addEventListener('scroll', onScroll);
    // cleaning up from the web API
    return () => {
      document.removeEventListener('scroll', onScroll);
    };
  }, [scroll, setScroll, elOffSetTop]);
  return scroll;
};

export function scrollToActiveTab(router) {
  const hash =
    typeof window !== 'undefined' && router.asPath.match(/#([a-z0-9]+)/gi)
      ? router.asPath.match(/#([a-z0-9]+)/gi)[0]
      : window.location.hash;
  // takes the hash() in the url, if it match the href of a tab, then force click on the tab to access content
  if (hash && document.body.contains(document.querySelector(`a[href="${hash}"]`))) {
    $(`a[href="${hash}"]`)[0].click();
    const element = document.querySelector('.tab-pane.active'); // get active tab content
    const y = element.getBoundingClientRect().top + window.pageYOffset - 200; // calculate it's top value and remove 140 of offset
    window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to tab
  }
}

export function renderOwnerNames(users, projectCreator) {
  let noOwner = true;
  users.map((user) => {
    if (user.owner) noOwner = false;
  }); // map through users, and set noOwner to false if there are no owners
  return (
    <p className="card-by">
      <FormattedMessage id="entity.card.by" defaultMessage="by " />
      {users
        .filter((user) => user.owner)
        .map((user, index, users) => {
          // filter to get only owners, and then map
          const rowLen = users.length;
          if (index < 6) {
            return (
              <Fragment key={index}>
                <Link href={`/user/${user.id}`}>
                  <a>{`${user.first_name} ${user.last_name}`}</a>
                </Link>
                {/* add comma, except for last item */}
                {rowLen !== index + 1 && <span>, </span>}
              </Fragment>
            );
          }
          if (index === 6) {
            return '...';
          }
          return '';
        })}
      {noOwner && ( // if project don't have any owner, display creator as the owner
        <Link href={`/user/${projectCreator.id}`}>
          <a>{`${projectCreator.first_name} ${projectCreator.last_name}`}</a>
        </Link>
      )}
    </p>
  );
}

const BubbleTeam = styled.div`
  display: flex;
  align-items: center;
  > span:nth-child(1) {
    margin-right: 3px;
  }
  .userImg {
    background-size: cover;
    width: 43px;
    height: 43px;
    border-radius: 50%;
    background-position: center center;
    background-repeat: no-repeat;
    margin: 2px -9px 2px 2px;
    background-color: white;
    border: 1px solid lightgray;
    &:hover {
      margin: 2px -9px 5px 2px;
    }
  }

  a:hover {
    text-decoration: none;
  }

  .moreMembers {
    width: 45px;
    height: 45px;
    background: blue;
    color: white;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    display: inline-flex;
    font-size: 15.8px;
    &:hover {
      opacity: 0.9;
    }
  }
`;

export function renderTeam(users, objType, objSlug, members_count, type = 'external') {
  return (
    <BubbleTeam>
      {users?.map((user, index) => {
        const logoStyle = {
          backgroundImage: `url(${user.logo_url || '/images/default/default-user.png'})`,
        };
        if (index < 6) {
          return (
            <Link href={`/user/${user.id}`} key={index}>
              <a>
                <div className="userImg" style={logoStyle} />
              </a>
            </Link>
          );
        }
        return '';
      })}
      {/* if obj has more than 6 members, show a bubble with the left members number, linking to different source depending on "type" prop */}
      {members_count > 6 && type === 'internal' && (
        <A href={`/${objType}/${objSlug}?tab=members`} shallow noStyle scroll={false}>
          <div className="moreMembers">+{members_count - 6}</div>
        </A>
      )}
      {members_count > 6 && type !== 'internal' && (
        <A href={`/${objType}/${objSlug}`} noStyle>
          <div className="moreMembers">+{members_count - 6}</div>
        </A>
      )}
    </BubbleTeam>
  );
}

export function formatNumber(value) {
  return Number(value).toLocaleString();
}

export function displayObjectDate(date) {
  const objectCreatedDate = new Date(date);
  const { value: objectDate, unit: objectUnit } = selectUnit(objectCreatedDate);
  // check if browser is safari or IE
  var isSafari,
    isIE = false;
  if (typeof window !== 'undefined') {
    isSafari =
      /constructor/i.test(window.HTMLElement) ||
      (function (p) {
        return p.toString() === '[object SafariRemoteNotification]';
      })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    isIE = /*@cc_on!@*/ false || !!document.documentMode;
  }
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // if date diff is less than 1h (3600ec), display related date (.. minutes ago, with update every x seconds)
  if (dateDiff < 3600 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} updateIntervalInSeconds={60} />;
  // else, if date diff less than 1 day (3600sec * 24h), display related date without time update
  if (dateDiff < 86400 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} />;
  // else display date with day, month & year
  return <FormattedDate value={objectCreatedDate} />;
}

export function displayObjectRelativeDate(date) {
  const objectCreatedDate = new Date(date);
  const { value: objectDate, unit: objectUnit } = selectUnit(objectCreatedDate);
  // check if browser is safari or IE
  var isSafari,
    isIE = false;
  if (typeof window !== 'undefined') {
    isSafari =
      /constructor/i.test(window.HTMLElement) ||
      (function (p) {
        return p.toString() === '[object SafariRemoteNotification]';
      })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    isIE = /*@cc_on!@*/ false || !!document.documentMode;
  }
  const diffSeconds = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  const diffDays = Math.round(Math.abs((objectCreatedDate - new Date()) / (24 * 60 * 60 * 1000))); // get days diff
  // if date diff is less than 1h (3600ec), display related date (.. minutes ago, with update every x seconds)
  if (diffSeconds < 3600 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} updateIntervalInSeconds={60} />;
  // if date diff less than 1 day, display related date without time update
  if (diffDays < 1 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} />;
  // if date diff is less than 30 days, force display relative time in days (X days ago)
  if (diffDays < 30 && !isSafari && !isIE) return <FormattedRelativeTime value={-diffDays} unit="day" />;
  // if notif is more than 30 days old, show default relative time (should display X months ago)
  if (diffDays >= 30 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} />;
  // if it's safari or Ie, display date with day, month & year (@TODO fix with polyfill)
  return <FormattedDate value={objectCreatedDate} />;
}

export function isDateUrgent(date) {
  const objectCreatedDate = new Date(date);
  const { value: objectDate, unit: objectUnit } = selectUnit(objectCreatedDate);
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // else, if date diff less than 2 days (3600sec * 24h * 2), display related date without time update
  if (dateDiff < 86400) return true;
}

// return first link of a post (the one we want the metadata from)
export function returnFirstLink(content) {
  if (content) {
    const regexLinks = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm; // match all links
    const match = content.match(regexLinks);
    if (match) {
      const firstLink = match[0];
      return firstLink;
    }
  }
}

export function copyLink(objectId, objectType, commentId) {
  // function to copy post link to user clipboard, we use a tweak because the browser don't like we force copy to clipboard
  const el = document.createElement('textarea'); // create textarea element
  const link =
    objectType !== 'comment'
      ? `${window.location.origin}/${objectType}/${objectId}` // if objectType is not comment, make its value be the object url
      : objectType === 'comment' && `${window.location.origin}/post/${objectId}#comment-${commentId}`; // if it' a comment, make its value be the comment single url
  el.value = link;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute'; // put some style that make it not visible
  el.style.left = '-9999px';
  document.body.appendChild(el); // add element to html (body)
  el.select(); // select the textarea text
  document.execCommand('copy'); // copy content to user clipboard
  document.body.removeChild(el); // remove element
  const alertId = commentId ? commentId : objectId;
  $(`.alert-${alertId}#copyConfirmation`).show(); // display "object was copied" alert
  setTimeout(() => {
    $(`.alert-${alertId}#copyConfirmation`).hide(300);
  }, 3000); // hide it after 2sec
}

export function reportContent(contentType, postId, commentId, api) {
  // function to send mail with the reported post to JOGL admin
  const alertType = contentType === 'post' ? 'reportPostConfirmation' : 'reportCommentConfirmation';
  const param =
    contentType === 'post'
      ? {
          // if it's post
          object: 'Post report', // mail subject
          content: `The user reported the following post: ${window.location.origin}/post/${postId}`, // mail content, with reported post link
        }
      : {
          // if it's comment
          object: 'Comment report', // mail subject
          content: `The user reported the following comment: ${window.location.origin}/post/${postId}#comment-${commentId}`, // mail content, with reported post link
        };
  api
    .post('/api/users/2/send_email', param) // send it to user 2, which is JOGL admin user
    .then((res) => {
      const alertId = commentId ? commentId : postId;
      $(`.alert-${alertId}#${alertType}`).show(); // display "post was reported" alert
      setTimeout(() => {
        $(`.alert-${alertId}#${alertType}`).hide(300);
      }, 2000); // hide it after 2sec
    })
    .catch((error) => {});
}

export function useWhyDidYouUpdate(name, props) {
  // Get a mutable ref object where we can store props ...
  // ... for comparison next time this hook runs.
  const previousProps = useRef();

  useEffect(() => {
    if (previousProps.current) {
      // Get all keys from previous and current props
      const allKeys = Object.keys({ ...previousProps.current, ...props });
      // Use this object to keep track of changed props
      const changesObj = {};
      // Iterate through keys
      allKeys.forEach((key) => {
        // If previous is different from current
        if (previousProps.current[key] !== props[key]) {
          // Add to changesObj
          changesObj[key] = {
            from: previousProps.current[key],
            to: props[key],
          };
        }
      });
    }

    // Finally update previousProps with current props for next hook call
    previousProps.current = props;
  });
}

export function defaultSdgsInterests(formatMessage) {
  return [
    {
      value: 1,
      label: formatMessage({
        id: 'sdg-title-1',
        defaultMessage: 'No poverty',
      }),
      color: '#EC1B30',
    },
    {
      value: 2,
      label: formatMessage({
        id: 'sdg-title-2',
        defaultMessage: 'Zero Hunger',
      }),
      color: '#D49F2B',
    },
    {
      value: 3,
      label: formatMessage({
        id: 'sdg-title-3',
        defaultMessage: 'Good Health and Well-being',
      }),
      color: '#2DA354',
    },
    {
      value: 4,
      label: formatMessage({
        id: 'sdg-title-4',
        defaultMessage: 'Quality Education',
      }),
      color: '#CB233B',
    },
    {
      value: 5,
      label: formatMessage({
        id: 'sdg-title-5',
        defaultMessage: 'Gender Equality',
      }),
      color: '#ED4129',
    },
    {
      value: 6,
      label: formatMessage({
        id: 'sdg-title-6',
        defaultMessage: 'Clean Water and Sanitation',
      }),
      color: '#02B7DF',
    },
    {
      value: 7,
      label: formatMessage({
        id: 'sdg-title-7',
        defaultMessage: 'Affordable and Clean Energy',
      }),
      color: '#FEBF12',
    },
    {
      value: 8,
      label: formatMessage({
        id: 'sdg-title-8',
        defaultMessage: 'Decent Work and Economic Growth',
      }),
      color: '#981A41',
    },
    {
      value: 9,
      label: formatMessage({
        id: 'sdg-title-9',
        defaultMessage: 'Industry, Innovation, and Infrastructure',
      }),
      color: '#F37634',
    },
    {
      value: 10,
      label: formatMessage({
        id: 'sdg-title-10',
        defaultMessage: 'Reduced Inequalities',
      }),
      color: '#DE1768',
    },
    {
      value: 11,
      label: formatMessage({
        id: 'sdg-title-11',
        defaultMessage: 'Sustainable Cities and Communities',
      }),
      color: '#FA9D26',
    },
    {
      value: 12,
      label: formatMessage({
        id: 'sdg-title-12',
        defaultMessage: 'Responsible Consumption and Production',
      }),
      color: '#C69932',
    },
    {
      value: 13,
      label: formatMessage({
        id: 'sdg-title-13',
        defaultMessage: 'Climate Action',
      }),
      color: '#498A50',
    },
    {
      value: 14,
      label: formatMessage({
        id: 'sdg-title-14',
        defaultMessage: 'Life Below Water',
      }),
      color: '#347DB7',
    },
    {
      value: 15,
      label: formatMessage({
        id: 'sdg-title-15',
        defaultMessage: 'Life on Land',
      }),
      color: '#40B04A',
    },
    {
      value: 16,
      label: formatMessage({
        id: 'sdg-title-16',
        defaultMessage: 'Peace, Justice, and Strong Institutions',
      }),
      color: '#02558B',
    },
    {
      value: 17,
      label: formatMessage({
        id: 'sdg-title-17',
        defaultMessage: 'Partnership for the Goals',
      }),
      color: '#1B366B',
    },
  ];
}
