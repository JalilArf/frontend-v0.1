import { CommentDots } from '@emotion-icons/boxicons-regular/CommentDots';
import { Feed } from '@emotion-icons/icomoon/Feed';
import { PersonPlusFill } from '@emotion-icons/bootstrap/PersonPlusFill';
import { Mention } from '@emotion-icons/octicons/Mention';
import { AdminPanelSettings } from '@emotion-icons/material/AdminPanelSettings';
import { Event } from '@emotion-icons/material/Event';
import { UserGroup } from '@emotion-icons/heroicons-outline/UserGroup';
import { SignLanguage } from '@emotion-icons/fa-solid/SignLanguage';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import styled from '~/utils/styled';

export const NotifIconWrapper = styled.div`
  ${EmotionIconBase} {
    color: ${(p) => p.theme.colors.primary};
    width: 25px;
    height: 25px;
  }
`;

export const notifIconTypes = {
  administration: <AdminPanelSettings />,
  clap: <SignLanguage />,
  comment: <CommentDots />,
  follow: <PersonPlusFill />,
  feed: <Feed />,
  program: <Event />,
  mention: <Mention />,
  membership: <UserGroup />,
  space: <Event />,
};
