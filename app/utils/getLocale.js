const parseCookies = (headers) => {
  const list = {};
  const rc = headers.cookie;
  if (rc) {
    rc.split(";").forEach((item) => {
      const parts = item.split("=");
      list[parts.shift().trim()] = decodeURI(parts.join("="));
    });
  }
  return list;
};
const defaultLocale = "en";

const getLocale = (headers, acceptedLocale, supportedLanguages) => {
  try {
    const cookieLocale = parseCookies(headers).locale;
    let locale = defaultLocale;
    if (cookieLocale) {
      // check if user has set locale
      locale = supportedLanguages.includes(cookieLocale) ? cookieLocale : defaultLocale;
    } else {
      // check if user has set locale
      const systemLocale = acceptedLocale || navigator.language || defaultLocale;
      locale = systemLocale;
      // cookie.set("locale", locale, { expires: 1 });
    }
    return locale;
  } catch (error) {
    console.error(error);

    return defaultLocale;
  }
};
module.exports = getLocale;
