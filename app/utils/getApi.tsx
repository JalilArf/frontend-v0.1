import axios from 'axios';
import nextCookie from 'next-cookies';
import { useRouter } from 'next/router';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import Button from '~/components/primitives/Button';
import { INotLoggedInModalContext } from '~/contexts/notLoggedInModalContext';

const URL = process.env.ADDRESS_BACK;

const getApi = ({ accessToken, client, uid, userId }, notLoggedInModal?: INotLoggedInModalContext) => {
  // Create the api
  const api = axios.create({
    baseURL: URL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

  // Intercepts requests to add the credentials
  // cookies to every axios requests if connected
  api.interceptors.request.use(
    (config) => {
      if (accessToken) {
        const common = { 'access-token': accessToken, client: client, uid: uid, userId: userId };
        // add the new common without deleting some extra keys in config
        return { ...config, headers: { ...config.headers, common: { ...config.headers.common, ...common } } };
      }
      return config;
    },
    (error) => Promise.reject(error)
  );

  // Intercepts responses to handles errors.
  api.interceptors.response.use(
    (response) => response,
    (error) => {
      const status = error?.response?.status;
      // Handle 401 unauthorized error.
      if (status === 401) {
        // if 401 error on all pages except signin
        if (document?.location?.pathname !== '/signin') {
          if (notLoggedInModal) {
            const { showModal } = notLoggedInModal;
            // ! The reason we cannot use the classic ModalContext is that it isn't available in this scope
            // ! since ModalProvider must be a child of UserProvider and ApiProvider to add any type of component;
            // ! That explains the existence of the NotLoggedInContext which won't have access to the context the contexts cited above
            // ! Which is fine.
            showModal({
              title: 'You must be logged in',
              titleId: 'footer.modalSignIn.title',
              maxWidth: '25rem',
            });
          }
        }
      }
      // ? To get better errors when using e.g. api.get() it should return only the error without the Promise
      // ? But when using SWR, it requires an error as a Promise to use errors. So maybe we should leave it with the Promise
      // ? to enforce the usage of SWR.
      return Promise.reject(error);
    }
  );
  return api;
};

/**
 * @param {*} ctx NextJS getInitialProps Context
 * @returns api
 */
export const getApiFromCtx = (ctx) => {
  // Get the cookies if they are present
  const { 'access-token': accessToken, uid, client, userId } = nextCookie(ctx);
  return getApi({ accessToken, client, uid, userId });
};

export const NotLoggedInModal = ({ hideModal }) => {
  // ! When using this component with NotLoggedInModalContext, it won't have access to the ApiContext nor the UserContext.
  const router = useRouter();
  return (
    <>
      <p>
        <FormattedMessage id="footer.modalSignIn.text" defaultMessage="Please log in" />
      </p>
      <Button
        onClick={() => {
          hideModal();
          router.push({ pathname: '/signin', query: { redirectUrl: router.asPath } });
        }}
      >
        {/* Pass current URL to sign in, so that it redirects there after sign in */}
        <FormattedMessage id="header.signIn" defaultMessage="Sign in" />
      </Button>
    </>
  );
};

export default getApi;
