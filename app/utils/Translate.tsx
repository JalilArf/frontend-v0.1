import React, { FC } from 'react';
import { FormattedMessage } from 'react-intl';
interface Props {
  id: keyof typeof import('../lang/en.json');
  defaultMessage: string;
  values?: Record<string, any>;
}
const Translate: FC<Props> = ({ id, defaultMessage, values }) => (
  <FormattedMessage id={id} defaultMessage={defaultMessage} values={values} />
);
export default Translate;
