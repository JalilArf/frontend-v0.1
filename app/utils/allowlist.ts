// Environment variables
const { SPACES_ALLOWLIST } = process.env;

/**
 * Array of user ids that are allowed to access Spaces feature
 */
const spacesAllowList = SPACES_ALLOWLIST?.split(',').map((x) => Number(x.trim()));

/**
 * Determines whether a given user is allowed to access the Spaces feature by comparing
 * their user id with entries in an allowed list.
 * @param userId A user id
 */
export const isSpacesAllowed = (userId: number) => Boolean(spacesAllowList?.includes(userId));
