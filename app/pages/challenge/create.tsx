import { useIntl } from 'react-intl';
import { NextPage } from 'next';
import { ReactNode, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '~/components/Layout';
import ChallengeForm from '~/components/Challenge/ChallengeForm';
import Alert from '~/components/Tools/Alert';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';

const ChallengeCreate: NextPage = () => {
  const api = useApi();
  const router = useRouter();
  const { formatMessage } = useIntl();
  const [error, setError] = useState<string | ReactNode>('');
  const [loading, setLoading] = useState(true);
  const [sending, setSending] = useState(false);
  const { userData } = useUserData();
  const [newChallenge, setNewChallenge] = useState({
    title: '',
    title_fr: '',
    short_title: '',
    short_description: '',
    short_description_fr: '',
    // interests: [],
    // skills: [],
    status: 'draft',
    creator_id: undefined,
  });
  // Check for :challenges_creator role on backend
  const { data: canCreate, error: cannotCreate } = useGet('/api/challenges/can_create', { user: userData });
  useEffect(() => {
    if (canCreate) {
      setLoading(false);
    }
    if (cannotCreate) {
      setLoading(false);
      router.push('/challenge/forbidden');
    }
  }, [canCreate, cannotCreate]);

  useEffect(() => {
    if (userData) {
      handleChange('creator_id', userData.id);
    }
  }, [userData]);

  const handleChange = (key, content) => {
    setNewChallenge((prev) => ({ ...prev, [key]: content }));
    setError('');
  };

  const handleSubmit = () => {
    if (newChallenge.title !== '' && newChallenge.short_description !== '' && newChallenge.short_title !== '') {
      setSending(true);
      api
        // check if short_title already exists
        .get(`/api/challenges/exists/${newChallenge.short_title}`)
        // if it's not the case, then we creat the challenge
        .then((res) => {
          if (res.data.data === 'short_title is available') {
            api
              .post('/api/challenges/', newChallenge)
              .then((res) => {
                setSending(false);
                router.push(`/challenge/${res.data.short_title}/edit`);
              })
              .catch(() => {
                setSending(false);
              });
          }
        })
        // else, show error message
        .catch((err) => {
          setSending(false);
          if (err.response.data.data === 'short_title already exists') {
            setError(formatMessage({ id: 'err-4006', defaultMessage: 'Short title is already taken' }));
          }
        });
    } else {
      // show error msg if title, short_desc or short_title are missing
      setError(formatMessage({ id: 'challenge.create', defaultMessage: 'Some information are missing' }));
    }
  };

  if (canCreate) {
    return (
      <Layout title="Create a new challenge | JOGL">
        <Loading active={loading}>
          <div className="challengeCreate container-fluid">
            <h1>{formatMessage({ id: 'challenge.create.title', defaultMessage: 'Create a new challenge' })}</h1>
            <ChallengeForm
              challenge={newChallenge}
              handleChange={handleChange}
              handleSubmit={handleSubmit}
              mode="create"
              sending={sending}
            />
            {error !== '' && <Alert type="danger" message={error} />}
          </div>
        </Loading>
      </Layout>
    );
  }

  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ChallengeCreate;
