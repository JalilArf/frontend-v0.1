import { useState, useContext, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { useRouter } from 'next/router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import ChallengeForm from '~/components/Challenge/ChallengeForm';
import Layout from '~/components/Layout';
import MembersList from '~/components/Members/MembersList';
import DocumentsManager from '~/components/Tools/Documents/DocumentsManager';
import ProjectAdminCard from '~/components/Project/ProjectAdminCard';
import { UserContext } from '~/contexts/UserProvider';
import { stickyTabNav, scrollToActiveTab } from '~/utils/utils';
import { useApi } from '~/contexts/apiContext';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import useGet from '~/hooks/useGet';
import Grid from '~/components/Grid';
import Loading from '~/components/Tools/Loading';
import Button from '~/components/primitives/Button';
import { useModal } from '~/contexts/modalContext';
import { NextPage } from 'next';
import Translate from '~/utils/Translate';
import { ProjectLinkModal } from '~/components/Project/ProjectLinkModal';
import Box from '~/components/Box';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import ManageFaq from '~/components/Tools/ManageFaq';
import { Program } from '~/types';

interface Props {
  challenge: {
    id: number;
    title: string;
    short_title: string;
    status: string;
    is_member: boolean;
    program: Program;
    is_owner: boolean;
  };
}
const ChallengeEdit: NextPage<Props> = ({ challenge: challengeProp }) => {
  const [challenge, setChallenge] = useState(challengeProp);
  const [updatedProgram, setUpdatedProgram] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [urlBack, setUrlBack] = useState(`/challenge/${challenge?.short_title}`);
  const userContext = useContext(UserContext);
  const api = useApi();
  const router = useRouter();
  const { data: projectsData, error: projectsError, revalidate: projectsRevalidate, mutate: mutateProjects } = useGet(
    `/api/challenges/${challenge.id}/projects`
  );
  const { showModal, setIsOpen } = useModal();

  const handleChange = (key, content) => {
    setChallenge((prevChallenge) => ({ ...prevChallenge, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedProgram((prevUpdatedProgram) => ({ ...prevUpdatedProgram, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .patch(`/api/challenges/${challenge.id}`, { challenge: updatedProgram })
      .then(() => {
        setSending(false);
        setUpdatedProgram(undefined); // reset updated program component
        setHasUpdated(true); // show update confirmation message
        setTimeout(() => {
          setHasUpdated(false);
        }, 3000); // hide confirmation message after 3 seconds
        // router.push({ pathname: urlBack, query: { success: 1 } });
      })
      .catch(() => setSending(false));
  };

  useEffect(() => {
    setTimeout(() => {
      stickyTabNav('isEdit'); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  useEffect(() => {
    setUrlBack(`/challenge/${challenge?.short_title}`);
  }, [challenge]);

  useEffect(() => {
    if (!userContext.isConnected) {
      // redirect to projects page if not connected, not admin, if project doesn't exist, or was just deleted
      router.push('/search/challenges').then(() => window.scrollTo(0, 0)); // force scroll to top of page @TODO might not need anymore once this issue will be fixed: https://github.com/vercel/next.js/issues/15206
    }
  }, [challenge, userContext.isConnected]);

  return (
    <Layout title={`${challenge.title} | JOGL`}>
      <div className="challengeEdit">
        <Box px={4}>
          <h1>
            <FormattedMessage id="challenge.edit.title" defaultMessage="Edit my Challenge" />
          </h1>
          <Link href={urlBack}>
            <a>
              {/* go back link */}
              <FontAwesomeIcon icon="arrow-left" />
              <FormattedMessage id="challenge.edit.back" defaultMessage="Go back" />
            </a>
          </Link>
        </Box>
        <nav className="nav nav-tabs container-fluid">
          <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
            <FormattedMessage id="entity.tab.basic_info" defaultMessage="Basic information" />
          </a>
          <a className="nav-item nav-link" href="#members" data-toggle="tab">
            <FormattedMessage id="entity.tab.members" defaultMessage="Members" />
          </a>
          <a className="nav-item nav-link" href="#projects" data-toggle="tab">
            <FormattedMessage id="general.projects" defaultMessage="Projects" />
          </a>
          <a className="nav-item nav-link" href="#documents" data-toggle="tab">
            <FormattedMessage id="entity.tab.documents" defaultMessage="Documents" />
          </a>
          <a className="nav-item nav-link" href="#faqs" data-toggle="tab">
            <FormattedMessage id="faq.title" defaultMessage="FAQs" />
          </a>
          <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
            <FormattedMessage id="entity.tab.advanced" defaultMessage="Advanced" />
          </a>
        </nav>
        <div className="tabContainer">
          <div className="tab-content justify-content-center container-fluid">
            <div className="tab-pane active" id="basic_info">
              <ChallengeForm
                challenge={challenge}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                mode="edit"
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </div>
            <div className="tab-pane" id="members">
              <MembersList itemType="challenges" itemId={challenge.id} isOwner={challenge.is_owner} />
            </div>
            <div className="tab-pane" id="projects">
              {projectsData ? (
                <div className="projectsAttachedList">
                  <Box pb={6}>
                    {challenge?.status === 'accepting' && challenge?.program.id !== -1 ? (
                      // show submit button only if challenge is attached to a program and has the status 'accepting'
                      <div className="justify-content-end projectsAttachedListBar">
                        <Button
                          onClick={() => {
                            showModal({
                              children: (
                                <ProjectLinkModal
                                  alreadyPresentProjects={projectsData?.projects}
                                  challengeId={challenge.id}
                                  programId={challenge.program.id}
                                  isMember={challenge.is_member}
                                  mutateProjects={mutateProjects}
                                  closeModal={() => setIsOpen(false)}
                                />
                              ),
                              title: 'Submit a project',
                              titleId: 'attach.project.title',
                              maxWidth: '50rem',
                            });
                          }}
                        >
                          <Translate id="attach.project.title" defaultMessage="Submit a project" />
                        </Button>
                      </div>
                    ) : (
                      // else show informative message
                      <Translate
                        id="attach.project.cantAdd"
                        defaultMessage="If you want to allow projects to be submitted to the challenge, please change challenge status to 'accepting projects'"
                      />
                    )}
                  </Box>
                  <Grid gridCols={1} display={['grid', 'inline-grid']} pt={4}>
                    {projectsData?.projects
                      // sort the array to have the pending projects first
                      .reduce((acc, element) => {
                        if (
                          // find challenge project_status by accessing object with same challenge_id as the accessed challenge id (old way was "element.challenge_status === 'pending'")
                          element.challenges.find((obj) => obj.challenge_id === challenge.id).project_status ===
                          'pending'
                        ) {
                          return [element, ...acc];
                        }
                        return [...acc, element];
                      }, [])
                      // then map through it
                      .map((project, i) => (
                        <ProjectAdminCard
                          key={i}
                          project={project}
                          challengeId={challenge.id}
                          callBack={projectsRevalidate}
                        />
                      ))}
                  </Grid>
                </div>
              ) : (
                <Loading />
              )}
            </div>
            <div className="tab-pane" id="documents">
              <DocumentsManager
                documents={challenge.documents}
                isAdmin={challenge.is_admin}
                itemId={challenge.id}
                itemType="challenges"
              />
            </div>
            <div className="tab-pane" id="faqs">
              <ManageFaq itemType="challenges" itemId={challenge.id} />
            </div>
            <div className="tab-pane" id="advanced">
              <ManageExternalLink itemType="challenges" itemId={challenge.id} />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
ChallengeEdit.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/challenges/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch challenge with short_title=${query.short_title}`, err));
  if (getIdRes?.data?.id) {
    const challengeRes = await api
      .get(`/api/challenges/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch challenge with id=${getIdRes.data.id}`, err));
    // Check if it got the challenge and if the user is admin
    if (challengeRes?.data?.is_admin) return { challenge: challengeRes.data };
  }
  isomorphicRedirect(ctx, '/search/challenges');
};
export default ChallengeEdit;
