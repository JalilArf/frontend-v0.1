import React from 'react';
import { useIntl } from 'react-intl';
import nextCookie from 'next-cookies';
import UserDelete from '~/components/User/Settings/UserDelete';
import UserDownloadInfos from '~/components/User/Settings/UserDownloadInfos';
import UserNotificationsSettings from '~/components/User/Settings/UserNotificationsSettings';
import FormChangePwd from '~/components/Tools/Forms/FormChangePwd';
import Layout from '~/components/Layout';
import Box from '~/components/Box';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import useUserData from '~/hooks/useUserData';
// import "./UserSettings.scss";

const UserSettings = () => {
  const { formatMessage } = useIntl();
  const { userData } = useUserData();
  return (
    <Layout title={`${formatMessage({ id: 'settings.title', defaultMessage: 'Settings' })} | JOGL`}>
      <div className="userSettings">
        <div className="container justify-content-center">
          <h2 className="title">{formatMessage({ id: 'settings.title', defaultMessage: 'Settings' })}</h2>
          <hr className="separator" />
          <UserNotificationsSettings />
          <h3>{formatMessage({ id: 'settings.others', defaultMessage: 'Other Settings' })}</h3>
          <Box className="d-flex" pt={5}>
            <a
              className="btn btn-primary changePwdBtn"
              data-toggle="collapse"
              href="#changePwdBtn"
              role="button"
              aria-expanded="false"
              aria-controls="collapseExample"
            >
              {formatMessage({ id: 'auth.changePwd.title', defaultMessage: 'Change password' })}
            </a>
            <div className="collapse" id="changePwdBtn">
              <FormChangePwd />
              <hr />
            </div>
            <UserDownloadInfos userId={userData?.id} />
            <UserDelete userId={userData?.id} />
          </Box>
        </div>
      </div>
    </Layout>
  );
};

UserSettings.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const { userId } = nextCookie(ctx);
  const res = await api
    .get(`/api/users/${query.id}`)
    .catch((err) => console.error(`Could not fetch user with id=${query.id}`, err));
  // Check if it got the user and if user is the connected user, else redirect to user page
  if (query.id === userId) return { user: res.data.id };
  isomorphicRedirect(ctx, `/user/${query.id}`);
};

export default UserSettings;
