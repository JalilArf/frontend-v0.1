/* eslint-disable global-require */
// Import icons
import {
  faFacebook,
  faInstagram,
  faLinkedin,
  faRedditAlien,
  faTelegramPlane,
  faTwitter,
  faWhatsapp,
} from '@fortawesome/free-brands-svg-icons';
import {
  faBell,
  faBookmark,
  faFile,
  faHandPaper,
  faTimesCircle,
  faBellSlash,
} from '@fortawesome/free-regular-svg-icons';
import {
  faAngleDown,
  faAngleUp,
  faArrowLeft,
  faArrowRight,
  faBars as fasBars,
  faBell as fasBell,
  faBolt as fasBolt,
  faBookmark as fasBookmark,
  faBookOpen as fasBookOpen,
  faCheck as fasCheck,
  faCircle,
  faCode,
  faCog,
  faComment,
  faComments,
  faEdit,
  faEnvelope,
  faExternalLinkAlt,
  faEye,
  faFile as fasFile,
  faFileArchive,
  faFileAudio,
  faFileCode,
  faFileImage,
  faFilePdf,
  faFileVideo,
  faFileWord,
  faFlag,
  faGlobe as fasGlobe,
  faLink,
  faList,
  faQuestionCircle,
  faSearch,
  faShare,
  faShareAlt,
  faSignLanguage,
  faSignOutAlt,
  faTimes,
  faTrash,
  faUserCircle,
} from '@fortawesome/free-solid-svg-icons';

// General imports
// import { CacheProvider as CacheProviderEmotion } from '@emotion/react';
// import { GlobalStyles } from 'twin.macro';
import { config as fontawesomeConfig, library as fontawesomeLibrary } from '@fortawesome/fontawesome-svg-core';
// import { cache as emotionCache } from '@emotion/css';
import { ThemeProvider as ThemeProviderEmotion } from '@emotion/react';
import nextCookie from 'next-cookies';
import App, { AppProps } from 'next/app';
import { Router } from 'next/router';
import nProgress from 'nprogress';
import React, { useState, useEffect } from 'react';
import { createIntl, createIntlCache, RawIntlProvider } from 'react-intl';
import UserProvider from '~/contexts/UserProvider';
import { ApiProvider } from '~/contexts/apiContext';
import { ModalProvider } from '~/contexts/modalContext';
import { NotLoggedInModalProvider } from '~/contexts/notLoggedInModalContext';
import { SpaceViewAsContextProvider } from '~/contexts/SpaceViewAsContext';
import useCookieConsent from '~/hooks/useCookieConsent';
import useGA from '~/hooks/useGA';
import useGTM from '~/hooks/useGTM';
import useHotjar from '~/hooks/useHotjar';
import useSyncLogout from '~/hooks/useSyncLogout';
import { Credentials } from '~/types';
import { getApiFromCtx } from '~/utils/getApi';
import theme from '~/utils/theme';

// Import styles
// This is where we import all our GLOBAL styles
// TODO: most of the files here should be turned into SCSS modules.
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-svg-core/styles.css';
import '@reach/menu-button/styles.css';
import '~/components/Auth.scss';
import '~/components/Cards.scss';
import '~/components/Challenge/ChallengeHeader.scss';
import '~/components/Community/CommunityForm.scss';
import '~/components/Feed/Comments/CommentCreate.scss';
import '~/components/Feed/Comments/CommentDisplay.scss';
import '~/components/Feed/Feed.scss';
import '~/components/Feed/Posts/PostCreate.scss';
import '~/components/Feed/Posts/PostDisplay.scss';
import '~/components/Footer/Footer.scss';
import '~/components/Members/MembersList.scss';
import '~/components/Need/need.scss';
import '~/components/Need/NeedContent.scss';
import '~/components/Need/NeedDocsManagement.scss';
import '~/components/Need/NeedForm.scss';
import '~/components/Need/NeedWorkers.scss';
import '~/components/Search/Search.scss';
import '~/components/Similar.scss';
import '~/components/Tools/BtnClap.scss';
import '~/components/Tools/BtnUploadFile.scss';
import '~/components/Tools/CustomChip.scss';
import '~/components/Tools/Documents/DocumentCard.scss';
import '~/components/Tools/Documents/DocumentCardFeed.scss';
import '~/components/Tools/DropdownRole.scss';
import '~/components/Tools/Forms/FormDefaultComponent.scss';
import '~/components/Tools/Forms/FormImgComponent.scss';
import '~/components/Tools/Forms/FormResourcesComponent.scss';
import '~/components/Tools/Forms/FormSkillsComponent.scss';
import '~/components/Tools/Forms/FormTextAreaComponent.scss';
import '~/components/Tools/Forms/FormToggleComponent.scss';
import '~/components/Tools/Forms/PostInputMentions.scss';
import '~/components/Tools/Webhooks/Webhook/HookCard.scss';
import '~/components/Tools/Webhooks/Webhook/HookForm.scss';
import '~/components/Tools/Webhooks/Webhooks.scss';
import '~/components/Tools/Info/InfoAddressComponent.scss';
import '~/components/Tools/Info/InfoDefaultComponent.scss';
import '~/components/Tools/Info/InfoHtmlComponent.scss';
import '~/components/Tools/Info/InfoInterestsComponent.scss';
import '~/components/Tools/ShareBtns/ShareBtns.scss';
import '~/components/User/Settings/UserSettings.scss';
import '~/components/User/UserHeader.scss';
import '~/components/User/UserMenu.scss';
import '~/components/User/UserProfile.scss';
import '~/components/User/UserProfileEdit.scss';
import '~/pages/404.scss';
import '~/pages/AlgoliaResultsPages.scss';
import '~/pages/complete-profile/CompleteProfile.scss';
import '~/pages/custom.scss';
import '~/pages/global.scss';
import '~/pages/Home.scss';
import '~/pages/legal.scss';
import '~/pages/nProgress.scss';
import '~/pages/search/search.scss';

// prettier-ignore
fontawesomeLibrary.add(
  faComments, faComment, faEdit, faSignLanguage, faFlag, faSearch, faLink, faExternalLinkAlt,
  faTrash, faFile, faFileAudio, faFileImage, faFilePdf, faFileWord, faFileVideo, faFileArchive,
  faFileCode, faTimes, faTimesCircle, faShare, faShareAlt, faEnvelope, faTwitter, faFacebook,
  faTelegramPlane, faRedditAlien, faWhatsapp, faLinkedin, faInstagram, faUserCircle, faQuestionCircle,
  faCog, faSignOutAlt, faCode, faArrowLeft, faArrowRight, faBookmark, fasBookmark, faBell, fasBell, faBellSlash,
  fasCheck, fasBars, fasGlobe, fasFile, fasBolt, faAngleDown, faAngleUp, faHandPaper, fasBookOpen, faEye, faCircle,
  faList
);
fontawesomeConfig.autoAddCss = false; // Tell Font Awesome to skip adding the CSS automatically since it's being imported above

// This code comes from /with-loading NextJS example (allows loading indicator at the top of the page)
// Current link : https://github.com/zeit/next.js/blob/canary/examples/with-loading/pages/_app.js
Router.events.on('routeChangeStart', () => nProgress.start());
Router.events.on('routeChangeComplete', () => nProgress.done());
Router.events.on('routeChangeError', () => nProgress.done());

// This code comes partially from the official NextJS example called with-react-intl.
// Current link: https://raw.githubusercontent.com/zeit/next.js/canary/examples/with-react-intl/
// This is optional but highly recommended since it prevents memory leak
const intlCache = createIntlCache();

interface Props extends AppProps, Credentials {
  locale: 'fr' | 'es' | 'de' | 'en';
  messages: Record<string, string>;
}
const CustomApp = ({ Component, pageProps, locale, messages, accessToken, uid, client, userId }: Props) => {
  const intl = createIntl({ locale, messages }, intlCache);
  const [credentials, setCredentials] = useState<Credentials>({ accessToken, uid, client, userId });

  // add class "using-mouse" to body if user is using mouse.
  // We do this so we can remove style from all elements on focus, but add a special focus style if user uses the tab key to navigate
  // so all the elements he navigates to will be focused (accessibility purpose)
  const addBodyClass = () => document.body.classList.add('using-mouse');
  const removeBodyClass = (e) => e.keyCode === 9 && document.body.classList.remove('using-mouse');
  useEffect(() => {
    window.addEventListener('mousedown', addBodyClass);
    window.addEventListener('keydown', removeBodyClass);
    return () => {
      window.removeEventListener('mousedown', addBodyClass);
      window.removeEventListener('keydown', removeBodyClass);
    };
  });

  useSyncLogout(setCredentials);
  useGA(process.env.GOOGLE_ANALYTICS_ID);
  useGTM(process.env.TAGMANAGER_ARGS_ID);
  useHotjar(process.env.HOTJAR_ID);
  useCookieConsent(locale);

  return (
    // <CacheProviderEmotion value={emotionCache}>
    <>
      {/* <GlobalStyles /> */}
      <ThemeProviderEmotion theme={theme}>
        <RawIntlProvider value={intl}>
          <NotLoggedInModalProvider>
            <ApiProvider credentials={credentials}>
              <UserProvider credentials={credentials} setCredentials={setCredentials}>
                <ModalProvider>
                  <SpaceViewAsContextProvider>
                    <Component {...pageProps} />
                  </SpaceViewAsContextProvider>
                </ModalProvider>
              </UserProvider>
            </ApiProvider>
          </NotLoggedInModalProvider>
        </RawIntlProvider>
      </ThemeProviderEmotion>
    </>
    // </CacheProviderEmotion>
  );
};
CustomApp.getInitialProps = async (appContext) => {
  // Get the cookies if they are present
  const { 'access-token': accessToken, uid, client, userId } = nextCookie(appContext.ctx);
  // Configure api so it can be use in get initial props (ctx.api).
  const api = getApiFromCtx(appContext.ctx);
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);

  // Get the `locale` and `messages` from the request object on the server.
  // In the browser, use the same values that the server serialized.
  // @ts-ignore
  const { locale, messages } = appContext.ctx.req || window.__NEXT_DATA__.props;

  return { ...appProps, locale, messages, accessToken, uid, client, userId, api };
};

export default CustomApp;
