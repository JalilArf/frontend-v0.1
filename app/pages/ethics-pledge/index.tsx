import { NextPage } from 'next';
import { useIntl } from 'react-intl';
import Layout from '~/components/Layout';
// import "./legal.scss";

const EthicsPledge: NextPage = () => {
  const { formatMessage } = useIntl();
  const title = `${formatMessage({ id: 'ethics.title', defaultMessage: 'Awareness and Ethics Pledge' })} | JOGL`;
  return (
    <Layout title={title}>
      <div className="container-fluid legal">
        <h1>{formatMessage({ id: 'ethics.title', defaultMessage: 'Awareness and Ethics Pledge' })}</h1>
        <h2>{formatMessage({ id: 'ethics.aware.title', defaultMessage: 'JOGL’s Awareness' })}</h2>
        <h4>{formatMessage({ id: 'ethics.aware.1', defaultMessage: 'Freedom to experiment' })}</h4>
        <p>
          {formatMessage({
            id: 'ethics.aware.1.text',
            defaultMessage:
              'There is no monopoly on great ideas. We consider that any person or organization must be able to initiate and conduct an experiment when it responds to an issue of general interest.',
          })}
        </p>
        <h4>{formatMessage({ id: 'ethics.aware.2', defaultMessage: 'Collective Intelligence' })}</h4>
        <p>
          {formatMessage({
            id: 'ethics.aware.2.text',
            defaultMessage:
              "To address the complexity of today's social and environmental challenges, it is essential to foster interdisciplinary approaches and the exploration of new paths. We are convinced that the resolving power and efficiency of collective intelligence surpasses that of isolated individuals, and that the use of intelligent algorithms can facilitate its emergence. Through our digital platform and our programs, we facilitate serendipity and collective intelligence.",
          })}
        </p>
        <h4>{formatMessage({ id: 'ethics.aware.3', defaultMessage: 'Free access' })}</h4>
        <p>
          {formatMessage({
            id: 'ethics.aware.3.text',
            defaultMessage:
              'We consider that free knowledge and open solutions are essential to respond effectively and inclusively to the challenges of the planet and of humanity. All results produced on JOGL will therefore be freely accessible.',
          })}
        </p>
        <h4>{formatMessage({ id: 'ethics.aware.4', defaultMessage: 'Inclusiveness' })}</h4>
        <p>
          {formatMessage({
            id: 'ethics.aware.4.text',
            defaultMessage:
              'We believe that the only valid evaluation criteria within JOGL are those related to collaboration, impact, quality and scientific and technical validity. Thus, we make no distinction based on gender, nationality, race, religion, social condition or political affiliation between participants and between projects.',
          })}
        </p>
        <h4>{formatMessage({ id: 'ethics.aware.5', defaultMessage: 'Independent but committed' })}</h4>
        <p>
          {formatMessage({
            id: 'ethics.aware.5.text',
            defaultMessage:
              "We are a non-profit organization, an open laboratory where the general interest is a priority goal. Our business and operating model allows us to ensure the independence of projects hosted on JOGL, impartiality in the evaluation of their results, non-monetization of personal information, and full reinvestment of the value created by groups directly within JOGL's missions. JOGL is opposed to any project that leads to polluting technologies, an active destruction of biodiversity, or an increase in the anthropological participation in climate change.",
          })}
        </p>
        <h2>{formatMessage({ id: 'ethics.pledge.title', defaultMessage: 'Ethics pledge' })}</h2>
        <p>{formatMessage({ id: 'ethics.pledge.engage', defaultMessage: 'I commit myself to:' })}</p>
        <h5>{formatMessage({ id: 'ethics.pledge.1', defaultMessage: '- Respect' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.1.text',
            defaultMessage:
              "To be respectful of others and particularly of the rights of humans and ecosystems of the planet, which is part of JOGL's fundamental commitments.",
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.2', defaultMessage: '- Safety' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.2.text',
            defaultMessage: 'Adopt practices that do not endanger me or others.',
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.3', defaultMessage: '- Modesty' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.3.text',
            defaultMessage:
              'Accept that I do not know everything, and that criticism and doubt are fundamental to the scientific spirit and the beating heart of the collective production of knowledge.',
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.4', defaultMessage: '- Transparency' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.4.text',
            defaultMessage:
              'Be transparent and participate in the sharing of ideas, knowledge, data and results in a way that is open and free.',
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.5', defaultMessage: '- Integrity' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.5.text',
            defaultMessage:
              'Be honest, incorruptible and follow a righteousness that leads to the observance of the rights and duties of justice, as long as it is justifiable.',
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.6', defaultMessage: '- Pacifism' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.6.text',
            defaultMessage:
              'Be responsible for the creation of research and innovation for peaceful purposes and respecting the codes of ethics and professional conduct.',
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.7', defaultMessage: '- Responsibility' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.7.text',
            defaultMessage:
              'Be responsible for my actions. This includes compliance with this charter, but also with any action that could harm a person, a group of people or an ecosystem.',
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.8', defaultMessage: '- Collaboration' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.8.text',
            defaultMessage:
              'Listen carefully to any problems and questions from the community and answer them honestly.',
          })}
        </p>
        <h5>{formatMessage({ id: 'ethics.pledge.9', defaultMessage: '- Education' })}</h5>
        <p>
          {formatMessage({
            id: 'ethics.pledge.9.text',
            defaultMessage:
              'Assist as much as possible JOGL users who would benefit from my expertise by sharing my knowledge and experience.',
          })}
        </p>
      </div>
    </Layout>
  );
};

export default EthicsPledge;
