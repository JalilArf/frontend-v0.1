import { useRouter } from 'next/router';
import { FormattedMessage, useIntl } from 'react-intl';
import { NextPage } from 'next';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from '~/contexts/UserProvider';
import Layout from '~/components/Layout';
import SpaceForm from '~/components/Space/SpaceForm';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import { useFeatureFlags } from '~/hooks/useFeatureFlags';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';

const CreateSpace: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [sending, setSending] = useState(false);
  const { formatMessage } = useIntl();
  const [newSpace, setNewSpace] = useState({
    title: '',
    short_title: '',
    logo_url: '',
    description: '',
    short_description: '',
    creator_id: Number(userContext.credentials.userId),
    status: 'draft',
    interests: [],
    skills: [],
    banner_url: '',
  });
  const { userData } = useUserData();
  // Limit spaces access to a set of specific users while the feature is built
  const { isSpacesAllowed } = useFeatureFlags(userData);
  // Check for :spaces_creator role on backend
  const { data: canCreate, error: cannotCreate } = useGet('/api/spaces/can_create', { user: userData });

  // If can't create a space, display forbidden page
  useEffect(() => {
    if (canCreate && isSpacesAllowed) {
      setLoading(false);
    }
    if (cannotCreate) {
      setLoading(false);
      router.push('/space/forbidden');
    }
  }, [canCreate, cannotCreate, isSpacesAllowed]);

  const handleChange = (key, content) => {
    setNewSpace((prevSpace) => ({ ...prevSpace, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      // check if short_title already exists
      .get(`/api/spaces/exists/${newSpace.short_title}`)
      // if it's not the case, then we creat the space
      .then((res) => {
        if (res.data.data === 'short_title is available') {
          api
            .post('/api/spaces/', { space: newSpace })
            .then((res) => {
              setSending(false);
              router.push(`/space/${res.data.short_title}/edit`);
            })
            .catch(() => {
              setSending(false);
            });
        }
      })
      // else, show error message
      .catch((err) => {
        setSending(false);
        if (err.response.data.data === 'short_title already exists') {
          alert(formatMessage({ id: 'err-4006', defaultMessage: 'Short title is already taken' }));
        }
      });
  };

  if (canCreate && isSpacesAllowed) {
    return (
      <Layout title={`${formatMessage({ id: 'space.create.title', defaultMessage: 'Create a new space' })} | JOGL`}>
        <Loading active={loading}>
          <div className="programCreate container-fluid">
            <h1>
              <FormattedMessage id="space.create.title" defaultMessage="Create a new space" />
            </h1>
            <SpaceForm
              mode="create"
              space={newSpace}
              handleChange={handleChange}
              handleSubmit={handleSubmit}
              sending={sending}
            />
          </div>
        </Loading>
      </Layout>
    );
  }

  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default CreateSpace;
