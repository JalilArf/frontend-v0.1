import { NextPage } from 'next';
import { useIntl } from 'react-intl';
import Layout from '~/components/Layout';
import { SearchStateContextProvider, useSearchStateContext } from '~/contexts/searchStateContext';
import { useFeatureFlags } from '~/hooks/useFeatureFlags';
import useUserData from '~/hooks/useUserData';
import { getSearchTab, TabIndex } from '~/components/SearchTabs';
import { useCallback } from 'react';

interface TabLinkProps {
  indexName: string;
  active: boolean;
}

const TabLink: React.FC<TabLinkProps> = ({ indexName, children, active }) => {
  const { setIndex } = useSearchStateContext();
  const handleClick = useCallback(() => setIndex(indexName as TabIndex), [indexName, setIndex]);
  return (
    <div
      className={`indexType ${active ? 'active' : ''}`}
      data-toggle="tab"
      onClick={handleClick}
      onKeyDown={handleClick}
      role="link"
      tabIndex={0}
    >
      {children}
    </div>
  );
};

const SearchPage: React.FC = () => {
  // Possible states: Members, Needs, Projects, Groups, Challenges, Spaces
  let indices = [
    { value: 'members', name: 'User', intlId: 'general.members' },
    { value: 'needs', name: 'Need', intlId: 'entity.tab.needs' },
    { value: 'projects', name: 'Project', intlId: 'general.projects' },
    { value: 'groups', name: 'Community', intlId: 'general.groups' },
    { value: 'challenges', name: 'Challenge', intlId: 'general.challenges' },
  ];
  const { userData } = useUserData();
  // Limit spaces access to a set of specific users while the feature is built
  const { isSpacesAllowed } = useFeatureFlags(userData);
  if (isSpacesAllowed) {
    indices = [...indices, { value: 'spaces', name: 'Spaces', intlId: 'general.spaces' }];
  }
  const { formatMessage } = useIntl();
  const { index } = useSearchStateContext();
  const Tab = getSearchTab(index);

  return (
    <Layout title={`Search ${index} | JOGL`}>
      <div className="searchPageAll AlgoliaResultsPages container-fluid">
        <nav className="nav container-fluid">
          {indices.map(({ value, intlId }, key) => (
            <TabLink key={key} indexName={value} active={value === index}>
              {formatMessage({ id: intlId, defaultMessage: value })}
            </TabLink>
          ))}
        </nav>
        <div className="tabContainer">
          <div className="tab-content">
            <Tab />
          </div>
        </div>
      </div>
    </Layout>
  );
};

const SearchPageIndices: NextPage = () => (
  <SearchStateContextProvider>
    <SearchPage />
  </SearchStateContextProvider>
);

export default SearchPageIndices;
