import Link from 'next/link';
// import Image from 'next/image';
import { NextPage } from 'next';
import { useIntl } from 'react-intl';
import FormChangePwd from '~/components/Tools/Forms/FormChangePwd';
import Layout from '~/components/Layout';
import Image2 from '~/components/Image2';

const ChangePassword: NextPage = () => {
  const intl = useIntl();
  return (
    <Layout
      className="no-margin"
      title={`${intl.formatMessage({ id: 'auth.changePwd.title', defaultMessage: 'Change password' })} | JOGL`}
    >
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link href="/">
            <a>
              <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" unsized />
            </a>
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <FormChangePwd />
        </div>
      </div>
    </Layout>
  );
};
export default ChangePassword;
