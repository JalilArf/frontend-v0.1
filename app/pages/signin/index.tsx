import $ from 'jquery';
import Link from 'next/link';
import ReactGA from 'react-ga';
import Router, { useRouter } from 'next/router';
import { FormattedMessage, useIntl } from 'react-intl';
import { NextPage } from 'next';
import { ReactNode, useCallback, useEffect, useState } from 'react';
import Alert from '~/components/Tools/Alert';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import Box from '~/components/Box';
import useUser from '~/hooks/useUser';
// import Image from 'next/image';
import Image2 from '~/components/Image2';
// import "./Auth.scss";

const Signin: NextPage = () => {
  const router = useRouter();
  const userContext = useUser();
  const api = useApi();
  const intl = useIntl();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [errorMessage, setErrorMessage] = useState<string | ReactNode>('');
  const redirectUrl = (router.query.redirectUrl as string) ? (router.query.redirectUrl as string) : '/';

  const handleChange = (event) => {
    switch (event.target.name) {
      case 'email':
        setEmail(event.target.value);
        break;
      case 'password':
        setPassword(event.target.value);
        break;
      default:
        break;
    }
  };
  const resendVerifEmail = useCallback(() => {
    const param = {
      user: { email },
    };
    api.post('/api/users/resend_confirmation', param).then(() => {
      $('.mail-sent.alert').show(); // display confirmation message
      setTimeout(() => {
        $('.mail-sent.alert').hide(700);
      }, 2400);
    });
  }, [api, email]);

  const handleSubmit = (event) => {
    event.preventDefault();
    userContext
      .signIn(email, password)
      .then((user) => {
        // send event to google analytics
        ReactGA.event({ category: 'User', action: 'signin', label: `${user?.id}` });
        window.gtag('event', 'login', { method: 'Website', userId: user?.id });
        // if user doesn't have SDG or Skills, country or short bio, redirect him to complete-profile page
        if (
          user?.skills?.length === 0 ||
          user?.interests?.length === 0 ||
          user?.short_bio === null ||
          user?.country === null
        ) {
          Router.push('/complete-profile');
        } else {
          // else redirect him to the page he was before signin, or the default
          Router.push(redirectUrl);
        }
      })
      .catch((errors) => {
        if (errors) {
          console.warn(errors?.response?.data?.errors);
          const errorMsg = errors?.response?.data?.errors[0];
          console.warn(errorMsg);
          setError(errorMsg ? errorMsg : '');
        }
      });
  };
  useEffect(() => {
    if (error.includes('Invalid login')) {
      // if error is about invalid login, show this message (translated)
      setErrorMessage(<FormattedMessage id="err-4010" defaultMessage="Invalid email or password. Please try again." />);
    } else if (error.includes('A confirmation email was sent')) {
      // if error is that account has not been validated, show this message (translated) + button to resend mail
      setErrorMessage(
        <>
          <p>
            <FormattedMessage
              id="signIn.resendConf"
              defaultMessage="You didn't validate your account. To activate it, please follow the instructions in the email that was sent to you (might be in your spam). If it's not working, click on this button"
            />
          </p>
          <button className="btn btn-primary" onClick={resendVerifEmail} type="button">
            <FormattedMessage id="signIn.resendConf.btn" defaultMessage="Re-send email" />
          </button>
          <div className="mail-sent alert alert-success" role="alert">
            <FormattedMessage id="signIn.resendConf.msgSent" defaultMessage="Email was sent!" />
          </div>
        </>
      );
    } else {
      // else simply display error
      setErrorMessage(error);
    }
  }, [error, resendVerifEmail]);

  return (
    <Layout
      className="no-margin"
      title={`${intl.formatMessage({ id: 'header.signIn', defaultMessage: 'Sign in' })} | JOGL`}
    >
      <Box pb={[10, undefined, undefined, 0]}>
        <div className="auth-form row align-items-center signIn">
          <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
            <Link href="/">
              <a>
                <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" unsized />
              </a>
            </Link>
          </div>
          <div className="col-12 col-lg-7 rightPannel">
            {router.query?.account_confirmation_success === 'true' && (
              <div className="alert alert-success" role="alert">
                <h4 className="alert-heading">
                  <FormattedMessage id="newJogler.title" defaultMessage="Congratulations !" />
                </h4>
                <p>
                  <FormattedMessage id="newJogler.message" defaultMessage="Your account is now activated on JoGL !" />
                </p>
              </div>
            )}

            <div className="form-content">
              <div className="form-header">
                <h2 className="form-title" id="signModalLabel">
                  <FormattedMessage id="signIn.title" defaultMessage="Sign in" />
                </h2>
                <p>
                  <FormattedMessage id="signIn.description" defaultMessage="Enter your details below." />
                </p>
              </div>
              <div className="form-body">
                <form>
                  <div className="form-group">
                    <label className="form-check-label" htmlFor="email">
                      <FormattedMessage id="auth.email" defaultMessage="Email" />
                    </label>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="form-control"
                      placeholder={intl.formatMessage({
                        id: 'auth.email.placeholder',
                        defaultMessage: 'your.email@domain.com',
                      })}
                      onChange={handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <div className="row rowPwd">
                      <div className="col-5">
                        <label className="form-check-label" htmlFor="password">
                          <FormattedMessage id="signIn.pwd" defaultMessage="Password" />
                        </label>
                      </div>
                      <div className="col-7 text-right forgotPwd">
                        <Link href="/auth/forgot-password">
                          <a className="nav-link" tabIndex="-1">
                            <FormattedMessage id="signIn.forgotPwd" defaultMessage="Forgot your password ?" />
                          </a>
                        </Link>
                      </div>
                    </div>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      className="form-control"
                      placeholder={intl.formatMessage({
                        id: 'signIn.pwd.placeholder',
                        defaultMessage: 'Your password',
                      })}
                      onChange={handleChange}
                    />
                  </div>
                  {error !== '' && <Alert type="danger" message={errorMessage} />}

                  <div className="goToSignUp">
                    <span>
                      <FormattedMessage id="signIn.newJoin" defaultMessage="Don’t have an account?" />
                    </span>
                    <span className="goToSignUp--signup">
                      <Link href="/signup">
                        <a className="nav-link">
                          <FormattedMessage id="header.signUp" defaultMessage="Sign up" />
                        </a>
                      </Link>
                    </span>
                  </div>

                  <button
                    type="submit"
                    className="btn btn-primary btn-block"
                    color="primary"
                    data-testid="signin-button"
                    onClick={handleSubmit}
                  >
                    <FormattedMessage id="signIn.btnSignIn" defaultMessage="Sign in" />
                  </button>
                  {/* <div className="form-group form-check remember">
                    <input type="checkbox" className="form-check-input" id="rememberMe" />
                    <label className="form-check-label" htmlFor="rememberMe">
                      <FormattedMessage id="signIn.remember" defaultMessage="Remember me" />
                    </label>
                  </div> */}
                </form>
              </div>
            </div>
          </div>
        </div>
      </Box>
    </Layout>
  );
};

export default Signin;
