import { FormattedMessage } from 'react-intl';
import Link from 'next/link';
import Layout from '~/components/Layout';
// import styles from "./404.module.scss";

export default function Page404() {
  return (
    <Layout title="404 | JOGL">
      {/* <div className={styles.notFound}> */}
      <div className="notFound">
        <p>
          <FormattedMessage id="err-404.title" defaultMessage="Sorry, that page doesn’t exist!" />
        </p>
        <Link href="/">
          <a>
            <button className="btn btn-primary" type="button">
              <FormattedMessage id="err-404.btn" defaultMessage="Back to Home page" redirectTo="/" />
            </button>
          </a>
        </Link>
      </div>
    </Layout>
  );
}
