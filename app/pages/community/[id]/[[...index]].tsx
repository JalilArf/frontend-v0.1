import $ from 'jquery';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Box from '~/components/Box';
import CommunityHeader from '~/components/Community/CommunityHeader';
import Feed from '~/components/Feed/Feed';
import Grid from '~/components/Grid';
import Layout from '~/components/Layout';
import H2 from '~/components/primitives/H2';
import Alert from '~/components/Tools/Alert';
import InfoHtmlComponent from '~/components/Tools/Info/InfoHtmlComponent';
import useGet from '~/hooks/useGet';
import { Community } from '~/types';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import styled from '~/utils/styled';
import { scrollToActiveTab, stickyTabNav } from '~/utils/utils';
// import "Components/Main/Similar.scss";

interface Props {
  community: Community;
}
const CommunityDetails: NextPage<Props> = ({ community }) => {
  const router = useRouter();
  const { formatMessage } = useIntl();
  const { data: dataExternalLink } = useGet(`/api/communities/${community?.id}/links`);
  useEffect(() => {
    setTimeout(() => {
      stickyTabNav(router);
      scrollToActiveTab(router);
      if (router.query.success === '1') {
        // if url success param is 1, show "saved changes" success alert
        $('#editSuccess').show(); // display success message
        setTimeout(() => {
          $('#editSuccess').hide(300);
        }, 2000); // hide it after 2sec
      }
    }, 700); // had to add setTimeout for the function to work
  }, []);

  // make different tab active depending if user is member or not
  let newsTabClasses;
  let aboutTabClasses;
  let newsPaneClasses;
  let aboutPaneClasses;
  if (community) {
    if (community.is_member || router.query.tab === 'feed') {
      newsTabClasses = 'nav-item nav-link active';
      aboutTabClasses = 'nav-item nav-link';
      newsPaneClasses = 'tab-pane active';
      aboutPaneClasses = 'tab-pane';
    } else {
      newsTabClasses = 'nav-item nav-link';
      aboutTabClasses = 'nav-item nav-link active';
      newsPaneClasses = 'tab-pane';
      aboutPaneClasses = 'tab-pane active';
    }
    // TODO: This should be a state
    // Dynamic SEO meta tags
    const pageTitle = `${community.title} | JOGL`;
    const pageDesc = community.short_description;
    const pageImg = community.banner_url || '/images/default/default-group.jpg';

    return (
      <Layout title={pageTitle} desc={pageDesc} img={pageImg}>
        {community && (
          <div className="communityDetails container-fluid">
            <CommunityHeader community={community} />
            <nav className="nav nav-tabs container-fluid">
              <a className={newsTabClasses} href="#news" data-toggle="tab">
                <FormattedMessage id="entity.tab.news" defaultMessage="News" />
              </a>
              <a className={aboutTabClasses} href="#about" data-toggle="tab">
                <FormattedMessage id="general.tab.about" defaultMessage="About" />
              </a>
            </nav>
            <div className="tabContainer">
              <div className="tab-content justify-content-center container-fluid">
                <div className={newsPaneClasses} id="news">
                  {community.feed_id && (
                    <Feed
                      feedId={community.feed_id}
                      // display post create component if you are member of the group
                      displayCreate={community.is_member}
                      isAdmin={community.is_admin}
                      needToJoinMsg={community.is_private}
                    />
                  )}
                </div>
                <div className={aboutPaneClasses} id="about">
                  <Box width={['100%', undefined, undefined, '70%']} margin="auto">
                    {community.description ? (
                      <InfoHtmlComponent title="" content={community.description} />
                    ) : (
                      community.short_description
                    )}
                    {dataExternalLink && dataExternalLink?.length !== 0 && (
                      <Box pt={8}>
                        <H2 pb={4}>
                          {formatMessage({ id: 'general.externalLink.findUs', defaultMessage: 'Find us' })}
                        </H2>
                        <Grid display="grid" gridTemplateColumns="repeat(auto-fill, minmax(55px, 1fr))">
                          {[...dataExternalLink].map((link, i) => (
                            <ExternalLinkIcon alignSelf="center" key={i} pb={3}>
                              <a href={link.url} target="_blank">
                                <img width="45px" src={link.icon_url} />
                              </a>
                            </ExternalLinkIcon>
                          ))}
                        </Grid>
                      </Box>
                    )}
                  </Box>
                </div>
              </div>
            </div>
            <Alert
              type="success"
              id="editSuccess"
              message={
                <FormattedMessage
                  id="general.editSuccessMsg"
                  defaultMessage="The changes have been saved successfully."
                />
              }
            />
          </div>
        )}
      </Layout>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

const ExternalLinkIcon = styled(Box)`
  img:hover {
    opacity: 0.8;
  }
`;

CommunityDetails.getInitialProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  try {
    const res = await api.get<Community>(`/api/communities/${ctx.query.id}`);
    return { community: res.data };
  } catch (err) {
    isomorphicRedirect(ctx, '/search/groups');
  }
};
export default CommunityDetails;
