import { useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Alert from '~/components/Tools/Alert';
import CommunityForm from '~/components/Community/CommunityForm';
import MembersList from '~/components/Members/MembersList';
import Layout from '~/components/Layout';
import { stickyTabNav, scrollToActiveTab } from '~/utils/utils';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import { useApi } from '~/contexts/apiContext';
import { NextPage } from 'next';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import Button from '~/components/primitives/Button';
import Translate from '~/utils/Translate';
import ReactGA from 'react-ga';
import { useModal } from '~/contexts/modalContext';
import P from '~/components/primitives/P';
import useUser from '~/hooks/useUser';
import { Community } from '~/types';
import 'twin.macro';

interface Props {
  community: Community;
}
const CommunityEdit: NextPage<Props> = ({ community: communityProp }) => {
  const [community, setCommunity] = useState(communityProp);
  const [updatedCommunity, setUpdatedCommunity] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [errors, setErrors] = useState('');
  const { showModal, setIsOpen } = useModal();
  const urlBack = `/community/${community?.id}/${community?.short_title}`;
  const router = useRouter();
  const api = useApi();
  const { user } = useUser();

  useEffect(() => {
    setTimeout(() => {
      stickyTabNav('isEdit');
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  const handleChange = (key, content) => {
    setCommunity((prevCommunity) => ({ ...prevCommunity, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedCommunity((prevUpdatedCommunity) => ({ ...prevUpdatedCommunity, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api
      .patch(`/api/communities/${community.id}`, { community: updatedCommunity })
      .catch(() => setSending(false));
    if (res) {
      setSending(false);
      setUpdatedCommunity(undefined); // reset updated community component
      setHasUpdated(true); // show update confirmation message
      setTimeout(() => {
        setHasUpdated(false);
      }, 3000); // hide confirmation message after 3 seconds
      // router.push({ pathname: urlBack, query: { success: 1 } }).then(() => window.scrollTo(0, 0)); // force scroll to top of page @TODO might not need anymore once this issue will be fixed: https://github.com/vercel/next.js/issues/15206
    }
  };

  const deleteGroup = async () => {
    const res = await api.delete(`api/communities/${community.id}/`).catch((error) => {
      setErrors(error.toString());
    });
    if (res) {
      ReactGA.event({ category: 'Group', action: 'delete', label: `[${user.id},${community.id}]` }); // record event to Google Analytics
      setIsOpen(false); // close modal
      router.push('/search/groups');
    }
  };
  const errorMessage = errors.includes('err-') ? (
    <FormattedMessage id={errors} defaultMessage="An error has occurred" />
  ) : (
    errors
  );
  const delBtnTitleId = community.members_count > 1 ? 'community.archive.title' : 'community.delete.title';
  const delBtnTitle = community.members_count > 1 ? 'Archive group' : 'Delete group';
  const delBtnTextId = community.members_count > 1 ? 'community.archive.text' : 'community.delete.text';
  const delBtnText =
    community.members_count > 1
      ? 'Are you sure you want to archive this group?'
      : 'Are you sure you want to delete this group?  It will be permanently deleted.';

  return (
    <Layout title={`${community.title} | JOGL`}>
      <div className="communityEdit container-fluid">
        <h1>
          <FormattedMessage id="community.edit.title" defaultMessage="Edit my Community" />
        </h1>
        <Link href={urlBack}>
          <a>
            {/* go back link */}
            <FontAwesomeIcon icon="arrow-left" />
            <FormattedMessage id="community.edit.back" defaultMessage="Go back" />
          </a>
        </Link>
        <nav className="nav nav-tabs container-fluid">
          <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
            <FormattedMessage id="entity.tab.basic_info" defaultMessage="Information" />
          </a>
          <a className="nav-item nav-link" href="#members" data-toggle="tab">
            <FormattedMessage id="entity.tab.members" defaultMessage="Members" />
          </a>
          <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
            <FormattedMessage id="entity.tab.advanced" defaultMessage="Advanced" />
          </a>
        </nav>
        <div className="tabContainer">
          <div className="tab-content justify-content-center container-fluid">
            <div className="tab-pane active" id="basic_info">
              <CommunityForm
                community={community}
                mode="edit"
                sending={sending}
                hasUpdated={hasUpdated}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
              />
            </div>
            <div className="tab-pane" id="members">
              <MembersList
                itemType="communities"
                itemId={parseInt(router.query.id as string)}
                isOwner={community.is_owner}
              />
            </div>
            <div className="tab-pane" id="advanced">
              <ManageExternalLink itemType="communities" itemId={router.query.id} />
              <hr />
              <div className="deleteBtns">
                {community && community.members_count > 1 && (
                  <p>
                    <FormattedMessage id="community.delete.explain" />
                  </p>
                )}
                <Button
                  onClick={() => {
                    showModal({
                      children: (
                        <>
                          {errors && <Alert type="danger" message={errorMessage} />}
                          <P fonSize="1rem">
                            <Translate id={delBtnTextId} defaultMessage={delBtnText} />
                          </P>
                          <div tw="inline-flex space-x-3">
                            <Button btnType="danger" onClick={deleteGroup}>
                              <FormattedMessage id="general.yes" defaultMessage="Yes" />
                            </Button>
                            <Button onClick={() => setIsOpen(false)}>
                              <FormattedMessage id="general.no" defaultMessage="No" />
                            </Button>
                          </div>
                        </>
                      ),
                      title: delBtnTitle,
                      titleId: delBtnTitleId,
                      maxWidth: '30rem',
                    });
                  }}
                  btnType="danger"
                >
                  <Translate id={delBtnTitleId} defaultMessage={delBtnTitle} />
                </Button>
                {community && community.members_count > 1 && (
                  <Button btnType="danger" disabled tw="ml-3">
                    <Translate id="community.delete.title" defaultMessage="Delete group" />
                  </Button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

CommunityEdit.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/communities/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch community with id=${query.id}`, err));
  // Check if it got the community and if the user is admin
  if (res?.data?.is_admin) return { community: res.data };
  isomorphicRedirect(ctx, '/search/groups');
};

export default CommunityEdit;
