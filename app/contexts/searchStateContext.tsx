import Router, { useRouter } from 'next/router';
import React, { Dispatch, SetStateAction, useEffect, useContext, useState } from 'react';
import qs from 'qs';
import { pathToSearchState, TabIndex } from '~/components/SearchTabs';

/**
 * SearchStateContext is a type of React Context that may be used to setup a search state.
 */
interface SearchStateContext {
  index: TabIndex;
  searchState: qs.ParsedQs;
  setIndex: (index: TabIndex) => void;
  setSearchState: Dispatch<SetStateAction<qs.ParsedQs>>;
}

const defaultSearchStateContext = {
  index: 'members',
  searchState: {},
} as SearchStateContext;

/**
 * SearchStateContext is a React Context that may be used to setup a search state.
 */
const SearchStateContext = React.createContext<SearchStateContext>(defaultSearchStateContext);

/**
 * useSearchStateContext may be used in a space's subcomponents to display
 * different search tabs.
 */
export const useSearchStateContext = () => useContext(SearchStateContext);

const searchStateToURL = (searchState, index) => {
  // Prevent the trailing ? when searchState is empty.
  const isSearchStateEmpty = Object.keys(searchState).length === 0 && searchState.constructor === Object;
  return isSearchStateEmpty ? `/search/${index}` : `/search/${index}?${qs.stringify(searchState)}`;
};

/**
 * Provides search state as context for components displayed as part
 * of the search page.
 * @param children Children of this provider
 */
export const SearchStateContextProvider: React.FC = ({ children }) => {
  const { asPath, query } = useRouter();
  const { 'active-index': activeIndex } = query;
  const [searchState, setSearchState] = useState(() => pathToSearchState(asPath));
  const [index, setIndex] = useState<TabIndex>(activeIndex as TabIndex);
  useEffect(() => {
    // Keep only the refinementList if present
    setSearchState((prevState) => ({
      ...(prevState?.refinementList ? { refinementList: prevState.refinementList } : {}),
      ...(prevState?.query ? { query: prevState.query } : {}),
    }));
    setIndex(activeIndex as TabIndex);
  }, [activeIndex, setSearchState, setIndex]);

  // Initialize searchState with params from the URL
  useEffect(() => {
    // Update URL params when searchState changes
    const href = searchStateToURL(searchState, index);
    Router.replace('/search/[active-index]', href, { shallow: true });
  }, [index, searchState]);

  return (
    <SearchStateContext.Provider value={{ index, searchState, setIndex, setSearchState }}>
      {children}
    </SearchStateContext.Provider>
  );
};
