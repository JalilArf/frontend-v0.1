module.exports = {
  ...require('./config/jest/jest-common'),
  projects: ['./config/jest/jest.lint.js', './config/jest/jest.client.js', './config//jest/jest.server.js'],
  // TODO: Remove coverage from test files.
  collectCoverageFrom: ['**/*.{js,jsx,ts,tsx}', '!**/*.d.ts', '!**/node_modules/**'],
  coveragePathIgnorePatterns: ['/node_modules/', '/__tests__/', '__server_tests__/'],
  // TODO: Change those values when reached decent amount of coverage
  coverageThreshold: {
    statements: 0,
    branches: 0,
    functions: 0,
    lines: 0,
  },
  // ///////////////////////////////////////////////////////////////
  moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
  // Mostly to enhance basic jest
  // setupFilesAfterEnv: ['<rootDir>/config/jest/setupTests.js'],
  // testPathIgnorePatterns: ['/node_modules/', '/.next/', '/config/'],
  transformIgnorePatterns: ['/node_modules/', '^.+\\.module\\.(css|sass|scss)$'],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
    '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
  },
};
