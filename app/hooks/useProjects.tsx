import useGet from '~/hooks/useGet';
import { ItemType, Project } from '~/types';

export default function useProjects(itemType: ItemType, itemId: number) {
  const { data, error: projectsError } = useGet<{ projects: Project }>(`/api/${itemType}/${itemId}/projects`);
  return { dataProjects: data?.projects, projectsError };
}
