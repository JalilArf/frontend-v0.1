import { useEffect } from 'react';
import TagManager from 'react-gtm-module';

export default function useGTM(TAGMANAGER_ARGS_ID) {
  useEffect(() => {
    // google tags manager
    if (TAGMANAGER_ARGS_ID) {
      const tagManagerArgs = {
        gtmId: process.env.TAGMANAGER_ARGS_ID,
      };
      TagManager.initialize(tagManagerArgs);
    }
  }, [TAGMANAGER_ARGS_ID]);
}
