import useSWR from 'swr';
import { FeatureFlags, User } from '../types';

const defaultFeatureFlags: FeatureFlags = {
  isSpacesAllowed: false,
};

const fetcher = (url: string): Promise<FeatureFlags> => fetch(url).then((res) => res.json());

/**
 * Hook that returns a set of feature flags for a given user.
 * @param user JOGL User
 */
export const useFeatureFlags = (user: User | undefined): FeatureFlags => {
  const { data } = useSWR<FeatureFlags>(() => user && `/api/featureflags/${user.id}`, fetcher);
  return data || defaultFeatureFlags;
};
