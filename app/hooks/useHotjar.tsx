import { useEffect } from 'react';
import { hotjar } from 'react-hotjar';

export default function useHotjar(HOTJAR_ID) {
  useEffect(() => {
    if (HOTJAR_ID) {
      hotjar.initialize(HOTJAR_ID);
    }
  }, [HOTJAR_ID]);
}
