import { render } from '@testing-library/react';
// import { ThemeProvider } from "my-ui-lib"
// import { TranslationProvider } from "my-i18n-lib"
// import defaultStrings from "i18n/en-x-default"
import { ThemeProvider as ThemeProviderEmotion } from '@emotion/react';
import theme from '~/utils/theme';
import { action } from '@storybook/addon-actions';
import { RouterContext } from 'next/dist/next-server/lib/router-context';
import { RawIntlProvider, createIntl } from 'react-intl';
import { useState } from 'react';
import Router, { NextRouter } from 'next/router';
import { UserContext } from '~/contexts/UserProvider';
// import { Url } from 'url';

const Providers = ({ children }) => {
  const [pathname, setPathname] = useState<string>('/');
  const mockRouter: NextRouter = {
    ...Router.router,
    pathname,
    query: {},
    prefetch: () => {},
    push: async (newPathname) => {
      // action('Clicked link')(newPathname);
      setPathname(newPathname);
    },
  };
  Router.router = mockRouter;
  const intl = createIntl({ locale: 'en', messages: {} });
  return (
    <RouterContext.Provider value={mockRouter}>
      <ThemeProviderEmotion theme={theme}>
        <RawIntlProvider value={intl}>
          <UserContext.Provider
            value={{
              isConnected: true,
              checkUserTokens: () => new Promise(() => {}),
              credentials: { accessToken: 'test', client: 'test', uid: 'test', userId: 'test' },
              frontLogout: () => new Promise(() => {}),
              logout: () => new Promise(() => {}),
              signIn: () => new Promise(() => {}),
              user: {},
            }}
          >
            {children}
          </UserContext.Provider>
        </RawIntlProvider>
      </ThemeProviderEmotion>
    </RouterContext.Provider>
  );
};

const customRender = (ui, options = {}) => render(ui, { wrapper: Providers, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
