# Contribute

JOGL is **100% open source**, and we fully welcome contributions! You can help us by **contributing to the code**, or translating the platform in your language.

We use the following technologies for frontend: [ReactJS](https://reactjs.org/), [NextJS](https://github.com/vercel/next.js/), Typescript, Algolia, Sass, AmazonS3, GraphQL, Elastic Search... If you have experience in one or multiple of them, your help will be much appreciated!

Feel free to browse through the issues and see if you can help us in one or multiple of them:

- **List view** - View issues in list: [frontend-only issues](https://gitlab.com/JOGL/JOGL/-/issues?label_name[]=frontend) or [all issues](https://gitlab.com/JOGL/JOGL/-/issues).
- **Board view** - View a Trello-like view of the issues listed by priority and state (to do, doing, ready/review): [frontend-only issues](https://gitlab.com/JOGL/JOGL/-/boards/1990876?&label_name[]=frontend) or [all issues](https://gitlab.com/JOGL/JOGL/-/boards/885598).

You can also contact us at support[at]jogl.io for any question.

### The philosophy:

1. API first, head to our [API documentation](https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest) for a current view of the API.
2. If what you need is missing, then you'll have to add it to our backend: https://gitlab.com/JOGL/backend-v0.1
3. If you want to add code, fork from the branch `develop` and merge request to the branch `develop`! Any request to `master` will be automatically rejected.
4. DRY!
5. Object Oriented thinking, if you think you should make a new component for this, then you should indeed! Modular code is the way forward!

For more detailed information on how to contribute, please read our [Contributing guidelines](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).

### How to report a bug or request a feature.

Please have a quick check through our [open issues](https://gitlab.com/JOGL/JOGL/-/issues) to see if your issue/problem/request has already been reported.

If so, you can click on :thumbsup: to indicate that you have the same issue, and you can add a comment to the issue to give more detail. If not, go ahead and **create your new issue** using [this link](https://gitlab.com/JOGL/JOGL/-/issues/new).

---

# Getting started

We highly recommend you setup a SSH key with your GitLab account (https://docs.gitlab.com/ee/ssh/), but this is up to you.

Clone the repository with SSH `git clone git@gitlab.com:JOGL/frontend-v0.1.git` or HTTPS `git clone https://gitlab.com/JOGL/frontend-v0.1.git`

Navigate to the repository

`cd frontend-v0.1/app`

Make sure you are onto the develop branch

`git checkout develop`

Create a new branch for your feature or bug correction

`git checkout -b MyNewBranch`

# Running the stack

You will need npm, follow the instalation guide for your computer (https://www.npmjs.com/get-npm)

At the root first `npm install` to install all required libraries followed by `npm run dev` to start the app locally.
We encourage you change your browser language to english, to enforce the usage of defaultMessage with `react-intl` while developing.

Before launching the app, make sure to duplicate the `.env.exemple` file (containing the necessary variables to run the stack), to create a `.env.local` file. The app will use the variables in this file on development and build mode.

Note: If you work on **both** the backend and the frontend, we recommend you setup your own Algolia account to be able to run your tests productively. See the section about [deployment](https://nextjs.org/docs/deployment#nodejs-server) for more information.

## Launching the app

### `npm run dev`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload as you make edits.<br>

### `npm run build` then `npm run start`

Builds the app for production.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
Then you can host it in a Node.JS server and start it with `npm run start`.

## Other available scripts

In the project directory, you can also run:

### `npm run test`

We are using Jest to test the app. Jest is configured with projects, meaning it will run multiple tests by default:

- Normal jest tests.
- ESLint tests

To only run normal tests, press `P` and de-select `eslint` by pressing the `space` key.

### `npm run analyze`

Visualize size of webpack output files with an interactive zoomable treemap. Will output the client and server results.

---

## Some technical aspect:

1. If you need an API to test things you can point to: https://jogl-backend-dev.herokuapp.com
2. If you prefer to work locally, test our dockerstack: https://gitlab.com/JOGL/JOGL

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
